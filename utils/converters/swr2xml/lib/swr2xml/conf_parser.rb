
require 'getoptlong'	
require 'rdoc/usage'
require 'logger'


module SWR2XML

# Support for .swr and .xml somewhere files
class ConfParser

	#####################
	public 

	attr_reader :path

	def initialize(path)
		@path = path
		@kind = nil
		readFile
	end


	# Save the theory as an XML file
	def saveAsXML(path)
		if @kind == :xml then
			File.copy(@path, path)
		elsif @kind == :swr then
			convertSWRtoXML path
		else
			raise "Impossible to convert file"
		end
	end

	# Save the theory as an .swr file
	def saveAsSWR(path)
		if @kind == :xml then
			convertXMLtoSWR path
		elsif @kind == :swr then
			File.copy(@path, path)
		else
			raise "Impossible to convert file"
		end
	end



	#####################
	private

	# Verify if file exists and determine its kind
	def readFile
		if @path =~ /.*(\.swr)$/ then
			@kind = :swr
		elsif @path =~ /.*(\.xml)$/ then
			@kind = :xml
		else
			throw "Invalid File"
		end
		throw "Invalid File" unless File.exists? @path
	end

	# Save a buffer to a file
	def saveBuff(path, buf)
		f = File.open(path, 'w')
		f.write buf
		f.close
	end


	# Convert an SWR file to XML
	def convertSWRtoXML path
		part = :name
		buff = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
		buff += "<!DOCTYPE peer SYSTEM \"PeerProfile.dtd\">\n"
		buff += "<peer>\n"

		begin

			File.open(@path, "r") { |file|
				while(line = file.gets)
					line.chomp!
					if line != "" or line != " " or line != "\n"
						case part
							when :name

								foo = line.split(":")
								uri = foo[0]
								peer = foo[2]

								buff += "\t<uri>#{uri}</uri>\n"
								buff += "\t<theory>\n"
								part = :theory
							when :theory

								if line == "Mappings"
									part = :mapping
									buff += "\t</theory>\n"
									buff += "\t<mapping>\n"
								else
									buff += "\t\t<clause>\n"
									buff += "\t\t\t<literal>\n"
									line.each(" ") { |foo|
										buff += "\t\t\t\t"
										buff += "<not>" if(foo =~ /^!(.)*/)

										lit = foo.gsub(/!/, "").split(":")
										litUri = lit[0]
										litPeer = lit[2]
										litVar = lit[3].chomp(" ")


										if litUri == uri and litPeer == peer then
											buff += "<variable>#{litVar}</variable>"
										else
											raise "Invalid File"
										end

										buff += "</not>" if(foo =~ /^!(.)*/)
										buff += "\n"
									}
									buff += "\t\t\t</literal>\n"
									buff += "\t\t</clause>\n"
								end
							when :mapping

								if line == "Production"
									part = :production
									buff += "\t</mapping>\n\n"
									buff += "\t<production>\n"
								else
									buff += "\t\t<clause>\n"
									buff += "\t\t\t<literal>\n"
									line.each(" ") { |foo|
										buff += "\t\t\t\t"
										buff += "<not>" if(foo =~ /^!(.)*/)

										lit = foo.gsub(/!/, "").split(":")
										litUri = lit[0]
										litPeer = lit[2]
										litVar = lit[3].chomp(" ")

										if litUri == uri and litPeer == peer then
											buff += "<variable>#{litVar}</variable>"
										else
											buff += "<variable url=\"#{litUri}:#{litPeer}\">#{litVar}</variable>"
										end

										buff += "</not>" if(foo =~ /^!(.)*/)
										buff += "\n"
									}
									buff += "\t\t\t</literal>\n"
									buff += "\t\t</clause>\n"
								end
							when :production

								line.each(" ") { |foo|
									lit = foo.gsub(/!/, "").split(":")
									litVar = lit[3].chomp(" ")

									buff += "\t\t<variable>#{litVar}</variable>\n"
								}
							else
								raise "Conversion error"
						end
					end
				end
			}

			buff += "\t</production>\n\n"
			buff += "</peer>"

		rescue Exception => e
			raise "Conversion error"
		end

		saveBuff(path, buff)
	end


	# Convert an XML file to SWR
	def convertXMLtoSWR path
		#TODO
		raise "TODO"
	end

end
	
	
	

end


