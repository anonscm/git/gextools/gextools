#!/usr/bin/env ruby
#--------------------------------------------
#== Synopsis
#	Concat a bunch of .swr files, or a directory of .swr files
#	to the a single .swr file
#
#== Usage
# swr2total --version
# swr2total --help
#	swr2total [-v] [-l path] file
#	swr2total [-v] [-l path] directory
#
#	Parameters:
#	 --version			Print program version
#	 -v				Verbose mode
#	 -l	file		Path to log file
#--------------------------------------------


require 'rubygems'
require 'optparse'
require 'logger'

module SWR2Total

	$logger = Logger.new(STDOUT)

	# The program's version
	$VERSION = "1.0"

	# Path of the .swr file or directory of .swr files
	#  to concat
	@path = "."

  # Path where the concated swr file will be outputed
  @output_path = "total.swr"

	# Says if we want the program to be verbose
	@verbose = false

	# Command Line Options
  opts = OptionParser.new do |opts|
    opts.banner = "Usage: swr2total.rb [options]"

    opts.separator ""
    opts.separator "Specific options:"

    opts.on("-v", "--verbose", "Run verbosely") do |v|
      @verbose = true
    end

    opts.on("-p", "--path PATH", "swr file or folder paths") do |path|
      @path = path
    end

    opts.on("-o", "--outputPath PATH", "path where the concatenated file will be ouputed") do |path|
      @output_path = path
    end

    opts.separator ""
    opts.separator "Common options:"

    # No argument, shows at tail.  This will print an options summary.
    # Try it and see!
    opts.on_tail("-h", "--help", "Show this message") do
      puts opts
      exit
    end

    # Another typical switch to print the version.
    opts.on_tail("-V","--version", "Show version") do
      puts $VERSION
      exit
    end

  end

  opts.parse!(ARGV)

	# Do the conversion of all specified .swr files
	def self.concat
    begin
      # Extract selected paths
      paths = []
      if File.directory? @path then
        paths = Dir["#{@path}/**/*.swr"].reject {|fn| File.directory?(fn) }
      else
        paths << @path
      end

      raise "The are no valid files to contact" if paths.empty?

      # Extract content of each swr file
      contents = []
      paths.each { |path|
        # Get a peer object from the current file content
        File.open(path, "r") { |file|
          contents << file.read
        }
      }

      # Starting concatenation process...
      total_content = []
      productions = ''
      contents.each { |content|

        # First step, delete profile id...
        content.sub!(/^.+$\n/, '')

        # Delete profile names...
        content.gsub!(/(!?)(\w+:)/, '\1total:')

        # Deleting the 'Mappings' special word...
        content.gsub!(/Mappings/, '')

        # Taking everything after 'Productions' and save it to a temp buffer...
        content =~ /Production\s*(.+)/
        productions += $1 unless $1.nil?

        # Deleting the 'Productions' special word...
        content.gsub!(/Production\s*(.+)/, '')

        # Put every clause in the total content array
        content.split("\n").each { |clause|
          total_content << clause unless clause.empty?
        }
      }

      # Duplicated clauses out!
      total_content.uniq!

      # Writing output file
      File.open(@output_path, "w") { |file|
        file.puts 'total'
        total_content.each { |clause|
          file.puts clause
        }
        file.puts 'Production'
        file.puts productions
      }

      $logger.info "The total swr file was generated sucessfully"
      exit 0
      
    rescue
			$logger.error $!.message
      $logger.error $!.backtrace.join("\n") if @verbose
		end
	end

	concat


end

