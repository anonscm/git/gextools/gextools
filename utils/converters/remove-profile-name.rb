#!/usr/bin/env ruby
#----------------------------------------------------------------------------------------
# Transform a swr instance dir with full name of profile into an instance without 
#  the full name of profiles
#----------------------------------------------------------------------------------------

require 'fileutils'

def usage
	puts "usage : cmd INDIR DESTDIR   or  cmd INFILE DESTDIR"
	exit
end

def convertDir(input_dir_path, destdir_path)
  if !File.exist?(input_dir_path)
    puts "invalid input dir : "+ input_dir_path
  else
    input_dir_basename = File.basename(input_dir_path)
    output_dir_path = File.join(destdir_path,input_dir_basename)
    if File.exist?(output_dir_path)
      puts "dir "+ output_dir_path + " already exists. Ignoring " + input_dir_basename + "\n"
    else
      FileUtils.mkdir_p(output_dir_path)
      Dir.new(input_dir_path).each { |name|
        next if File.fnmatch('.*',  name)
          # files beginning with a . (including . and .. .svn) are ignored
        full_input_name = File.join(input_dir_path, name)
        if File.directory?(full_input_name) then
          convertDir(full_input_name, output_dir_path)
        else
          if File.fnmatch('*.swr',  name)
            convertFile(full_input_name, output_dir_path)
          else
             output_path = File.join(output_dir_path, name)
             FileUtils.cp(full_input_name,output_path)
          end
        end
      }
    end
  end
end


def convertFile(path, destdir_path)
  if !File.exist?(destdir_path)
    puts "invalid dest dir " + destdir_path + " - ignoring "+path
  else
    inputFileName = File.basename(path)
    destfilepath = File.join(destdir_path,inputFileName)
    content =""
    File.open(path, "r") { |file|
      content = file.read
      content = content.gsub(/[^!:\s]+:[^:]+:(\S+\s)/, '\1')
    }
    File.open(destfilepath, "w") { |outfile|
      outfile.print content
    }
  end
end


# Take the name of the directory, in command line params
$path = ARGV.shift
usage if $path.nil? || $path == "" || !File.exist?($path)
$outpath = ARGV.shift
usage if $outpath.nil? || $outpath == ""

# Open the directory, if its a dir, or the file, if its a file
if File.file?($path) then
  convertFile($path, $outpath)
elsif File.directory?($path) then
  convertDir($path, $outpath)
else
  usage
end
