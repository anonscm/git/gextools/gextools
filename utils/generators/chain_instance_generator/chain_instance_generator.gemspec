lib = File.expand_path('../lib/', __FILE__)
$:.unshift lib unless $:.include?(lib)

Gem::Specification.new do |s|
  s.name = 'chain_instance_generator'
  s.version = '1.0'
  s.has_rdoc = true
  s.date = Time.now.utc.strftime("%Y-%m-%d")
  s.extra_rdoc_files = ['README', 'LICENSE']
  s.summary = 'Chain Instance Generator'
  s.description = s.summary
  s.author = 'Andre Fonseca'
  s.email = 'andre.amorimfonseca@gmail.com'
  s.executables = ['chainGenerator']
  s.files = %w(LICENSE README Rakefile) + Dir.glob("{bin,lib,spec}/**/*")
  s.require_path = "lib"
  s.bindir = "bin"

  s.add_development_dependency 'debugger'

  s.add_runtime_dependency "rake"
  s.add_runtime_dependency "trollop"
end
