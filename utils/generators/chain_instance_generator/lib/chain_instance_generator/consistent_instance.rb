# To change this template, choose Tools | Templates
# and open the template in the editor.

module ChainGenerator

require 'chain_instance_generator/instance'

class ConsistentInstance < Instance
  
  def generate(number)

    for i in (0..(number - 1))
      name = "P#{i}"
      clauses = ["P#{i}:A#{i} P#{i}:C#{i}"]
      mappings = []
      mappings.push("P#{i}:A#{i} P#{i}:B#{i} !P#{i+1}:A#{i+1}") unless i == (number - 1)
      target = ["P#{i}:A#{i}", "P#{i}:B#{i}", "P#{i}:C#{i}"]

      @peers[name] = Peer.new(name, clauses, mappings, target)
    end
    
  end

end

end

