# To change this template, choose Tools | Templates
# and open the template in the editor.

module ChainGenerator

require 'chain_instance_generator/instance'

class InconsistentInstance < Instance

  def generate(number)

    random_number = rand(number)
    random_number += 1 if random_number == 0
    random_number -= 1 if random_number == number

    for i in (0..(random_number - 1))
      name = "P#{i}"
      clauses = ["!P#{i}:A#{i} P#{i}:D#{i}", "!P#{i}:D#{i} P#{i}:C#{i}", "!P#{i}:D#{i} P#{i}:B#{i}"]
      mappings = []
      mappings.push("!P#{i}:A#{i} P#{i+1}:A#{i+1}")
      mappings.push("!P#{i}:A#{i} !P#{number - 1}:A#{number - 1}") if i == 0
      target = ["P#{i}:A#{i}", "P#{i}:B#{i}", "P#{i}:C#{i}", "P#{i}:D#{i}"]

      @peers[name] = Peer.new(name, clauses, mappings, target)
    end

    name = "P#{random_number}"
    clauses = ["!P#{random_number}:A#{random_number} P#{random_number}:D#{random_number}",
      "!P#{random_number}:D#{random_number} P#{random_number}:C#{random_number}"]
    mappings = []
    mappings.push("!P#{random_number}:D#{random_number} P#{random_number}:B#{random_number} P#{random_number+1}:D#{random_number+1}")
    mappings.push("!P#{random_number}:A#{random_number} P#{random_number+1}:A#{random_number+1}")
    target = ["P#{random_number}:A#{random_number}", "P#{random_number}:B#{random_number}",
      "P#{random_number}:C#{random_number}", "P#{random_number}:D#{random_number}"]

    @peers[name] = Peer.new(name, clauses, mappings, target)

    for i in ((random_number + 1)..(number-1))
      name = "P#{i}"
      clauses = ["!P#{i}:D#{i} P#{i}:C#{i}"]
      mappings = []
      mappings.push("!P#{i}:D#{i} P#{i}:B#{i} P#{i+1}:D#{i+1}") unless i == (number - 1)
      mappings.push("!P#{i}:A#{i} P#{i+1}:A#{i+1}") unless i == (number - 1)
      target = ["P#{i}:A#{i}", "P#{i}:B#{i}", "P#{i}:C#{i}", "P#{i}:D#{i}"]

      @peers[name] = Peer.new(name, clauses, mappings, target)
    end
    
  end

end
end
