
require 'logger'
require 'rubygems'
require 'fileutils'

module ChainGenerator

require 'chain_instance_generator/peer'

class Instance

  attr_accessor :peers
  
  def initialize
    @peers = {}
  end

  def generate(number)
    NotImplementedError.new
  end

  def export(name, path)
    raise ArgumentError.new(:name) if name.nil?
    raise ArgumentError.new(:path) if path.nil?

    path = File.join(path, name)

    # Says a warning if the directory already exists
    $logger.warn "The output data directory #{path} already exists ! Still continuing anyway..." if File.exists? path

    begin
      # Create the directory if it doesn't already exists
      FileUtils.mkdir_p path

      contents = export_swr()
      contents.each { |peer_name, content|
        $logger.debug "Generating peer #{peer_name}.swr..."
        new_path = File.join(path, "#{peer_name}.swr")
        File.open(new_path, "w") { |file|
          # Write content
          file.write content
        }
      }
    rescue
      $logger.error "Imposible to copy instance into file with options " +$!
      raise
    end
  end

  def export_swr()

    contents = {}
    @peers.each { |name, peer|
      # Write name
      swr_string = name + "\n"

      # Write theory
      peer[:theory].each { |c|
        swr_string += c + "\n"
      }

      # Write mappings
      swr_string += "Mappings" + "\n"
      peer[:mappings].each { |c|
        swr_string += c + "\n"
      }

      # Write targets
      swr_string += "Production" + "\n"
      line = ""
      peer[:targets].each { |v|
        line += v + " "
      }
      swr_string += line + "\n"

      contents[name] = swr_string
    }

    return contents

  end
  
end
end
