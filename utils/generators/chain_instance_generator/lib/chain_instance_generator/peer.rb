# To change this template, choose Tools | Templates
# and open the template in the editor.

module ChainGenerator

class Peer < Hash

  def initialize(name, theory, mappings, targets)
    raise ArgumentError.new(:theory) if theory.nil?
		raise ArgumentError.new(:mappings) if mappings.nil?
		raise ArgumentError.new(:name) if name.nil?
		raise ArgumentError.new(:targets) if targets.nil?

		self[:name] = name # The theory clauses list object
		self[:theory] = theory # The mapping clauses list object
		self[:targets] = targets # The targets variables list object
		self[:mappings] = mappings # The name of the peer
	end
  
end

end
