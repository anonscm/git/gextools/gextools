require 'logger'


module ChainGenerator


$logger = Logger.new(STDOUT)

# The program's version
$VERSION = "1.0"

# The program's version String
$VERSIONSTR = "Chain Instance Generator version #{$VERSION}"

#------------------------------------------#
# Use a file as log file                   #
#------------------------------------------#
def confLogger(path)
	begin
		file = File.open(path, File::WRONLY | File::APPEND)
		logger = Logger.new(file)
	rescue Exception => e
		puts "Cannot use file #{path} as log file"
		exit 1
	end
end


end
