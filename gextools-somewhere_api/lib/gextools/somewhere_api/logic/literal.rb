#
# Gextools Somewhere API - PLogic API (
# https://sourcesup.renater.fr/projects/gextools/ ) - This file is part of
# Somewhere API
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

module GexTools
module Components
module SomewhereApi
module Logic

require 'gextools/somewhere_api/logic/variable'

# Define a logical literal
class Literal < Variable

	# Says if this literal is a negative one
	@negative = false

	attr_accessor :negative

	def initialize(name, negative)
		super(name)
		raise "Invalid Argument" if negative == nil
		@negative = negative
	end

	def negative?
		negative
	end

	def to_s
		res = ""
		res += "!" if negative?
		res += @name.to_s
		return res
	end

  def ==(object)
    return self.to_s == object.to_s
  end

end


end
end
end
end