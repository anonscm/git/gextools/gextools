#
# Gextools Somewhere API - PLogic API (
# https://sourcesup.renater.fr/projects/gextools/ ) - This file is part of
# Somewhere API
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

module GexTools
module Components
module SomewhereApi
module Logic

# Define a logical clause
class Clause < Array

	def to_s
		#@literals.join(" ")
		join(" ")
	end

end

end
end
end
end