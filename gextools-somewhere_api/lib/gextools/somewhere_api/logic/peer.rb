#
# Gextools Somewhere API - PLogic API (
# https://sourcesup.renater.fr/projects/gextools/ ) - This file is part of
# Somewhere API
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

module GexTools
module Components
module SomewhereApi
module Logic

require 'gextools/somewhere_api/logic/variable'
require 'gextools/somewhere_api/logic/theory'
require 'gextools/somewhere_api/logic/mappings'
require 'gextools/somewhere_api/logic/targets'

# Define peer
class Peer < Hash

	def initialize(name, theory, mappings, targets, variables)
    raise ArgumentError.new(:theory) if theory.nil?
		raise ArgumentError.new(:mappings) if mappings.nil?
		raise ArgumentError.new(:targets) if targets.nil?
		raise ArgumentError.new(:name) if name.nil?
		raise ArgumentError.new(:variables) if variables.nil?

		self[:name] = name # The theory clauses list object
		self[:theory] = theory # The mapping clauses list object
		self[:targets] = targets # The targets variables list object
		self[:mappings] = mappings # The name of the peer
		self[:variables] = variables # The variables of the peer
	end

	def deleteMappings
		self[:mappings] = []
	end

end

end
end
end
end