#
# Gextools Somewhere API - PLogic API (
# https://sourcesup.renater.fr/projects/gextools/ ) - This file is part of
# Somewhere API
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#
module GexTools
module Components
module SomewhereApi
module Instances

require 'gextools/progs/progs'
require 'gextools/instances/instance_chunk_format'


# Deals with saving and importing a peer in dimacs format
class SATUtils

  #------------------------------------------#
	# Perform SAT analysis                     #
  #------------------------------------------#
  def self.analyseSAT(instance, sat)
    raise ArgumentError.new(:instance) if instance.nil?
    raise ArgumentError.new(:sat) if sat.nil?

    path = nil
    consistent = false

    $logger.info "Performing sat analysis with sat prog '#{sat.name}' ..."

    begin
      # Prepare analysis
      eval "path = #{instance.instance_chunk_format.format}Format.prepareSatAnalysis(instance)"
    rescue
      $logger.error "Impossible to retrieve DIMACS instance file. Reason : " + $!.inspect
      $logger.error $!.backtrace.join("\n")
      raise
    end

    begin
      # Perform analysis
      output = sat.execute(:parameters => path) unless path.nil?
      res_satis = (output =~ /s SATISFIABLE/)
    rescue
      $logger.error "Impossible to perform sat analysis. Reason : " + $!.inspect
      $logger.error $!.backtrace.join("\n")
      raise
    ensure
      # Release folder used on analysis
      GexTools::Temp.release(File.dirname(path)) unless path.nil?
    end

    if res_satis.nil?
      $logger.info "[#Instance #{instance.name} is unsatisfiable]"
    else
      $logger.info "[#Instance #{instance.name} is satisfiable]"
      consistent = true
    end

    return consistent
  end

end

end
end
end
end