#
# Gextools Somewhere API - PLogic API (
# https://sourcesup.renater.fr/projects/gextools/ ) - This file is part of
# Somewhere API
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'debugger'

module GexTools
module Components
module SomewhereApi
module Instances

require 'gextools/instances/instance'
require 'gextools/instances/concrete_instance'

require 'gextools/somewhere_api/instances/SWR_format'
require 'gextools/somewhere_api/instances/DIMACS_format'


# Instance object for somewhere, as it will be recorded in the database
class SWInstance < GexTools::Instances::ConcreteInstance

  self.table_name = "sw_instances"

  #------------------------------------------#
  # Gets concrete instance infos             #
  #------------------------------------------#
  def infos()
    res = "\n\tnumber of peers: #{self.peersNumber}"
    res << "\n\tnumber of clauses: #{self.clauseNumber}"
    res << "\n\tnumber of literals: #{self.litNumber}"
    res << "\n\tnumber of mappings: #{self.mappingNumber}"
    res << "\n\tnumber of targets: #{self.mappingNumber}"
    res
  end

  #------------------------------------------#
  # Parse instance files                     #
  #------------------------------------------#
  def parse(content)
    format = self.instance.instance_chunk_format.format

    peer = nil

    case
    when format == "SWR"
      peer = GexTools::Components::SomewhereApi::Instances::SWRFormat.parse(content)
    when format == "DIMACS"
      peer = GexTools::Components::SomewhereApi::Instances::DIMACSFormat.parse(content)
    else
    end

    return peer
  end

  #------------------------------------------#
  # Fetches instance statistics              #
  #------------------------------------------#
  def fetchInstanceStats()
    self.peersNumber = instance.peers.size
    self.clauseNumber = 0
    self.litNumber = 0
    self.mappingNumber = 0
    self.targetNumber = 0

    instance.peers.each { |name, peer|
      self.mappingNumber += peer[:mappings].size
      self.targetNumber += peer[:targets].size
      peer[:theory].each { |clause|
         self.clauseNumber += 1
         clause.each{ |lit|
           self.litNumber += 1
         }
      }
    }
  end

  #------------------------------------------#
  # Delete all mappings from logic peer      #
  #------------------------------------------#
  def deleteMappingsFromPeer
    instance.peers.each { |name, peer|
      peer.deleteMappings
    }
  end

  #------------------------------------------#
  # Delete all mappings from chunk files     #
  #------------------------------------------#
  def deleteMappingsFromChunk(content)
    lines = content.split("\n")
    mappings_index = lines.index("Mappings")
    production_index = lines.index("Production")
    lines_to_delete_num = production_index - mappings_index
    
    lines_to_delete_num.times do
      lines.delete_at(mappings_index)
    end

    return lines.join("\n")
  end

  #------------------------------------------#
  # Duplicate mappings in chunk files        #
  #------------------------------------------#
  def duplicateMappingsInChunk(current_peer_name, content, chunks)
    lines = content.split(/[\r\n]+/)
    
    mappings_index = lines.index("Mappings")
    return if (mappings_index.nil?)
    
    production_index = lines.index("Production")
    if (!production_index.nil?)
      mappings = lines[(mappings_index + 1)..(production_index - 1)]
    else
      mappings = lines[(mappings_index + 1)..(lines.size - 1)]
    end

    mappings.each { |mapping|
      peer_names = mapping.scan(/!?(\w+):/).flatten
      peer_names.each { |peer_name|

        if (current_peer_name != peer_name) then
          distant_content = chunks[peer_name]
          distant_lines = distant_content.split(/[\r\n]+/)
          distant_mappings_index = distant_lines.index("Mappings")
          distant_lines.insert(distant_mappings_index + 1, mapping) if !distant_lines.include?(mapping)
          chunks[peer_name] = distant_lines.join("\n")
        end
        
      }
    }

  end

end



end
end
end
end
