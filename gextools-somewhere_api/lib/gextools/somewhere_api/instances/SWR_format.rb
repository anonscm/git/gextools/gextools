#
# Gextools Somewhere API - PLogic API (
# https://sourcesup.renater.fr/projects/gextools/ ) - This file is part of
# Somewhere API
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

module GexTools
module Components
module SomewhereApi
module Instances

require 'gextools/instances/instances'
require 'gextools/instances/instance_chunk_format'
require 'gextools/progs/progs'

require 'gextools/somewhere_api/logic/theory'
require 'gextools/somewhere_api/logic/targets'
require 'gextools/somewhere_api/logic/mappings'
require 'gextools/somewhere_api/logic/clause'
require 'gextools/somewhere_api/logic/literal'
require 'gextools/somewhere_api/logic/peer'

require 'gextools/somewhere_api/instances/DIMACS_format'


# Deals with saving and importing a peer in swr format
class SWRFormat <  GexTools::Instances::InstanceChunkFormat
  
	def self.parse(content)
		raise ArgumentError.new(:content) if content.nil?

		name = nil
		theory = GexTools::Components::SomewhereApi::Logic::Theory.new
		mappings = GexTools::Components::SomewhereApi::Logic::Mappings.new
		targets = GexTools::Components::SomewhereApi::Logic::Targets.new
		variables = []
		part = :name

    content.each_line { |line|
      line.chomp!
      line.strip!
      if line != "" or line != " " or line != "\n"
        case part
          when :name
            name = line
            part = :theory
          when :theory
            if line.capitalize == "Mappings"
              part = :mapping
            elsif line.capitalize == "Production"
              part = :production
            else
              clause = GexTools::Components::SomewhereApi::Logic::Clause.new
              literals = line.split(/\s+/)
              literals.each { |foo|
                next if foo.nil? or foo.to_s == " "
                neg = (foo =~ /^!(.)*/) == 0
                litStr = foo.gsub(/!/, "").chomp(" ")
                lit = GexTools::Components::SomewhereApi::Logic::Literal.new(litStr, neg)
                clause << lit
                var = GexTools::Components::SomewhereApi::Logic::Variable.new(litStr)
                variables << var
              }
              theory << clause
            end
          when :mapping
            if line.capitalize == "Production"
              part = :production
            else
              clause = GexTools::Components::SomewhereApi::Logic::Clause.new
              literals = line.split(/\s+/)
              literals.each { |foo|
                next if foo.nil? or foo.to_s == " "
                neg = (foo =~ /^!(.)*/) == 0
                litStr = foo.gsub(/!/, "").chomp(" ")
                lit = GexTools::Components::SomewhereApi::Logic::Literal.new(litStr, neg)
                clause << lit
                var = GexTools::Components::SomewhereApi::Logic::Variable.new(litStr)
                #variables << var
              }
              mappings << clause
            end
          when :production
            literals = line.split(/\s+/)
            literals.each { |foo|
              next if foo.nil? or foo.to_s == " "
              neg = (foo =~ /^!(.)*/) == 0
              litStr = foo.gsub(/!/, "").chomp(" ")
              lit = GexTools::Components::SomewhereApi::Logic::Literal.new(litStr, neg)
              targets << lit.clone
              var = GexTools::Components::SomewhereApi::Logic::Variable.new(litStr)
              variables << var.clone
            }
        else
          raise "Conversion error"
        end # case
      end # if
    }

    # XXX:It is really necessary to check unicity ???
		theory.uniq
		mappings.uniq
		targets.uniq
		variables.uniq

		peer = GexTools::Components::SomewhereApi::Logic::Peer.new(name, theory, mappings, targets, variables)
		return peer
	end


  def self.export(peers={})

    contents = {}
    peers.each { |name, peer|
      # Write name
      swr_string = name + "\n"

      # Write theory
      peer[:theory].each { |c|
        line = ""
        c.each{ |l|
          line += "!" if l.negative
          line += l.name + " "
        }
        swr_string += line + "\n"
      }

      # Write mappings
      swr_string += "Mappings" + "\n"
      peer[:mappings].each { |c|
        line = ""
        c.each{ |l|
          line += "!" if l.negative
          line += l.name + " "
        }
        swr_string += line + "\n"
      }

      # Write targets
      swr_string += "Production" + "\n"
      line = ""
      peer[:targets].each { |v|
        line += v.name + " "
      }
      swr_string += line + "\n"

      contents[name] = swr_string
    }

    return contents

  end
  
  def self.prepareSatAnalysis(instance)
    # Get a temporary folder
    temp_dir = GexTools::Temp.getTempFolder
    path = File.join(temp_dir, "instance.cnf")

    begin
      contents = GexTools::Components::SomewhereApi::Instances::DIMACSFormat.export(instance.peers)

      contents.each { |name, content|
        File.open(path, "w") { |file|
          # Write content
          file.write content
        }
      }
    rescue
      $logger.error "Imposible to prepare cnf instance ... #{$!}"
      raise
    end
    
    return path
  end

end



end
end
end
end