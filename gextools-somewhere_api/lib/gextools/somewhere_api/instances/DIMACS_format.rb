#
# Gextools Somewhere API - PLogic API (
# https://sourcesup.renater.fr/projects/gextools/ ) - This file is part of
# Somewhere API
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

module GexTools
module Components
module SomewhereApi
module Instances

require 'gextools/progs/progs'
require 'gextools/instances/instance_chunk_format'

require 'gextools/somewhere_api/logic/theory'
require 'gextools/somewhere_api/logic/targets'
require 'gextools/somewhere_api/logic/mappings'
require 'gextools/somewhere_api/logic/clause'
require 'gextools/somewhere_api/logic/literal'
require 'gextools/somewhere_api/logic/peer'


# Deals with saving and importing a peer in dimacs format
class DIMACSFormat < GexTools::Instances::InstanceChunkFormat

  
	def self.export(peers={})
    
    contents = {}
    clauses = []
    vars = []

    peers.each { |name, peer|
      (peer[:theory] + peer[:mappings]).each { |clause|
        clauseTmp = []
        clause.each { |lit|
          vars << lit.name.chomp(" ") unless vars.include? lit.name.chomp(" ")

          dimacsVarNb = 1 + vars.index(lit.name.chomp(" "))
          if lit.negative?
            dimacsVar = - dimacsVarNb
          else
            dimacsVar = dimacsVarNb
          end
          clauseTmp << dimacsVar.to_s
        }
        clauseTmp << " 0"
        clauses << clauseTmp
      }

      $logger.info "Finished parsing peer #{peer[:name]} ..." if $config[:verbose]
    }

    # Building content
    content = "p cnf #{vars.size} #{clauses.size}\n"
    clauses.each { |clause|
      content += "#{clause.join(" ")}\n"
    }

    contents["instance"] = content

    return contents
	end


  def self.prepareSatAnalysis(instance)
    path = File.join($config[:dataDirectory], instance.name, "sat", "#{instance.name}.cnf")
    return path
  end

end


end
end
end
end