#
# Gextools Somewhere API - PLogic API (
# https://sourcesup.renater.fr/projects/gextools/ ) - This file is part of
# Somewhere API
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#


module GexTools
module Components
module SomewhereApi
module Problems

require 'gextools/problems/problem'
require 'gextools/problems/concrete_problem'

# ClauseAdd object for somewhere, as it will be recorded in the database
class ClauseAdd < GexTools::Problems::ConcreteProblem

  self.table_name = "sw_clause_add"

  #------------------------------------------#
	# Return the problem type short name       #
  #------------------------------------------#
  def getProblemTypeName()
    return "clauseAdd"
  end

  #------------------------------------------#
	# Return the problem concerned peers       #
  #------------------------------------------#
  def getConcernedPeers()
    if ($config[:component] == "somewhere")
      return modifiedPeers.split("&")
    else
      return modifiedPeers.split(" v ")
    end
  end

  #------------------------------------------#
	# Return the problem concerned             #
  # problem string                           #
  #------------------------------------------#
  def getConcernedProblem()
    return clause
  end


  #------------------------------------------#
	# Return the problem concerned             #
  # problem string (SWR Format)              #
  #------------------------------------------#
  def getProblemString()
    if ($config[:component] == "somewhere")
      symbol = "&"
    else
      symbol = " v "
    end

    result = ""
    list = clause.split(symbol)
    list.each { |lit|

      lit =~ /(!?)(.+)/
      neg = ($1 == "!")
      string = $2

      if neg then
        result += "!#{string}#{symbol}"
      else
        result += "#{string}#{symbol}"
      end
    }

    return result.chop!
  end

end



end
end
end
end