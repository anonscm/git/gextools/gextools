#
# Gextools Somewhere API - PLogic API (
# https://sourcesup.renater.fr/projects/gextools/ ) - This file is part of
# Somewhere API
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

module GexTools
module Components
module SomewhereApi
module Problems

require 'gextools/problems/problem'
require 'gextools/problems/concrete_problem'

# Query object for somewhere, as it will be recorded in the database
class Query < GexTools::Problems::ConcreteProblem

  self.table_name = "sw_queries"

  #------------------------------------------#
	# Return the problem type short name       #
  #------------------------------------------#
  def getProblemTypeName()
    return "query"
  end

  #------------------------------------------#
	# Return the problem concerned peers       #
  #------------------------------------------#
  def getConcernedPeers()
     if ($config[:component] == "somewhere")
      return queriedPeer.split("&")
    else
      return queriedPeer.split(" v ")
    end
  end

  #------------------------------------------#
	# Return the problem concerned             #
  # problem                                  #
  #------------------------------------------#
  def getConcernedProblem()
    return query
  end

  #------------------------------------------#
	# Return the problem concerned             #
  # problem string (SWR format)              #
  #------------------------------------------#
  def getProblemString()
    query =~ /(!?)(.+)/
    neg = ($1 == "!")
    lit = $2

    result = "#{queriedPeer}:#{lit}"
    result = "!" + result if neg
    
    return result
  end

end



end
end
end
end