#
# Gextools Somewhere API - PLogic API (
# https://sourcesup.renater.fr/projects/gextools/ ) - This file is part of
# Somewhere API
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

module GexTools
module Components
module SomewhereApi
module Problems

require 'gextools/problems/problem'

require 'gextools/somewhere_api/logic/clause'
require 'gextools/somewhere_api/logic/literal'


# Somewhere Queries Generation
class GenQueries


	#####################
	protected


	#####################
	public

  #------------------------------------------#
	# Deals with 'queries' problem             #
  # generation                               #
  #------------------------------------------#
	def self.generate(opts)
		raise ArgumentError.new(:instance) if opts[:instance].nil?
    raise ArgumentError.new(:nbQueries) if opts[:nbQueries].nil?
    raise ArgumentError.new(:probabilityOfNegativeQueries) if opts[:probabilityOfNegativeQueries].nil?

    instance = opts[:instance]

		# Get interesting values from opts and stuffs
		nbQueries = opts[:nbQueries].to_i
		probabilityOfNegativeQueries = opts[:probabilityOfNegativeQueries].to_f
		targetVariablesAreInterresting = opts[:targetVariablesAreInterresting]

		# Get interesting variables list for all peers of the instance
		# Substitute targets variables if necessary
		vars = {}
		instance.peers.each { |peerName, peer|
			vars[peerName] =	if targetVariablesAreInterresting
									peer[:variables]
								else
									peer[:variables] - peer[:targets]
								end
		}

		# Verify that opts parameters are correct
		if opts[:nbMaxLiteralsPerQuery] < opts[:nbMinLiteralsPerQuery]
			opts[:nbMinLiteralsPerQuery] = opts[:nbMaxLiteralsPerQuery]
		end
		opts[:nbMinLiteralsPerQuery] = 1 if opts[:nbMinLiteralsPerQuery] < 1
		opts[:nbMaxLiteralsPerQuery] = 1 if opts[:nbMaxLiteralsPerQuery] < 1

		# Generate !
    problems = []
    logic_queries = []
    size = vars.size
    nbQueries.times {
      # Determine how many literal this query will contain
			nbLit = opts[:nbMinLiteralsPerQuery] + rand(opts[:nbMaxLiteralsPerQuery] - opts[:nbMinLiteralsPerQuery])

      logic_query = GexTools::Components::SomewhereApi::Logic::Clause.new
      peerName = vars.keys[rand(size)]
      peerVars = vars[peerName]

      nbLit.times {
        var = peerVars.sort_by{rand}[0]
        next if var.nil? or var.to_s == " "
        neg = (rand <= probabilityOfNegativeQueries)
        lit = GexTools::Components::SomewhereApi::Logic::Literal.new(var, neg)
        logic_query << lit
      }

      next if logic_queries.include?(logic_query) #XXX There is no warranty about the correct number of queries

      problem = GexTools::Problems::Problem.new
      concrete_query = GexTools::Components::SomewhereApi::Problems::Query.new
      concrete_query.query = logic_query.to_s.gsub(/#{peerName}:/, "")
      concrete_query.queriedPeer = peerName
      problem.concrete_problem = concrete_query
      logic_queries << logic_query
      problems << problem

    }

		return problems
	end

  #------------------------------------------#
	# Parse 'queries' problems                 #
  #------------------------------------------#
  def self.parse(line)
    line = line.strip
    return if line.nil? or line == "\n"
    
    line =~ /(!?)(.+):(.+)/
    neg = ($1 == "!")
    peer_name = $2
    tmp = $3
    lit = GexTools::Components::SomewhereApi::Logic::Literal.new(tmp, neg)

    problem = GexTools::Problems::Problem.new
    concrete_query = GexTools::Components::SomewhereApi::Problems::Query.new
    concrete_query.query = lit.to_s
    concrete_query.queriedPeer = peer_name
    problem.concrete_problem = concrete_query

    return problem
  end

end



end
end
end
end