#
# Gextools Somewhere API - PLogic API (
# https://sourcesup.renater.fr/projects/gextools/ ) - This file is part of
# Somewhere API
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#


module GexTools
module Components
module SomewhereApi
module Problems

require 'gextools/problems/problem'
require 'gextools/somewhere_api/logic/literal'


# Somewhere GenClauseAdd Generation
class GenClauseAdd

	#####################
	protected


	#####################
	public


	#------------------------------------------#
	# Deals with 'mappingAdd' problem          #
  # generation                               #
  #------------------------------------------#
	def self.generate(opts)
		raise ArgumentError.new(:instance) if opts[:instance].nil?
    raise ArgumentError.new(:clauseStrategy) if opts[:clauseStrategy].nil?

    instance = opts[:instance]

		# Generate !
    mappings_hash = {}
		case opts[:clauseStrategy].downcase
      when "random"
        instance.peers.each { |peerName, peer|
          # Randomize mappings order
          peer[:mappings].sort_by{rand}.each { |mapping|
            mappings_hash[mapping] = peerName
          }
        }
      when "in_order"
        instance.peers.each { |peerName, peer|
          # peers will be inserted in order
          peer[:mappings].each { |mapping|
            mappings_hash[mapping] = peerName
          }
        }
      else
        err = "Unknown mappingStrategy option : " + opts[:clauseStrategy]
        $logger.error err
        raise err
    end

    problems = []
    mappings_hash.each { |mapping, peerName|

      problem = GexTools::Problems::Problem.new
      concrete_query = GexTools::Components::SomewhereApi::Problems::ClauseAdd.new
      if ($config[:component].downcase == "somewhere") then
        concrete_query.clause = mapping.join("&")
      else
        concrete_query.clause = mapping.join(" v ")
      end
      concrete_query.modifiedPeers = peerName
      problem.concrete_problem = concrete_query
      problems << problem
    }

		return problems
	end

  #------------------------------------------#
	# Parse 'mappingAdd' problems              #
  #------------------------------------------#
  def self.parse(line)
    line = line.strip
    return if line.nil? or line == "\n"
    
    problem = GexTools::Problems::Problem.new
    concrete_query = GexTools::Components::SomewhereApi::Problems::ClauseAdd.new

    if ($config[:component].downcase == "somewhere") then
      symbol = "&"
    else
      symbol = " v "
    end

    peerNames = []
    line.split(symbol).each { |literal|
      literal =~ /(!?)(.+):(.+)/
      neg = ($1 == "!")
      peer_name = $2
    }

    concrete_query.clause = line.strip
    concrete_query.modifiedPeers = peerNames[0]
    problem.concrete_problem = concrete_query

    return problem
  end

end



end
end
end
end