#
# Gextools Somewhere API - PLogic API (
# https://sourcesup.renater.fr/projects/gextools/ ) - This file is part of
# Somewhere API
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'logger'
require 'rubygems'
require 'active_record'



# Migrate everything
class CreateSomewhereTables < ActiveRecord::Migration

	def self.up
    begin
      return if table_exists? "sw_instances"

      create_table :sw_instances do |t|
        t.column :peersNumber, :int
        t.column :litNumber, :int
        t.column :clauseNumber, :int
        t.column :targetNumber, :int
        t.column :mappingNumber, :int
        t.column :isSat, :boolean, :null => false
      end

      create_table :sw_queries do |t|
        t.column :query, :string, :limit => 45, :null => false
        t.column :queriedPeer, :string, :limit => 45, :null => false
      end

      create_table :sw_clause_add do |t|
        t.column :clause, :string, :limit => 255, :null => false
        t.column :modifiedPeers, :string, :limit => 255, :null => false
      end

      add_index :sw_queries, :queriedPeer
      add_index :sw_clause_add, :modifiedPeers

    rescue
      puts "\nException while migrating the database =>" + $!
      puts "Rolling back transaction...\n"
      self.down
      raise "\nDone rollback."
    end

	end

	def self.down
		drop_table :sw_instances
    drop_table :sw_queries
    drop_table :sw_clause_add
	end

end