#
# Gextools SAT - SAT component ( https://sourcesup.renater.fr/projects/gextools/ )
# - This file is part of Gextools SAT
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'builder'

module GexTools
module Components
module Sat
module Results

require 'gextools/results/result'
require 'gextools/results/result_status'
require 'gextools/results/results'
require 'gextools/problems/problem'
require 'gextools/executions/execution'

# Somewhere Results based object
class Results < GexTools::Results::Results


	#####################
	protected



	#####################
	public

	# Parameters
	@param = nil

	def genCmdLine
		global = Trollop::options do
		end

		@param = global
	end


  #------------------------------------------#
	# Analyze the answers of a somewhere       #
  # execution and build results objects over #
  # it.                                      #
  #------------------------------------------#
	def saveResultsOfProblem(opts={})
    begin
      # Get useful parameters
      execution = opts[:execution]
      raise ArgumentError.new(:execution) if execution.nil?
      content = opts[:result_string]
      raise ArgumentError.new(:result_string) if content.nil?
      problem = opts[:problem]
      raise ArgumentError.new(:problem) if problem.nil?
      instance = problem.problem_set.instance

      # Build the result object
      result = GexTools::Results::Result.new
      result.rawOutput = content
      result.execution = execution
      result.problem = problem
      result.completionTime = 0

      # Build the main xml to be saved as "extractedResult"
      xml = ''
      doc = Builder::XmlMarkup.new(:target=> xml, :indent => 1 )
      doc.instruct! :xml, :encoding => "UTF-8"

      # Root element
      doc.extracted do

        # Get the concerned problem in order to be "query" key
        doc.query(problem.concrete_problem.getConcernedProblem())

        resSatis = (content =~ /s SATISFIABLE/)

        if resSatis.nil?
          doc.sat("false")
          instance.concrete_instance.update_attributes(:isSat => false)
        else
          doc.sat("true")
          instance.concrete_instance.update_attributes(:isSat => true)
        end

        content =~ /CPU time\s+:\s+(\d+\.\d+)/
        result.completionTime = $1 unless $1.nil?

        doc.completion_time = result.completionTime
      end

      result.result_status = GexTools::Results::Results.findSucessResultStatus()
      result.extractedResults =  xml
      result.save
    rescue
      $logger.error "Error building the result for problem : #{$!}"
      raise
    end
  end

  #------------------------------------------#
	# Export results in xml format             #
  #------------------------------------------#
  def exportXML(path, experiment, results)
    begin
      results.each { |result|
        # If the parent dir doesnt yet exists, create it
        pathParent = File.join(path, experiment.name, result.execution.label)
        FileUtils.mkdir_p pathParent unless File.exists? pathParent

        # Creating file name for result
        file_path = File.join(pathParent , "#{result.problem.id}.xml")

        # Write results
        File.open(file_path, "w+") { |file|
          file.write result.extractedResults
        }
      }
    rescue Exception
			$logger.error "There was an error generating results xml files ... #{$!}"
			raise
		end
  end

  #------------------------------------------#
	# Export some graphics related to a set of #
  # somewhere executions                     #
  #------------------------------------------#
  def exportGraphs(path, experiment, execution_type_results={})
    begin
      raise NotImplementedError.new
    rescue
      $logger.info "Glucose component do not have any specific comparison method."
    end
	end


  #------------------------------------------#
	# Compare results for 1, 10, 100 and 1000  #
  # results                                  #
  #------------------------------------------#
  def compare(path, experiment, results, results_to_compare)
    begin
      raise NotImplementedError.new
    rescue
      $logger.info "Glucose component do not have any specific comparison method."
    end
  end

  
	def initialize
		genCmdLine
	end

end



end
end
end
end