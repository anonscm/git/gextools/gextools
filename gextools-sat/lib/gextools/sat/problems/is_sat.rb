#
# Gextools SAT - SAT component ( https://sourcesup.renater.fr/projects/gextools/ )
# - This file is part of Gextools SAT
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

module GexTools
module Components
module Sat
module Problems

require 'gextools/problems/problem'
require 'gextools/problems/concrete_problem'

# Query object for somewhere, as it will be recorded in the database
class IsSat < GexTools::Problems::ConcreteProblem

  self.table_name = "sat_problems"

  #------------------------------------------#
	# Return the parameters to be used when    #
  # posing the problem on a program          #
  #------------------------------------------#
  def getProblemString()
    return "isSat?"
  end

  #------------------------------------------#
	# Return the problem type short name       #
  #------------------------------------------#
  def getProblemTypeName()
    return "IsSat"
  end

  #------------------------------------------#
	# Return the problem concerned peers       #
  #------------------------------------------#
  def getConcernedPeers()
    return NoSuchMethodError.new
  end

  #------------------------------------------#
	# Return the problem concerned             #
  # problem string                           #
  #------------------------------------------#
  def getConcernedProblem()
    return "isSat"
  end

end



end
end
end
end