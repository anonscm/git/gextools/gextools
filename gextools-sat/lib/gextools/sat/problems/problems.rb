#
# Gextools SAT - SAT component ( https://sourcesup.renater.fr/projects/gextools/ )
# - This file is part of Gextools SAT
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#


module GexTools
module Components
module Sat
module Problems

require 'gextools/problems/problem'
require 'gextools/problems/problems'
require 'gextools/problems/problem_set'

require 'gextools/sat/problems/is_sat'


# Somewhere Problems based operation
class Problems < GexTools::Problems::Problems


	#####################
	protected


	#####################
	public

  #------------------------------------------#
	# Deals with the problem set registration  #
  # on the database.                         #
  #------------------------------------------#
	def register(opts={})
    begin
      raise NotImplementedError.new
    rescue
      $logger.info "Glucose component do not have any specific register method."
    end
	end

  #------------------------------------------#
	# Deals with the problem set generation    #
  #------------------------------------------#
	def generate(opts={})
		raise ArgumentError.new(:opts) if opts.nil?

    problem_set = GexTools::Problems::ProblemSet.new
    problem_set.name = opts[:name]
    problem_set.instance = opts[:instance]

    is_sat = IsSat.new
    is_sat.desc = "isSat?"

		problems = []
    problem = GexTools::Problems::Problem.new
    problem.concrete_problem = is_sat
    problems << problem
    problem_set.problems << problems

		return problem_set
	end

end



end
end
end
end