#
# Gextools SAT - SAT component ( https://sourcesup.renater.fr/projects/gextools/ )
# - This file is part of Gextools SAT
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'fileutils'
require 'debugger'

module GexTools
module Components
module Sat
module Experiments

require 'gextools/temp'
require 'gextools/results/results'
require 'gextools/experiments/experiment'
require 'gextools/executions/executions'

require 'gextools/sat/instances/instances'
require 'gextools/sat/problems/problems'
require 'gextools/sat/problems/is_sat'

require 'gextools/somewhere_api/instances/DIMACS_format'
require 'gextools/somewhere_api/instances/SWR_format'

# Experiment object for somewhere, as it will be executed
class Experiment < GexTools::Experiments::Experiment

  #####################
	private

  #------------------------------------------#
	# Validate if the experiment was created   #
  # with a sat solver        #
  # program                                  #
  #------------------------------------------#
  def correct_progs?
    solver = nil
    progs.each { |prog|
      if (prog.prog_type.name == "sat") then
        solver = prog
      end
    }

    unless (progs.size == 1) and (!solver.nil?)
      $logger.error("A Glucose experiment requires one sat solver program.")
      errors.add(:progs, "A Glucose experiment requires one sat solver program.")
    end
  end

  #------------------------------------------#
  # Change the name of the nodes in          #
  #	SomeWhere files to the name of the       #
  #	reserved nodes                           #
  #------------------------------------------#
  def transformsAndSendInstanceFiles(instance, assignedPeers, parameters={})
    begin

      # Only one resource will be used on this operation
      peer_name = nil
      assignedPeers.delete_if {|key, value|
        if peer_name.nil? then
          peer_name = key
          false
        else
          true
        end
      }

      # Apply Transformation to all instances files and transfer them to the grid
      path = nil
      begin
        # Prepare analysis
        instance.after_find_instance
        type = "GexTools::Components::SomewhereApi::Instances"
        eval "path = #{type}::#{instance.instance_chunk_format.format}Format.prepareSatAnalysis(instance)"
      rescue
        $logger.error "Impossible to retrieve DIMACS instance file. Reason : " + $!.inspect
        $logger.error $!.backtrace.join("\n")
        raise
      end

      # Creating instance folder
      instance_folder = getRelativePathInsideAnExperimentFolder("~", :instance, instance.name)
      createFolderCmd = "mkdir -p #{instance_folder}"
      deployment.runCmd(createFolderCmd)

      # Reading generated file contents
      file = File.open(path, "rb")
      content = file.read

      # Sending cnf file to grid
      peer_path = File.join(instance_folder , instance.name + ".cnf")
      copyCmd = "echo '#{content}' > #{peer_path}"
      deployment.runCmd(copyCmd)

    rescue Exception
      raise "There was an error applying changing somewhere files ... #{$!}"
      puts $!.backtrace.join("\n")
    end
  end

  #------------------------------------------#
  # Run program on all nodes                 #
  #------------------------------------------#
  def runSolver(solver, reservationNumber, assignedPeers, execution, problem)
    directory = getRelativePathInsideAnExperimentFolder("~", :nil)

    # Get information about the target instance
    instance = self.problem_set.instance
    instance_folder = getRelativePathInsideAnExperimentFolder("~", :instance, instance.name)

    assignedPeers.each{ |peerName, params|
      machine = params[:machine]

      $logger.info "Running Sat Solver on node : #{machine}..."

      # Get the output directory for this machine
      file_path = File.join(instance_folder, instance.name, "#{instance.name}.cnf")

      # Execute someWhere on this node
      opts = {}
      opts[:cmd] = "#{solver.executionPrefix} #{File.join(directory, "bin", "#{solver.name}")} #{file_path} 2>&1"
      opts[:reservationNumber] = reservationNumber
      opts[:machine] = machine
      resultString = deployment.runCmdOnNode(opts)


      getResults(0, problem, execution, resultString, params)
    }

    sleep 3
  end

	#####################
	public

  #------------------------------------------#
	# Execute this experiment!!!               #
  #------------------------------------------#
  def execute(reservationID, assignedPeers, execution, parameters)

    # Getting used programs
    solver = nil
    progs.each { |prog|
      if (prog.prog_type.name == "sat") then
        solver = prog
      end
    }

    # Get associated problem
    problem = problem_set.problems.first
    
    # Run solver and get results
    runSolver(solver, reservationID, assignedPeers, execution, problem)
	end

end


end
end
end
end