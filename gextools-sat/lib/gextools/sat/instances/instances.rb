#
# Gextools SAT - SAT component ( https://sourcesup.renater.fr/projects/gextools/ )
# - This file is part of Gextools SAT
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'fileutils'

module GexTools
module Components
module Somewhere2
module Instances

require 'gextools/instances/instances'
require 'gextools/instances/instance'
require 'gextools/instances/instance_generated'
require 'gextools/progs/Progs'
require 'gextools/temp'

require 'gextools/somewhere_api/instances/SWR_format'
require 'gextools/somewhere_api/instances/SW_instance'

require 'gextools/somewhere_api/logic/theory'
require 'gextools/somewhere_api/logic/targets'


# Somewhere Instances based operation
class Instances < GexTools::Instances::Instances
  
	#####################
	protected



	#####################
	public


  #------------------------------------------#
	# Deals with the instance registration on  #
  # the database.                            #
  #------------------------------------------#
	def register(param={})
		raise NotImplementedError.new
	end


  #------------------------------------------#
	# Deals with the instance generation       #
  #------------------------------------------#
	def generate(param={})
		raise NotImplementedError.new
	end

end



end
end
end
end