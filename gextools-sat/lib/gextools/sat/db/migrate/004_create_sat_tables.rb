#
# Gextools SAT - SAT component ( https://sourcesup.renater.fr/projects/gextools/ )
# - This file is part of Gextools SAT
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'logger'
require 'rubygems'
require 'active_record'



# Migrate everything
class CreateSatTables < ActiveRecord::Migration

	def self.up
    begin
      return if table_exists? "sat_problems"

      create_table :sat_problems do |t|
        t.column :desc, :string, :limit => 45, :null => false
      end
      
    rescue
      puts "\nException while migrating the database =>" + $!
      puts "Rolling back transaction...\n"
      self.down
      raise "Done rollback."
    end
	end

	def self.down
		drop_table :sat_problems
	end

end