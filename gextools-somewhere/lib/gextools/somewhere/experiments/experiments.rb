#
# Gextools Somewhere - Somewhere component (
# https://sourcesup.renater.fr/projects/gextools/ ) - This file is part of
# Gextools Somewhere
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

module GexTools
module Components
module Sat
module Experiments

require 'gextools/experiments/experiments'
require 'gextools/progs/progs'

require 'gextools/sat/experiments/experiment'


# Somewhere experiments based operation
class Experiments < GexTools::Experiments::Experiments


	#####################
	protected



	#####################
	public


	# Parameters
	@param = nil

	def genCmdLine
    global = case $config[:command]
      when "register"
        Trollop::options do
          opt :solverName, "Name of the sat solver to use", :type => String, :short => '-s', :required => true
          opt :solverVersion, "Version of the sat solver to use", :type => String, :short => '-S', :required => true
          stop_on_unknown
        end
      when "execute"
        Trollop::options do
          opt :duplicateMappings, "Duplicate the mappings in the instance", :short => '-m', :default => true
          opt :deleteMappings, "If you want to ignore the mappings of the instance", :short => '-d', :default => false
          opt :ttl, "Time to leave of current problem propagations", :type => :int, :short => '-t', :default => 30
          opt :bl, "Bounded lenghts: maximum number of literals on the result", :type => :int, :short => '-b'
          opt :wfDeca, "Use wfDeca", :short => '-w'
          opt :cache, "Use cache", :short => '-c', :required => false
          stop_on_unknown
        end
    end

		@param = global
	end

  #------------------------------------------#
	# Deals with the creation of an experiment #
	# object                          				 #
  #------------------------------------------#
	def generate(param={})
		raise ArgumentError.new(:param) if param.nil?

    opts = @param.merge(param)

		experiment = Experiment.new
		experiment.name = opts[:name]
		experiment.problem_set = opts[:problem_set]
    experiment.deployment = opts[:deployment]

    solver = GexTools::Progs::Progs.findWantedProg(opts[:solverName], opts[:solverVersion])
		experiment.progs << solver

		return experiment
	end

  #------------------------------------------#
	# Returns used execution parameters        #
  #------------------------------------------#
  def getParameters()
    return @param
  end

	def initialize
		genCmdLine
	end

end



end
end
end
end