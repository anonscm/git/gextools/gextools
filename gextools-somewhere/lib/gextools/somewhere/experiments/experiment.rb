#
# Gextools Somewhere - Somewhere component (
# https://sourcesup.renater.fr/projects/gextools/ ) - This file is part of
# Gextools Somewhere
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'fileutils'
require 'debugger'

module GexTools
module Components
module Somewhere
module Experiments

require 'gextools/temp'
require 'gextools/results/results'
require 'gextools/experiments/experiment'
require 'gextools/executions/executions'
require 'gextools/executions/helper/thread_pool'
require 'gextools/experiments/helper/schedule'

require 'gextools/somewhere/instances/instances'
require 'gextools/somewhere/problems/problems'

require 'gextools/somewhere_api/problems/query'
require 'gextools/somewhere_api/problems/clause_add'

# Experiment object for somewhere, as it will be executed
class Experiment < GexTools::Experiments::Experiment

  #####################
	private

  #------------------------------------------#
	# Validate if the experiment was created   #
  # with an answerManager and a solver       #
  # program                                  #
  #------------------------------------------#
  def correct_progs?
    solver = nil
    answerManager = nil
    progs.each { |prog|
      if (prog.prog_type.name == "solver" and solver.nil?) then
        solver = prog
      elsif (prog.prog_type.name == "answerManager" and answerManager.nil?) then
        answerManager = prog
      end
    }

    unless (progs.size == 2) and (!solver.nil?) and (!answerManager.nil?)
      $logger.error("A Somewhere experiment requires one solver program and one answerManager.")
      errors.add(:progs, "A Somewhere experiment requires one solver program and one answerManager.")
    end
  end

  #------------------------------------------#
  # Change the name of the nodes in          #
  #	SomeWhere files to the name of the       #
  #	reserved nodes                           #
  #------------------------------------------#
  def transformsAndSendInstanceFiles(instance, assignedPeers, parameters={})
    begin
      $logger.info("There are #{instance.instanceChunks.size} SomeWhere files to update.")

      # Apply Transformation to all instances files and transfer them to the grid
      outputdirs = {}
      chuncks = Hash[instance.instanceChunks.map {|chunk| [chunk.name, chunk.chunkFile]}]
      
      # Duplicating or eliminating mappings
      chuncks.each { |chunk_name, content|
        # If required, delete all or duplicate mappings from the instance
        if parameters[:deleteMappings] then
          content = instance.concrete_instance.deleteMappingsFromChunk(content)
        elsif parameters[:duplicateMappings] then
          instance.concrete_instance.duplicateMappingsInChunk(chunk_name, content, chuncks)
        end
      }

      # Adding host information
      chuncks.each { |chunk_name, content|
        $logger.info("Applying transformation on file : #{chunk_name}")

        # Create an array of replacement parameters to be used by the awk command
        assignedPeers.each { |peerName, params|
          machine = params[:machine]
          port = params[:port]

          # Set the correct final part of the output dir
          outputdirs[chunk_name] = "#{machine}_#{port}" if peerName == chunk_name

          # Do the transformation on the output file content
          # When using the 1.9 version of ruby, use the 'negative lookbehind' feature on regular expressions : (?<!:)
          content = content.gsub(/^#{peerName}([:$\s])/, "#{machine}:#{port}:#{peerName}" + '\1');
          content = content.gsub(/ #{peerName}([:$\s])/, " #{machine}:#{port}:#{peerName}" + '\1');
          content = content.gsub(/!#{peerName}([:$\s])/, "!#{machine}:#{port}:#{peerName}" + '\1');
        }

        instance_folder = getRelativePathInsideAnExperimentFolder("~", :instance, outputdirs[chunk_name])
        createFolderCmd = "mkdir -p #{instance_folder}"
        @deployment_to_use.runCmd(createFolderCmd)

        renamed_peer_path = File.join(instance_folder , chunk_name + "." + instance.instance_chunk_format.extension)
        copyCmd = "echo '#{content}' > #{renamed_peer_path}"
        @deployment_to_use.runCmd(copyCmd)
      }

    rescue Exception
      raise "There was an error applying changing somewhere files ... #{$!}"
      puts $!.backtrace.join("\n")
    end
  end

  #------------------------------------------#
  # Run a problem on the grid                #
  #------------------------------------------#
	def runProblem(answerManager, id, problem, reservationNumber, assignedPeers, expExec, parameters)
    # Get parameters
    ttl = parameters[:ttl]
    bl = parameters[:bl]
    wf = parameters[:wfDeca]

    # Get the infos of the peer from the first literal in order to set the jar execution parameters.
    # We will need to do it again in order to correct the parameters of the problem string.
    directory = getRelativePathInsideAnExperimentFolder("~", :nil)
    machine = nil
    port = nil
    assignedPeers.each { |peerName, params|
      if (problem.concrete_problem.getConcernedPeers.include?(peerName)) then
        machine = params[:machine]
        port = params[:port]
        break
      end
    }

    # Build problem string
    opts = {}
    problem_port = 60000 + id
    opts[:cmd] = "#{answerManager.executionPrefix} #{File.join(directory, "bin", "#{answerManager.name}")} -h #{machine} -p #{problem_port} "
    opts[:cmd] += " -wfdeca 1 " if (!wf.nil? and problem.concrete_problem.getProblemTypeName() == "query")
    opts[:cmd] += " -ttl #{ttl} " unless ttl.nil?

    # Concrete problem string
    if problem.concrete_problem.getProblemTypeName == "query" then
      query = problem.concrete_problem.getConcernedProblem()
      queried_peer = problem.concrete_problem.getConcernedPeers().first
      $logger.info "Querying literal #{}..." if $verbose

      opts[:cmd] += " -query #{query} #{queried_peer} #{machine} #{port}"
    elsif problem.concrete_problem.getProblemTypeName == "clauseAdd" then
      assigned_mapping = problem.concrete_problem.getConcernedProblem()
      tmp_modified_peers = problem.concrete_problem.getConcernedPeers()
      assigned_modified_peers = problem.concrete_problem.getConcernedPeers().join("&")
      assignedPeers.each { |peerName, params|
        if (tmp_modified_peers.include?(peerName)) then
          machine = params[:machine]
          port = params[:port]

          # When using the 1.9 version of ruby, use the 'negative lookbehind' feature on regular expressions : (?<!:)
          assigned_mapping = assigned_mapping.gsub(/^#{peerName}:/, "#{machine}:#{port}:#{peerName}:")
          assigned_mapping = assigned_mapping.gsub(/!#{peerName}:/, "!#{machine}:#{port}:#{peerName}:")
          assigned_mapping = assigned_mapping.gsub(/ #{peerName}:/, " #{machine}:#{port}:#{peerName}:")
          assigned_mapping = assigned_mapping.gsub(/&#{peerName}:/, "&#{machine}:#{port}:#{peerName}:")

          assigned_modified_peers = assigned_modified_peers.gsub(/^#{peerName}/, "#{machine}:#{port}:#{peerName}")
          assigned_modified_peers = assigned_modified_peers.gsub(/&#{peerName}&/, "&#{machine}:#{port}:#{peerName}&")
          assigned_modified_peers = assigned_modified_peers.gsub(/&#{peerName}$/, "&#{machine}:#{port}:#{peerName}")
        end
      }

      $logger.info "Adding mapping '#{assigned_mapping}' on peers '#{assigned_modified_peers}'" if $verbose

      opts[:cmd] = " -add #{assigned_mapping}"
      opts[:cmd] += " -where #{assigned_modified_peers}"
      opts[:cmd] += " -query !a0 P0 localhost 50000 "
    end

    opts[:cmd] += " -lb #{bl.to_s} " unless bl.nil?

    opts[:cmd].gsub!(/&/, "\\\\&")
		opts[:cmd].gsub!(/!/, "\\!")

    opts[:reservationNumber] = reservationNumber
    opts[:machine] = machine

    resultString = @deployment_to_use.runCmdOnNode(opts)

    # Fetch the results from the grid
    params = {}
    params[:ttl] = ttl
    getResults(id, problem, expExec, resultString, params)
	end
  

  #------------------------------------------#
  # Run problems following an execution type #
  #------------------------------------------#
	def runProblemSet(answerManager, reservationNumber, assignedPeers, expExec, parameters)
		begin
      $logger.info("## Doing problems set named " + problem_set.name)

			$logger.info("--------------- Queries Run Output --------------") if $verbose

      if (expExec.execution_type.name.upcase == "SEQUENTIAL") then
        order = 0
        problem_set.problems.each { |problem|
          runProblem(answerManager, order, problem, reservationNumber, assignedPeers, expExec, parameters)
          order += 1
        }
      elsif (expExec.execution_type.name.upcase == "CONCURRENT") then
        threads = GexTools::Executions::Helper::ThreadPool.new(10)
        problem_set.problems.each { |problem|
          threads.execute{
            runProblem(answerManager, threads.waiting, problem, reservationNumber, assignedPeers, expExec, parameters)
          }
        }
        threads.join
        threads.close
      end

			$logger.info("----------------- Output End -----------------") if $verbos
		rescue
			$logger.info("----------------- Output End -----------------") if $verbose
			$logger.error "Error during queries sets : #{$!}"
			raise
		end
	end

  #------------------------------------------#
  # Run program on all nodes                 #
  #------------------------------------------#
  def runSolver(solver, reservationNumber, assignedPeers, parameters)
    cache = parameters[:cache]
    directory = getRelativePathInsideAnExperimentFolder("~", :nil)
    outputDirectory = File.join directory, "instance"

    # Create log directory
    log_folder = getRelativePathInsideAnExperimentFolder("~", :log)
    createFolderCmd = "mkdir -p #{log_folder}"
    @deployment_to_use.runCmd(createFolderCmd)

    running_instances = []
    assignedPeers.each{ |peerName, params|
      machine = params[:machine]
      port = params[:port]

      unless running_instances.include?("#{machine}_#{port}")
        $logger.info "Running SomeWhere on node : #{machine}, port #{port}..."

        # Get the output directory for this machine
        dir = File.join outputDirectory, "#{machine}_#{port}"

        # Execute someWhere on this node
        opts = {}
        opts[:cmd] = "#{solver.executionPrefix} #{File.join(directory, "bin", "#{solver.name}")} -path #{dir} -m #{machine} -p #{port}"
        opts[:cmd] += " -cache" unless cache.nil?
        opts[:cmd] += " &> #{File.join(log_folder, "logSomeWhere_#{machine}_#{port}.log")} & disown"
        opts[:reservationNumber] = reservationNumber
        opts[:machine] = machine
        @deployment_to_use.runCmdOnNode(opts)
        
        running_instances.push("#{machine}_#{port}")
      end
    }

    sleep 3
  end

	#####################
	public

  #------------------------------------------#
	# Execute this experiment!!!               #
  #------------------------------------------#
  def execute(reservationID, assignedPeers, execution, parameters)

    # Getting used programs
    solver = nil
    answerManager = nil
    progs.each { |prog|
      if (prog.prog_type.name == "solver" and solver.nil?) then
        solver = prog
      elsif (prog.prog_type.name == "answerManager" and answerManager.nil?) then
        answerManager = prog
      end
    }
    
    # Run solver
    runSolver(solver, reservationID, assignedPeers, parameters)

    # Run related problem set
    runProblemSet(answerManager, reservationID, assignedPeers, execution, parameters)
	end

end


end
end
end
end