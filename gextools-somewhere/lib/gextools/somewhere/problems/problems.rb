#
# Gextools Somewhere - Somewhere component (
# https://sourcesup.renater.fr/projects/gextools/ ) - This file is part of
# Gextools Somewhere
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#


module GexTools
module Components
module Somewhere
module Problems

require 'gextools/problems/problem'
require 'gextools/problems/problems'
require 'gextools/problems/problem_set'

require 'gextools/somewhere_api/problems/gen_queries'
require 'gextools/somewhere_api/problems/gen_clause_add'
require 'gextools/somewhere_api/problems/query'
require 'gextools/somewhere_api/problems/clause_add'


# Somewhere Problems based operation
class Problems < GexTools::Problems::Problems


	#####################
	protected


	#####################
	public

	# Parameters
	@param = nil

	def genCmdLine
		global = Trollop::options do
			opt :nbQueries, "Number of queries to generate", :type => String, :short => '-q'
			opt :probabilityOfNegativeQueries, "Probability of negative queries generated", :type => :float, :short => '-p', :default => 0.50
			opt :targetVariablesAreInterresting, "Add targets variables in queries", :type => :bool, :short => '-v', :default => false
			opt :subtype, "Subtype of the query : QUERY or CLAUSEADD", :type => :string, :short => '-t', :required => false, :default => "QUERY"
			opt :clauseStrategy, "Strategy for creating CLAUSEADD", :type => :string, :short => '-S', :required => false, :default => "random"
			opt :nbMinLiteralsPerQuery, "Minimal number of literals per query", :short => '-l', :default => 1
			opt :nbMaxLiteralsPerQuery, "Maximal number of literals per query", :short => '-L', :default => 2
		end

		@param = global
	end

  #------------------------------------------#
	# Deals with the problem set registration  #
  # on the database.                         #
  #------------------------------------------#
	def register(opts={})
    raise ArgumentError if opts.nil?

		param = opts.merge(@param)

    instance = param[:instance]
    problem_set_path = param[:path]
    raise "Error in query path" if problem_set_path.nil? or problem_set_path == ""
    raise "Bad query path" if !File.exists?(problem_set_path)
    raise "Your query path is a directory  " if File.directory?(problem_set_path)

    # building problem set
    problem_set = GexTools::Problems::ProblemSet.new
    problem_set.name = param[:name]
    problem_set.instance = instance

    File.open(problem_set_path, "r") { |file|
      file.readlines.each { |line|
        subtype = line.split(" ")[0]
        line = line.sub(/#{subtype}/i, "").strip

        case subtype.upcase
          when "QUERY", nil, ""
            problem = GexTools::Components::SomewhereApi::Problems::GenQueries.parse(line)

          when "CLAUSEADD"
            problem = GexTools::Components::SomewhereApi::Problems::GenClauseAdd.parse(line)

          else
            $logger.error("Invalid problem subtype : " + subtype)
        end

        # Check that literals in problems belong to the instance language
        logic_problem_str = problem.concrete_problem.getConcernedProblem()
        peer_str = problem.concrete_problem.getConcernedPeers()

        logic_problem_str.split("&") { |litName|
          peer_str.split("&") { |peerName|
            peer = instance.peers[peerName]
            if !peer[:variables].include?(litName) then
             $logger.error("Invalid literal name (`#{litName}` in problem set `#{problem_set.name}`")
             raise
            end
          }
        }

        problem_set.problems << problem
      }
    }

    $logger.info "Registering problem set '#{opts[:name]}' ..."

		return problem_set
	end

  #------------------------------------------#
	# Deals with the problem set generation    #
  #------------------------------------------#
	def generate(opts={})
		raise ArgumentError.new(:opts) if opts.nil?

		param = opts.merge(@param)

    problem_set = GexTools::Problems::ProblemSet.new
    problem_set.name = param[:name]
    problem_set.instance = param[:instance]

    problems = nil
		case param[:subtype].upcase
			when "QUERY", nil, ""
				problems = GexTools::Components::SomewhereApi::Problems::GenQueries.generate(param)

			when "CLAUSEADD"
				problems = GexTools::Components::SomewhereApi::Problems::GenClauseAdd.generate(param)

			else
				$logger.error("Invalid query subtype : " + param[:subtype])
		end

    problems.each{ |problem|
      problem.problem_set = problem_set
    }

    problem_set.problems << problems

		return problem_set
	end

  #------------------------------------------#
	# Deals with the problems export           #
  #------------------------------------------#
  def export(newpath, problems)
    File.open(newpath, "w") { |file|
      problems.each { |problem|
        file.write "#{problem.concrete_problem.getProblemTypeName().upcase} #{problem.concrete_problem.getProblemString()}\n"
      }
    }
  end

  def initialize
		genCmdLine
	end

end



end
end
end
end