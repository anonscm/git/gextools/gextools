require 'test/unit'
require 'test/IntegrationTestHelper'

class BLTest < Test::Unit::TestCase

  @@source_folder = "src"

  def testBL_0
    # Deleting previous executions of this experiment
    system "cd #{@@source_folder};./gexExperiments.rb -v -n integrationTestBL delete -l bl_0"

    # Execute experiment again
    system "cd #{@@source_folder};./gexExperiments.rb -v -n integrationTestBL execute -l bl_0 somewhere -b 0"
    system "cd #{@@source_folder};./gexResults.rb -v -n integrationTestBL export -f XML"

    comparation_result = IntegrationTestHelper.compare("integrationTestBL", "bl_0")

    assert(comparation_result)
  end

  def testBL_1
    # Execute experiment again
    system "cd #{@@source_folder};./gexExperiments.rb -v -n integrationTestBL execute -l bl_1 somewhere -b 1"
    system "cd #{@@source_folder};./gexResults.rb -v -n integrationTestBL export -f XML"

    comparation_result = IntegrationTestHelper.compare("integrationTestBL", "bl_1")

    assert(comparation_result)
  end

  def testBL_2
    # Deleting previous executions of this experiment
    system "cd #{@@source_folder};./gexExperiments.rb -v -n integrationTestBL delete -l bl_2"

    # Execute experiment again
    system "cd #{@@source_folder};./gexExperiments.rb -v -n integrationTestBL execute -l bl_2 somewhere -b 2"
    system "cd #{@@source_folder};./gexResults.rb -v -n integrationTestBL export -f XML"

    comparation_result = IntegrationTestHelper.compare("integrationTestBL", "bl_2")

    assert(comparation_result)
  end

  def testBL_3
    # Deleting previous executions of this experiment
    system "cd #{@@source_folder};./gexExperiments.rb -v -n integrationTestBL delete -l bl_3"

    # Execute experiment again
    system "cd #{@@source_folder};./gexExperiments.rb -v -n integrationTestBL execute -l bl_3 somewhere -b 3"
    system "cd #{@@source_folder};./gexResults.rb -v -n integrationTestBL export -f XML"

    comparation_result = IntegrationTestHelper.compare("integrationTestBL", "bl_3")

    assert(comparation_result)
  end

end
