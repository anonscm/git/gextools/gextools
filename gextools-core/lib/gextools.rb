#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'logger'


module GexTools


$logger = Logger.new(STDOUT)

# The program's version
$VERSION = "1.0"

# The program's version String
$VERSIONSTR = "GexTools version #{$VERSION}"

#------------------------------------------#
# Use a file as log file                   #
#------------------------------------------#
def confLogger(path)
	begin
		file = File.open(path, File::WRONLY | File::APPEND)
		logger = Logger.new(file)
	rescue Exception => e
		puts "Cannot use file #{path} as log file"
		exit 1
	end
end


end