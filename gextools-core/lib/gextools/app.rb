#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'logger'
require 'rubygems'
require 'trollop'
require 'mysql2'
require 'yaml'


module GexTools


require 'gextools/db/db_connection_builder'
require_relative 'components_manager'

# Abstract gexTools applications
class App

	#####################
	protected 

  #------------------------------------------#
	# Creates a BD Connection                  #
  #------------------------------------------#
	def createBDConnection
		@db = GexTools::DB::DBConnectionBuilder::create
	end

  #------------------------------------------#
	# Do the actions specified by the command  #
  # line                                     #
  #------------------------------------------#
	def doCmd
		raise NotImplementedError.new
	end


  #------------------------------------------#
	# Create a configuration from command line #
  #  parameter and/or a config file          #
  #------------------------------------------#
	def createConfig
    database_env = ENV['DATABASE_ENV'] || 'development'
    $dbConfig = {}

		$config = YAML::load(File.open($opts[:componentConfigFile])) if File.exists?($opts[:componentConfigFile]) and File.readable?($opts[:componentConfigFile]) unless $opts[:componentConfigFile].nil?
		$dbConfig = YAML::load(File.open($opts[:databaseConfigFile])) if File.exists?($opts[:databaseConfigFile]) and File.readable?($opts[:databaseConfigFile]) unless $opts[:databaseConfigFile].nil?

    $dbConfig = $dbConfig[database_env] unless $dbConfig.empty?

    $dbConfig = $dbConfig.formatKeys
    $config.mergeWithoutNullElem!( $dbConfig)
    $config.mergeWithoutNullElem!( $cmdOpts )
    $config.mergeWithoutNullElem!( $opts )
      
    $config[:command] = $cmd unless $cmd.nil? or $cmd == ''
    $config[:type] = $instanceType unless $instanceType.nil? or $instanceType == ''

    $verbose = $config[:verbose]

    $logger.level = Logger::INFO unless $verbose

    ComponentsManager.includeComponents
	end



	#####################
	public 

	def initialize
		@db = nil # Database connection

		createConfig
		verifyConfig
		createBDConnection
		res = doCmd
		Process.exit res
	end
end



end


class Hash

  def formatKeys
    items = {}

		self.each{ |k, v|
      sym = k.respond_to?(:to_sym) ? k.to_sym : k
			items[sym] = v unless k.to_s.include?(":")
      self.delete(k) unless k == sym
		}

    return items
	end

	def mergeWithoutNullElem!(other_hash)
		other_hash.each{ |k,v|
			self[k] = v if !v.nil? and self[k].nil?
		}
	end

	def mergeWithoutNullElem(other_hash)
		res = self.clone
		res.mergeWithoutNullElem!(other_hash)
		return res
	end


end