#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

module GexTools
module Results

require 'gextools/operation'
require 'gextools/results/result'
require 'gextools/results/result_status'
require 'gextools/experiments/experiments'
require 'gextools/executions/execution'
require 'gextools/executions/execution_type'
require 'gextools/results/helper/graphs_generator'
require 'gextools/components_manager'

# Results based operation
class Results < Operation

	#####################
	protected


	#####################
	public


  #------------------------------------------#
	# Delegates the results informations       #
  # retrieving to the concrete grid          #
  # component                                #
  #------------------------------------------#
	def self.retreive(opts={})
		# Get the class of the results manager of the component
    deployment = opts[:deployment]
    deploymentType = deployment.deployment_type.name

		begin
			managerClass = ComponentsManager.managerClass(deploymentType, :results)
		rescue
			$logger.fatal "Impossible to find results manager class of component '#{deploymentType}'"
			raise
		end

		# Process the results
		begin
			eval "#{managerClass}.new.retreive(opts)"
		rescue
			$logger.fatal "Impossible to process results with options : '#{opts.inspect}'"
			raise
		end
	end


  #------------------------------------------#
	# Delegates the results informations       #
  # storing to the concrete grid             #
  # component                                #
  #------------------------------------------#
	def self.saveResultsOfProblem(opts={})
		# Get the class of the results manager of the component
		begin
			managerClass = ComponentsManager.managerClass(opts[:component], :results)
		rescue
			$logger.fatal "Impossible to find results manager class of component '#{opts[:component]}'"
			raise
		end

		# Process the results
		begin
			eval "#{managerClass}.new.saveResultsOfProblem(opts)"
		rescue
			$logger.fatal "Impossible to process results with options : '#{opts.inspect}'"
			raise
		end
	end

  #------------------------------------------#
	# Main result fetch method                 #
  #------------------------------------------#
	def self.findResult(opts={})

		param = opts.clone
		param.delete :component

		results = Result.find(:all, :conditions => param)
		return results
	end

  #------------------------------------------#
	# Result fetch method providing an index   #
  # and a type                               #
  #------------------------------------------#
	def self.findResultOfIndex(index, type)

		begin
			results = findResult(:id => index)
		rescue
			err = "Impossible to find wanted results #{$!.inspect}."
			$logger.error err
			raise Exception.new(err)
		end

		if results.length < 1
			$logger.error "Haven't found any results of id '#{index}'."
			raise Exception.new
		elsif results.length == 1
			return results[0]
		else
			$logger.error "There are more than 1 results of id '#{index} ! Database corrupted!"
			raise Exception.new
		end
	end

  #------------------------------------------#
	# Result fetch method providing an         #
  # experiment name and a its type           #
  #------------------------------------------#
  def self.findResultsByExperimentName(name, component)

		begin
      experiment = GexTools::Experiments::Experiments.findWantedExperiment(name, component)
			results = experiment.results
		rescue
			err = "Impossible to find wanted results #{$!.inspect}"
			$logger.error err
			raise Exception.new(err)
		end

		if results.length < 1
			$logger.error "Haven't found any results with Experiment name '#{name}'."
			raise Exception.new
    end

		return results
	end

  #------------------------------------------#
	# Result fetch method providing an         #
  # experiment name and, its type and an     #
  # execution label                          #
  #------------------------------------------#
  def self.findResultsByExperimentNameAndExecutionLabel(name, type, label)
    
		begin
      results = findResultsByExperimentName(name, type)
			results.delete_if { |result| result.execution.label != label }
		rescue
			err = "Impossible to find wanted results #{$!.inspect}"
			$logger.error err
			raise Exception.new(err)
		end

    if results.length < 1
			$logger.info "Haven't found any information for experiment '#{name}' with execution label '#{label}'."
    end

    return results
	end

  #------------------------------------------#
	# Fetches the "success" execution status   #
  # object                                   #
  #------------------------------------------#
  def self.findSucessResultStatus()
		param = {}
    param[:status] = "success"

		result_status = ResultStatus.find(:all, :conditions => param)
		return result_status[0]
	end

  #------------------------------------------#
	# Fetches the "fail" execution status   #
  # object                                   #
  #------------------------------------------#
  def self.findFailResultStatus()
		param = {}
    param[:status] = "fail"

		result_status = ResultStatus.find(:all, :conditions => param)
		return result_status[0]
	end

  #------------------------------------------#
	# Fetches the "timeout" execution status   #
  # object                                   #
  #------------------------------------------#
  def self.findTimeoutResultStatus()
		param = {}
    param[:status] = "timeout"

		result_status = ResultStatus.find(:all, :conditions => param)
		return result_status[0]
	end

  #------------------------------------------#
	# Obtains infos about results on the       #
  # database                                 #
  #------------------------------------------#
	def self.infos(opts={})
    puts "\n==================================================="

    if (!opts[:experiment].nil? and !opts[:executionLabel].nil?) then

      puts "Results for execution '#{opts[:executionLabel]}' on experiment '#{opts[:experiment]}'"

      findResultsByExperimentNameAndExecutionLabel(opts[:experiment],
        opts[:component],
        opts[:executionLabel]).each{ |elem|
          puts elem.infos
      }
    elsif (!opts[:experiment].nil?) then

      puts "Results for all executions made on experiment '#{opts[:experiment]}'"

      findResultsByExperimentName(opts[:experiment],
        opts[:component]).each{ |elem|
          puts elem.infos
      }
    end
	end

  #------------------------------------------#
	# Export results to a given format         #
  #------------------------------------------#
	def self.export(opts={})
		results = findResultsByExperimentName(opts[:experiment], opts[:component])
    experiment = GexTools::Experiments::Experiments.findWantedExperiment(opts[:experiment], opts[:component])

    # Get the class of the results manager of the component
		begin
			managerClass = ComponentsManager.managerClass(opts[:component], :results)
		rescue
			$logger.fatal "Impossible to find results manager class of component '#{opts[:component]}'"
			raise
		end

		case opts[:format].upcase
			when "LATEX"
        path = File.join(opts[:path], "latex")
				self.exportLatex(managerClass, path, experiment, results)
      when "GRAPHS"
        path = File.join(opts[:path], "graphs")
				self.exportGraphs(managerClass, path, experiment, results)
      when "XML"
        path = File.join(opts[:path], "xml")
				self.exportXML(managerClass, path, experiment, results)
			else
				$logger.error "Unknown format : #{opts[:format]}"
        raise
		end

		$logger.info "Successfully exported result(s) into '#{opts[:format]}' format."
	end
  
  #------------------------------------------#
	# Export results in latex format TODO      #
  #------------------------------------------#
	def self.exportLatex(managerClass, path, experiment, results)
		raise ArgumentError.new(:path) if path.nil?
		raise ArgumentError.new(:results) if results.nil?
    raise ArgumentError.new(:experiment) if experiment.nil?

		# Open the files, and write results in latex format
		begin
			eval "#{managerClass}.new.exportLatex(path, experiment, results)"
		rescue Exception
			$logger.error "There was an error exporting results into file #{path} ..."
			raise
		end
	end

  #------------------------------------------#
	# Export results in xml format             #
  #------------------------------------------#
  def self.exportXML(managerClass, path, experiment, results)
		raise ArgumentError.new(:path) if path.nil?
		raise ArgumentError.new(:results) if results.nil?
    raise ArgumentError.new(:experiment) if experiment.nil?

		# Open the files, and write results in xml format
		begin
			eval "#{managerClass}.new.exportXML(path, experiment, results)"
		rescue Exception
			$logger.error "There was an error exporting results into file #{path} ..."
			raise
		end
	end
  
  #------------------------------------------#
	# Export graphical representation of       #
  # results                                  #
  #------------------------------------------#
  def self.exportGraphs(managerClass, path, experiment, results)
		raise ArgumentError.new(:path) if path.nil?
		raise ArgumentError.new(:results) if results.nil?
    raise ArgumentError.new(:experiment) if experiment.nil?

		begin
      # Get results obtained from different types of executions
      execution_type_results = {}
      execution_types = GexTools::Executions::ExecutionType.find(:all) #joins(:executions => :experiment).where(:executions => {:experiment_id => experiment.id})
      execution_types.each { |execution_type|

        success_results = Result.joins(:result_status, :execution => :execution_type).
                          where(:result_statuses => {:status => "SUCCESS"}, 
                                :executions => {:experiment_id => experiment.id, :execution_type_id => execution_type.id})

        timeout_results = Result.joins(:result_status, :execution => :execution_type).
                          where(:result_statuses => {:status => "TIMEOUT"},
                                :executions => {:experiment_id => experiment.id, :execution_type_id => execution_type.id})

        fail_results = Result.joins(:result_status, :execution => :execution_type).
                          where(:result_statuses => {:status => "FAIL"},
                                :executions => {:experiment_id => experiment.id, :execution_type_id => execution_type.id})

        execution_type_results[execution_type] = {
          :success_results => success_results,
          :timeout_results => timeout_results,
          :fail_results => fail_results}
      }

      # Create graphs folder
      child_path = File.join(path, experiment.name)
      FileUtils.mkdir_p child_path unless File.exists? child_path

      $logger.info "Generating default graphs..."
      GexTools::Results::Helper::GraphsGenerator.averageTimeGraphByExecutionType(child_path, experiment, execution_type_results)
      GexTools::Results::Helper::GraphsGenerator.medianTimeGraphByExecutionType(child_path, experiment, execution_type_results)
      GexTools::Results::Helper::GraphsGenerator.executionsStatusGraph(child_path, experiment, execution_type_results)

      $logger.info "Generating component specific graphs..."
      eval "#{managerClass}.new.exportGraphs(path, experiment, execution_type_results)"
		rescue Exception
			$logger.error "There was an error exporting results into graphs ..."
			raise
		end
	end

  #------------------------------------------#
	# Compare the results of some experiments  #
  #------------------------------------------#
	def self.compare(opts={})
    base_experiment = GexTools::Experiments::Experiments.findWantedExperiment(opts[:experiment], opts[:component])

    # Array of elements to be compared
    elements_to_compare = []

    # Create hash of results by experiments/executions.
    if opts[:executions].nil? then
      # Search results of experiments/executions to be compared
      to_compare = {}
      to_compare[:experiment] = base_experiment
      to_compare[:results] =  findResultsByExperimentName(opts[:experiment], opts[:component])
      elements_to_compare.push(to_compare)

      opts[:experimentsToCompare].each{ |experimentName|
        to_compare = {}
        to_compare[:experiment] = GexTools::Experiments::Experiments.findWantedExperiment(experimentName, opts[:component])
        to_compare[:results] =  findResultsByExperimentName(experimentName, opts[:component])
        elements_to_compare.push(to_compare)
      }
    else
      opts[:executions].each{ |executionLabel|
        to_compare = {}
        to_compare[:experiment] = GexTools::Experiments::Experiments.findWantedExperiment(opts[:experiment], opts[:component])
        to_compare[:executionLabel] = executionLabel
        to_compare[:results] = findResultsByExperimentNameAndExecutionLabel(opts[:experiment], opts[:component], executionLabel)
        elements_to_compare.push(to_compare)
      }
    end

    # Do basic comparisons
    $logger.info "Generating basic comparisons..."
    GexTools::Results::Helper::GraphsGenerator.compareAverageTime(opts[:path], elements_to_compare)
    GexTools::Results::Helper::GraphsGenerator.compareMedianTime(opts[:path], elements_to_compare)

    # Get the class of the results manager of the component
		begin
			managerClass = ComponentsManager.managerClass(opts[:component], :results)
		rescue
			$logger.fatal "Impossible to find results manager class of component '#{opts[:component]}'"
			raise
		end

		# Do component specific comparisons, and write results in xml format
		begin
      path = opts[:path]
			eval "#{managerClass}.new.compare(path, elements_to_compare)"
		rescue Exception
			$logger.error "There was an error exporting results into file #{opts[:path]} ..."
			raise
		end

		$logger.info "Successfully generated experiments/executions comparation."
	end


end


end
end