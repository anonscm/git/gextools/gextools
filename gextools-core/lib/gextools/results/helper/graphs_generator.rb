#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'gchart'

module GexTools
module Results
module Helper
  
require 'gextools/results/result'
require 'gextools/results/result_status'
require 'gextools/executions/execution'
require 'gextools/executions/execution_type'


class GraphsGenerator

  @@colors = ['1346b3', '902b1c', '319d61', 'ffcc00', '76A4FB']

  #####################
  protected

  def self.findMedian(data=[])
    # Sorting results
    data.sort! {|result1, result2| result1.completionTime <=> result2.completionTime}

    # Verifies if the number of elements on the sorted array is pair or odd
    data.size % 2 == 0 ? pair = true : pair = false

    # Find the median
    median = 0.0
    if pair then
      median_index1 = data.size / 2
      median_index2 = median_index1 + 1

      median = (data[median_index1].completionTime + data[median_index2].completionTime) / 2
    else
      median_index = (data.size / 2).floor

      median = data[median_index].completionTime
    end

    return median
  end

  def self.findAverage(data=[])
    total_time = 0.0
    data.each { |selected_result|
      total_time += selected_result.completionTime
    }
    average = total_time / data.size

    return average
  end
  
  #####################
	public

  #------------------------------------------#
	# Builds an average time comparison graph, #
  # comparing a set of execution of an       #
  # experiment by its execution type         #
  #------------------------------------------#
  def self.averageTimeGraphByExecutionType(path, experiment, execution_type_results={})

    title = "Average time"
    
    data = []
    legend = []

    # Get results obtained from different types of executions
    execution_type_results.each { |execution_type, results|

      # Building graph parameters
      execution_type_name = execution_type.name if execution_type_results.size > 1

      success_results = results[:success_results]

      if (success_results.size != 0) then
        average = findAverage(success_results)
        data << [average]
        legend << execution_type_name
      end
    }

    # Open the file, and plot graphs
    ::Gchart.bar(:title => title,
      :title_alignment => :center,
      :stacked => false,
      :size => '450x300',
      :legend => legend,
      :data => data,
      :bar_colors => @@colors,
      :axis_with_labels => 'y',
      :axis_labels => ['Time (ms)'],
      :format => 'file',
      :filename => File.join(path, "averageTime.png")
    )
    
  end


  #------------------------------------------#
	# Builds an median time comparison graph,  #
  # comparing a set of execution of an       #
  # experiment by its execution type         #
  #------------------------------------------#
  def self.medianTimeGraphByExecutionType(path, experiment, execution_type_results={})

    title = "Median time"

    data = []
    legend = []

    # Get results obtained from different types of executions
    execution_type_results.each { |execution_type, results|

      # Building graph parameters
      execution_type_name = execution_type.name if execution_type_results.size > 1

      success_results = results[:success_results]

      if (success_results.size != 0) then
        median = findMedian(success_results)
        data << [median]
        legend << execution_type_name
      end
    }

    # Open the file, and plot graphs
    ::Gchart.bar(:title => title,
      :title_alignment => :center,
      :stacked => false,
      :size => '450x300',
      :legend => legend,
      :data => data,
      :bar_colors => @@colors,
      :axis_with_labels => 'y',
      :axis_labels => ['Time (ms)'],
      :format => 'file',
      :filename => File.join(path, "medianTime.png")
    )
  end


  #------------------------------------------#
	# Builds a graph showing the proportion    #
  # between answers and its execution status #
  #------------------------------------------#
  def self.executionsStatusGraph(path, experiment, execution_type_results={})

    # Get results obtained from different types of executions
    execution_type_results.each { |execution_type, results|
      next if results[:success_results].size == 0

      title = "Executions status - #{execution_type.name}"

      success_results = results[:success_results]
      timeout_results = results[:timeout_results]
      fail_results = results[:fail_results]
      total = success_results.size + fail_results.size + timeout_results.size

      success_label = "Success (#{success_results.size == 0 ? 0 : ((success_results.size.to_f / total) * 100).round(2)})"
      timeout_label = "Timeout (#{timeout_results.size == 0 ? 0 : ((timeout_results.size.to_f / total) * 100).round(2)})"
      fail_label = "Fail (#{fail_results.size == 0 ? 0 : ((fail_results.size.to_f / total) * 100).round(2)})"

      Gchart.pie(:title => title,
        :title_alignment => :center,
        :theme => :thirty7signals,
        :size => '450x300',
        :data => [success_results.size, fail_results.size, timeout_results.size],
        :labels => [success_label, fail_label, timeout_label],
        :format => 'file',
        :filename => File.join(path, "executionStatuses_#{execution_type.name}.png")
      )
    }
  end

  #------------------------------------------#
	# Basic compare - Compare average time     #
  #------------------------------------------#
  def self.compareAverageTime(path, elements_to_compare=[])

    data = []
    legend = []
    title = "Average Time"
    status = GexTools::Results::Results.findSucessResultStatus()

    elements_to_compare.each { |element|
      selected_experiment = element[:experiment]
      selected_execution = element[:executionLabel]
      selected_results = element[:results]

      success_results = selected_results.select { |result| result.result_status == status}
      success_results = success_results.select { |result| result.execution.label == selected_execution} unless selected_execution.nil?

      average = 0
      average = findAverage(success_results) if (success_results.size != 0)
      data << [average]

      if (selected_execution.nil?) then
        legend << selected_experiment.name
      else
        legend << selected_execution
      end
    }

    # Open the file, and plot graphs
    ::Gchart.bar(:title => title,
      :title_alignment => :center,
      :stacked => false,
      :size => '450x300',
      :legend => legend,
      :data => data,
      :bar_colors => @@colors,
      :axis_with_labels => 'x,y',
      :axis_labels => ['Time (ms)'],
      :bar_width_and_spacing => {:width => 40, :spacing => 10 },
      :format => 'file',
      :filename => File.join(path, "comparisionAverage.png")
    )

  end


  #------------------------------------------#
	# Basic compare - Compare median time     #
  #------------------------------------------#
  def self.compareMedianTime(path, elements_to_compare=[])

    data = []
    legend = []
    title = "Median Time"
    status = GexTools::Results::Results.findSucessResultStatus()

    elements_to_compare.each { |element|
      selected_experiment = element[:experiment]
      selected_execution = element[:executionLabel]
      selected_results = element[:results]

      success_results = selected_results.select { |result| result.result_status == status}
      success_results = success_results.select { |result| result.execution.label == selected_execution} unless selected_execution.nil?

      average = 0
      average = findMedian(success_results) if (success_results.size != 0)
      data << [average]

      if (selected_execution.nil?) then
        legend << selected_experiment.name
      else
        legend << selected_execution
      end
    }

    # Open the file, and plot graphs
    ::Gchart.bar(:title => title,
      :title_alignment => :center,
      :stacked => false,
      :size => '450x300',
      :legend => legend,
      :data => data,
      :bar_colors => @@colors,
      :axis_with_labels => 'x,y',
      :axis_labels => ['Time (ms)'],
      :bar_width_and_spacing => {:width => 40, :spacing => 10 },
      :format => 'file',
      :filename => File.join(path, "comparisionMedian.png")
    )
  end

end

end
end
end