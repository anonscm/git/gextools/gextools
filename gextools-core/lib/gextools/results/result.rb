#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'active_record'


module GexTools
module Results

require 'gextools/instances/instance'
require 'gextools/executions/execution'
require 'gextools/experiments/experiment'
require 'gextools/problems/problem'


# Result object, as it will be recorded in the database
class Result < ActiveRecord::Base
  
  belongs_to :problem, :class_name => "GexTools::Problems::Problem", :include => [:concrete_problem]
  belongs_to :execution, :class_name => "GexTools::Executions::Execution"
  belongs_to :result_status

	#------------------------------------------#
	# Get infos about this result              #
  #------------------------------------------#
	def infos()
    res = "==================================================="
    res << "\nexecution label: #{execution.label}"
    res << "\nproblem: #{problem.concrete_problem.getConcernedProblem()}"
    res << "\nstatus: #{result_status.status}"
    res << "\ncompletionTime: #{completionTime}"
		res << "\nextractedResults: \n#{extractedResults}"
    res << "\n"
		res
	end

  def self.success
    where("result_statuses.status = ? ", "success")
  end

  def self.timed_out
    where("result_statuses.status = ? ", "timeout")
  end

  def self.failed
    where("result_statuses.status = ? ", "fail")
  end
	
end



end
end