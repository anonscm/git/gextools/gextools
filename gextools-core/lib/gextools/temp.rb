#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'fileutils'

module GexTools

# Deals with temporary folders
class Temp

	attr_accessor :keepDirs, :baseDir

	# Says if we want to keep the temp folder anyway, instead of releasing them
	@@keepDirs = false

	# List of used temp folders
	@@tempFoldersList = []

	# Base Dir of Temps dirs
	@@baseDir = "/tmp/gextools"

  #------------------------------------------#
	# Ensure the base dir exists               #
  #------------------------------------------#
	def self.ensureBaseDirExists
		FileUtils.mkdir_p @@baseDir
	end

  #------------------------------------------#
	# Create a temp folder and return its      #
  # fullpath                                 #
  #------------------------------------------#
	def self.getTempFolder
		ensureBaseDirExists
		fullpath = nil

		index = 0
		catch(:getIt) do
			while true do
				fullpath = File.join(@@baseDir, index.to_s)
				throw :getIt unless File.exists? fullpath and File.directory? fullpath
				index += 1
			end
		end
		FileUtils.mkdir fullpath
		@@tempFoldersList << fullpath
		@@tempFoldersList.uniq
		
		return fullpath
	end


  #------------------------------------------#
	# Release (destroy) a temp folder          #
  #------------------------------------------#
	def self.release(fullpath)
		raise ArgumentError.new(:fullpath) unless @@tempFoldersList.include? fullpath
		unless @@keepDirs
			FileUtils.rm_rf fullpath
		end
		@@tempFoldersList.delete fullpath
	end


end


end