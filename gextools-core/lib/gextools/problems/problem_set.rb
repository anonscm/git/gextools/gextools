#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'active_record'

module GexTools
module Problems

require 'gextools/problems/problem'

# Represent a problem set object, as it will be recorded in the database
class ProblemSet < ActiveRecord::Base

  has_many :problems, :dependent => :destroy, :include => [:concrete_problem]
  belongs_to :instance, :class_name => 'GexTools::Instances::Instance'

  #------------------------------------------#
	# Get infos about this problem set         #
  #------------------------------------------#
	def infos(reduced = false)
		res = "name: #{name}"
    res << "\nrelated Instance: #{instance.name}"
    res << "\nproblems count: #{problems.size}"

    unless (reduced) then
      res << "\nproblems:"
      problems.each { |problem|
        res << "\n\t==============="
        res << problem.infos()
      }
    end
    
    res
	end

  #------------------------------------------#
	# Copy the problem set in some place       #
  #------------------------------------------#
	def copyIn(newpath)
		raise ArgumentError.new(:path) if newpath.nil?

		dirpath = File.dirname(newpath)

		# Says a warning if the directory already exists
		$logger.warn "The output data directory #{dirpath} already exists ! Still continuing anyway..." if File.exists? dirpath

    begin
      # Create the directory if it doesn't already exists
      FileUtils.mkdir_p dirpath

      managerClass = ComponentsManager.managerClass($config[:component], :problems)
      eval "#{managerClass}.new.export(newpath, problems)"
    rescue
    end
	end

end

end
end