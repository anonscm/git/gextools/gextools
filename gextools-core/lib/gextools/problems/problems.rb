#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

module GexTools
module Problems

require 'gextools/operation'
require 'gextools/problems/problem'
require 'gextools/problems/problem_set'
require 'gextools/instances/instances'
require 'gextools/components_manager'

# Problems based operation
class Problems < Operation

	#####################
	protected


	#####################
	public


  #------------------------------------------#
	# Creates a problem set object due an      #
  # automatic generation or a simple         #
  # registration                             #
  #------------------------------------------#
	def self.create(opts={})
		raise ArgumentError.new(:component) if opts[:component].nil?

    # Verify if problem set exists in the database before execute anything
    problem_set = findProblemSet(:name => opts[:name])
    if !problem_set.empty? then
      $logger.error "Problem set already registered on the database."
      raise
    end

		# Get the class of the problems manager of the component
		begin
			managerClass = ComponentsManager.managerClass(opts[:component], :problems)
		rescue
			$logger.fatal "Impossible to find problems manager class of of component type '#{opts[:type]}'"
			raise
		end

		# Process the problem
    problem_set = nil
		begin
			case opts[:generate]
				when true	# Generate new problem
					eval "problem_set = #{managerClass}.new.generate(opts)"
				when false	# Register already created problem
					raise ArgumentError.new(:path) if opts[:path].nil?
					eval "problem_set = #{managerClass}.new.register(opts)"
				else
					raise ArgumentError.new(:generate)
			end
    rescue SystemExit
      # Fails silently
      return
		rescue
			$logger.fatal "Impossible to process problem : #{$!.inspect}"
			raise
		end

		# Save the problem in the database
		$logger.info "Saving problem set '#{problem_set.name}' ..."
		res = problem_set.save
		if res
			$logger.info "Has successfully saved problem set '#{problem_set.name}' ..."
		else
			$logger.error "Impossible to register problem in the database... Maybe it is already registered"
		end
	end

  #------------------------------------------#
	# Main problem set fetch method            #
  #------------------------------------------#
  def self.findProblemSet(opts={})

		param = opts.clone
		param.delete :component unless param[:component].nil?

		problem_sets = ProblemSet.where(param).all

		return problem_sets
	end

  #------------------------------------------#
	# Problem set fetch method providing a name#
  #------------------------------------------#
	def self.findWantedProblemSet(name)

		begin
			problem_sets = findProblemSet(:name => name)
		rescue
			$logger.error = "Impossible to find wanted problem set: #{$!.inspect}"
			raise Exception.new
		end

		if problem_sets.length < 1
			$logger.error "Haven't found any problems set of name '#{name}'."
			raise Exception.new
		elsif problem_sets.length == 1
			return problem_sets[0]
		else
			$logger.error "There are more than 1 problems sets of name '#{name}'! Database corrupted!"
			raise Exception.new
		end
	end

  #------------------------------------------#
	# Problem set fetch method providing an    #
  # index                                    #
  #------------------------------------------#
	def self.findProblemSetsOfIndex(id)

		begin
			problem_sets = findProblemSets(:id => id)
		rescue
			err = "Impossible to find wanted problem set"
			$logger.error err 
			raise Exception.new(err)
		end

		if problem_sets.length < 1
			$logger.error "Haven't found any problem set of id '#{id}'."
			raise Exception.new
		elsif problem_sets.length == 1
			return problem_sets[0]
		else
			$logger.error "There are more than 1 problem sets of id '#{id}'! Database corrupted!"
			raise Exception.new
		end
	end

  #------------------------------------------#
	# Obtains infos about problem sets on the  #
  # database                                 #
  #------------------------------------------#
	def self.infos(opts={})
    param = opts.clone
    param.delete :name if param[:name].nil?
    problem_sets = findProblemSet(param)

    if problem_sets.empty? then
      $logger.info "Haven't found any problem set with the specified options"
      raise
    end

    puts "\n======================="
    problem_sets.each{ |elem|
      begin
        if param[:name].nil?
          puts elem.infos(true)
        else
          puts elem.infos
        end
        puts "======================="
      rescue
      end
    }
	end

  #------------------------------------------#
	# Export problem set                       #
  #------------------------------------------#
	def self.export(opts={})
		param = opts.clone
		param.delete :path

		problem_set = findWantedProblemSet(param[:name])

    $logger.info "Exporting problem set #{problem_set.name} into file '#{opts[:path]}' ..."
		problem_set.copyIn(opts[:path])
    
		$logger.info "Successfully copyed #{problem_set.name} into file '#{opts[:path]}' ."
	end

  #------------------------------------------#
	# Delete problem set from the database     #
  #------------------------------------------#
	def self.delete(opts={})
    problem_set = findWantedProblemSet(opts[:name])  

    $logger.info "Do you really want to delete this problem set? (y/n) "
    opt = gets
    if (opt.downcase.chomp == "y") then
      res = problem_set.destroy
      if res.nil? then
        $logger.info "Problem set '#{opts[:name]}' was not found on the database"
      else
        $logger.info "Has successfully deleted problem set '#{opts[:name]}' from the database"
      end
    end
	end


end



end
end