#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'active_record'

module GexTools
module Problems

require 'gextools/problems/problem'

# Problem object, as it will be recorded in the database
class ConcreteProblem < ActiveRecord::Base

  self.abstract_class = true
  has_one :problem, :as => :concrete_problem

  #------------------------------------------#
	# Return the parameters to be used when    #
  # posing the problem on a program          #
  #------------------------------------------#
  def getProblemString(assignedPeers)
    raise NotImplementedError.new
  end

  #------------------------------------------#
	# Return the problem type short name       #
  #------------------------------------------#
  def getProblemTypeName()
    raise NotImplementedError.new
  end

  #------------------------------------------#
	# Return the problem concerned peers       #
  #------------------------------------------#
  def getConcernedPeers()
    raise NotImplementedError.new
  end

  #------------------------------------------#
	# Return the problem concerned             #
  # problem string                           #
  #------------------------------------------#
  def getConcernedProblem()
    raise NotImplementedError.new
  end

end

end
end