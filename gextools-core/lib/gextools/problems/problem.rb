#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'active_record'

module GexTools
module Problems


require 'gextools/instances/instance'
require 'gextools/experiments/experiment'
require 'gextools/results/result'
require 'gextools/problems/concrete_problem'


# Problem object, as it will be recorded in the database
class Problem < ActiveRecord::Base

  has_one :result, :class_name => 'GexTools::Result::Result'
  belongs_to :problem_set
  belongs_to :concrete_problem, :polymorphic => true

  before_destroy :hook_after_destroy

  #------------------------------------------#
	# Destroy associated concrete problem      #
  # information                              #
  #------------------------------------------#
  def hook_after_destroy
    concrete_problem.delete unless concrete_problem.nil?
  end

  #------------------------------------------#
	# Get infos about this problem             #
  #------------------------------------------#
	def infos()
    res = "\n\tproblem type: #{concrete_problem.getProblemTypeName()}"
    res << "\n\tconcerned peers: #{concrete_problem.getConcernedPeers()}"
    res << "\n\tconcerned problem: #{concrete_problem.getConcernedProblem()}"
	end

end


end

end