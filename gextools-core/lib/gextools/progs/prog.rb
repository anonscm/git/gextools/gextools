#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'active_record'
require 'fileutils'
require 'open3'


module GexTools
module Progs

require 'gextools/progs/prog_type'
require 'gextools/experiments/experiment'


# Program object, as it will be recorded in the database
class Prog < ActiveRecord::Base
	validates_uniqueness_of :path
	validates_uniqueness_of :name, :version, :scope => [:id]

  belongs_to :prog_type, :class_name => "::GexTools::Progs::ProgType"
  has_and_belongs_to_many :experiments, :class_name => "GexTools::Experiments::Experiment"

	#------------------------------------------#
	# Get infos about this program             #
  #------------------------------------------#
	def infos()
		res = "name: #{name}"
    res << "\n\ttype: #{prog_type.name}"
    res << "\n\tpath: #{path}"
		res << "\n\tversion: #{version}"
		res
	end

	#------------------------------------------#
	# Copy the program into some path          #
  #------------------------------------------#
	def copyIn(newpath)
		FileUtils.cp(path, newpath)
	end

  #------------------------------------------#
	# Execute the program with parameters      #
  #------------------------------------------#
	def execute(opts={})
    $logger.info "Executing program #{name} - #{version}"

    cmdLine = "#{executionPrefix} "
		cmdLine += "#{path} "
    cmdLine += "#{executionSuffix}"
    cmdLine += "#{opts[:parameters]}"

		resOutput = ""
    resError = ""
    
		Open3.popen3(cmdLine) { |stdin,stdout,stderr|
			resOutput = stdout.read
      resError = stderr.read
		}

    if (resOutput.empty? or !resError.empty?)
      err = "#{name} - #{version} fault : Impossible to run program."
      $logger.error "Used Command line: #{cmdLine}"
			$logger.error "Program output: \n #{resError}"
			raise Exception.new(err)
    end

		if $verbose
			$logger.info "--------------- Output -----------------"
			$logger.info resOutput
			$logger.info "------------- Output End ---------------"
		end
		return resOutput
	end

end



end
end