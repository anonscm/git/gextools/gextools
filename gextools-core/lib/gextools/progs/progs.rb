#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

module GexTools
module Progs

require 'gextools/operation'
require 'gextools/progs/prog'
require 'gextools/progs/prog_type'

# Progs based operation
class Progs < Operation

	#####################
	protected

	#####################
	public

  #------------------------------------------#
	# Creates an program object                #
  #------------------------------------------#
	def self.create(opts={})
		# Verify if prog exists
		$logger.warn "WARNING: program does not exists" unless 
			File.exists? opts[:path]

		# Process the prog
		prog = nil
		opts2 = opts.clone
		opts2.delete :component
    begin
			prog = Prog.new(opts2)
		rescue
			$logger.fatal "Impossible to process prog with options : '#{opts.inspect}': #{$!.inspect}"
			raise
		end

		# Save the prog in the database
		res = prog.save
		if res
			$logger.info "Successfully registered program in the database"
		else
			$logger.error "Impossible to register program in the database... Maybe it is already registered" 
		end
	end

  #------------------------------------------#
	# Register a new program type              #
  #------------------------------------------#
  def self.createType(opts={})
		# Process the prog
		progType = ProgType.new(:name => opts[:programTypeName])

		# Save the prog in the database
		res = progType.save
		if res
			$logger.info "Successfully registered program type in the database"
		else
			$logger.error "Impossible to register program type in the database... Maybe it is already registered"
		end
	end

  #------------------------------------------#
	# Main program fetch method                #
  #------------------------------------------#
	def self.findProg(opts={})
    
    param = opts.clone
    param.delete :component unless param[:component].nil?

		progs = Prog.find(:all, :conditions => param)
		return progs
	end

  #------------------------------------------#
	# Main program type fetch method           #
  #------------------------------------------#
  def self.findProgType(opts={})
		progTypes = ProgType.find(:all, :conditions => opts)
		return progTypes
	end

  #------------------------------------------#
	# Program type fetch method providing a    #
  # name                                     #
  #------------------------------------------#
  def self.findWantedProgType(name)
		begin
			progTypes = findProgType(:name => name)
		rescue
			$logger.error "Impossible to find wanted program type: #{$!.inspect}"
			raise Exception.new
		end

		if progTypes.length < 1
			$logger.error "Haven't found any program types with name '#{name}'. You need to register a program type before register a program!"
			raise Exception.new
		elsif progTypes.length == 1
			return progTypes[0]
		else
			$logger.error "There are more than 1 program type with name '#{name}'! Database corrupted!"
			raise Exception.new
		end
	end

	#------------------------------------------#
	# Program fetch method providing a name    #
  #------------------------------------------#
	def self.findWantedProg(name, version)
		begin
			progs = findProg(:name => name, :version => version)
		rescue
			err = "Impossible to find wanted program"
			$logger.error err 
			raise Exception.new(err)
		end

		if progs.length < 1
			$logger.error "Haven't found any program with name '#{name}' and version '#{version}."
			raise Exception.new
		elsif progs.length == 1
			return progs[0]
		else
			$logger.error "There are more than 1 programs with name '#{name}' and version '#{version}! Database corrupted!"
			raise Exception.new
		end
	end

  #------------------------------------------#
	# Program fetch method providing an index  #
  #------------------------------------------#
	def self.findProgamOfIndex(id)
		begin
			solvers = findProg(:id => id)
		rescue
			err = "Impossible to find wanted program"
			$logger.error err 
			raise Exception.new(err)
		end

		if solvers.length < 1
			$logger.error "Haven't found any program with id '#{id}'."
			raise Exception.new
		elsif solvers.length == 1
			return solvers[0]
		else
			$logger.error "There are more than 1 programs with id '#{id}'! Database corrupted!"
			raise Exception.new
		end
	end

  #------------------------------------------#
	# Obtains infos about programs on the      #
  # database                                 #
  #------------------------------------------#
	def self.infos(opts={})
    param = opts.clone
    param.delete :version unless param[:version].nil?
    param.delete :name if param[:name].nil?
    progs = findProg(param)

    if progs.empty? then
      $logger.info "Haven't found any programs set with the specified options"
      raise
    end

		progs.each{ |elem|
			$logger.info elem.infos
		}
	end

  #------------------------------------------#
	# Delete programs from the database        #
  #------------------------------------------#
	def self.delete(opts={})
		program = findWantedProgram(opts[:name], opts[:version])

    $logger.info "Do you really want to delete this program (y/n) "
    opt = gets
    if (opt.downcase.chomp == "y") then
      res = program.destroy
      if res.nil? then
        $logger.info "Program '#{opts[:name]}' v. '#{opts[:version]}' was not found on the database"
      else
        $logger.info "Has successfully deleted program '#{opts[:name]}' v. '#{opts[:version]}' from the database"
      end
    end
	end


end



end
end