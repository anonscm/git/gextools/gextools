#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#
require 'active_record'

module GexTools
module Executions

# Represents an execution object, as it will be recorded in the database
class Execution < ActiveRecord::Base

  validates_uniqueness_of :label, :scope => [:experiment_id]
  belongs_to :experiment, :class_name => 'GexTools::Experiments::Experiment'
  belongs_to :execution_type
  has_many :results, :class_name => 'GexTools::Results::Result', :dependent => :destroy, :include => [:result_status]

  #------------------------------------------#
	# Get infos about this execution           #
  #------------------------------------------#
	def infos()
		res = "label: #{label}"
		res << "\n\tmode: #{execution_type.name}"
    res << "\n\tparameters: #{parameters}" unless parameters.nil?
    res << "\n\tsuccesfull results number: #{results.success.count} "
    res << "| timeout results number: #{results.failed.size} "
    res << "| failed results number: #{results.timed_out.size}"

		res
	end

end


end
end