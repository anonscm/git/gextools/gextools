#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

module GexTools
module Executions

require 'gextools/operation'
require 'gextools/executions/execution'
require 'gextools/executions/execution_type'
require 'gextools/experiments/experiment'
require 'gextools/experiments/experiments'

# Experiments based operation
class Executions < Operation

	#####################
	protected


	#####################
	public

  #------------------------------------------#
	# Creates an execution object              #
  #------------------------------------------#
	def self.create(opts={})    
		# Process the experiment
		execution = nil
		begin
			execution = Execution.new
      execution.experiment = opts[:experiment]
      execution.execution_type = opts[:execution_type]

      if (opts[:label].nil?) then
        time = Time.new
        execution.label = "#{time.year}#{time.month}#{time.day}_#{time.hour}-#{time.min}-#{time.sec}"
      else
        execution.label = opts[:label]
      end

      unless opts[:parameters].empty then
        execution.parameters = ""
        opts[:parameters].each { |key, value|
          execution.parameters << "#{key}:#{value} " if key != :help
        }
        execution.parameters.rstrip!
      end
		rescue
			$logger.fatal "Impossible to process execution : #{$!.inspect}"
			raise
		end

		# Save the experiment in the database
		$logger.info "Saving execution '#{execution.label}' ..."
		res = execution.save
		if res
			$logger.info "Has successfully saved execution '#{execution.label}' ..."
      return execution
		else
			$logger.error "Impossible to register execution in the database... Maybe it is already registered" unless res
      raise Exception.new
		end
	end

  #------------------------------------------#
	# Creates a new execution type             #
  #------------------------------------------#
  def self.createExecutionType(opts={})
    raise ArgumentError.new(:name) if opts[:name].nil?

    executionType = nil
    begin
      executionType = ExecutionType.new(:name => opts[:name])
    rescue
      $logger.fatal "Impossible to process execution type. Reason: #{$!.inspect}"
      raise
    end

    # Save the instance in the database
    $logger.info "Saving execution type '#{executionType.format}' ..."
    res = executionType.save
    if res
      $logger.info "Has successfully saved execution type'#{executionType.format}' ..."
    else
      $logger.error "Impossible to register execution type in the database... Maybe it is already registered"
    end
  end

  #------------------------------------------#
	# Main execution fetch method              #
  #------------------------------------------#
	def self.findExecution(opts={})
		exections = Execution.find(:all, :conditions => opts)
		return exections
	end

  #------------------------------------------#
	# Main execution type fetch method         #
  #------------------------------------------#
  def self.findExecutionType(opts={})
		exections = ExecutionType.find(:all, :conditions => opts)
		return exections
	end

  #------------------------------------------#
	# Execution fetch method providing an index#
  #------------------------------------------#
	def self.findExecutionOfIndex(id, experiment_id)
		begin
			executions = findExecution(:id => id, :experiment_id => experiment_id)
		rescue
			err = "Impossible to find wanted execution"
			$logger.error err
			raise Exception.new(err)
		end

		if executions.length < 1
			$logger.error "Haven't found any execution of id '#{id}'."
			raise Exception.new
		end

    return executions[0]
	end

  #------------------------------------------#
	# Execution fetch method providing an      #
  # experiment name                          #
  #------------------------------------------#
  def self.findExecutionByExperimentName(experiment_name, experiment_type)
		begin
      experiment = GexTools::Experiments::Experiments.findWantedExperiment(experiment_name, experiment_type)
			executions = findExecution(:experiment_id => experiment.id)
		rescue Exception
			$logger.error "Impossible to find wanted execution #{$!.inspect}"
			raise Exception.new
		end

		if executions.length < 1
			$logger.error "Haven't found any execution for experiment '#{experiment_name}'."
			raise Exception.new
		end

    return executions[0]
	end

  #------------------------------------------#
	# Execution fetch method providing an      #
  # experiment name and execution label      #
  #------------------------------------------#
  def self.findExecutionByExperimentNameAndLabel(experiment_name, experiment_type, executionLabel)
		begin
      experiment = GexTools::Experiments::Experiments.findWantedExperiment(experiment_name, experiment_type)
			executions = findExecution(:experiment_id => experiment.id, :label => executionLabel)
		rescue Exception
			$logger.error "Impossible to find wanted execution #{$!.inspect}"
			raise Exception.new
		end

		if executions.length < 1
			$logger.error "Haven't found any execution with label '#{executionLabel}'."
			raise Exception.new
		end

    return executions[0]
	end

  #------------------------------------------#
	# Execution type fetch method providing a  #
  # name                                     #
  #------------------------------------------#
  def self.findWantedExecutionType(name)
		begin
			executionTypes = findExecutionType(:name => name)
		rescue Exception
			$logger.error "Impossible to find wanted execution type #{$!.inspect}"
			raise Exception.new
		end

		if executionTypes.length < 1
			$logger.error "Haven't found any execution type of name '#{name}'."
			raise Exception.new
		end

    return executionTypes[0]
	end

  #------------------------------------------#
	# Delete executions from the database      #
  #------------------------------------------#
	def self.delete(opts={})
		execution = findExecutionByExperimentNameAndLabel(opts[:name], opts[:component], opts[:executionLabel])

    $logger.info "Do you really want to delete this execution? (y/n) "
    opt = gets
    if (opt.downcase.chomp == "y") then
      res = execution.destroy

      if res.nil? then
        $logger.info "Execution '#{execution.label}' of experiment '#{opts[:name]}' was not found on the database"
      else
        $logger.info "Has successfully deleted execution '#{execution.label}' of experiment '#{opts[:name]}' from the database"
      end
    end
	end

end



end
end