#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'active_record'

module GexTools
module DB


# Deals with connection with the database
class DBConnection

	attr_accessor :param

	# Connection parameters
	@param = {
		:adapter => "mysql2",
		:host => "localhost",
		:username => "me",
		:password => "secret",
		:database => "gextools"
	}

	def initialize(param)
		@param = param unless param == nil
	end

	def connect
		ActiveRecord::Base.establish_connection(@param)
	end

	def logger
		ActiveRecord::Base.logger
	end

	def logger=(foo)
		ActiveRecord::Base.logger = foo
	end




end



end
end