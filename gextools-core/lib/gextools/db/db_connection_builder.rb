#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require_relative 'db_connection'

module GexTools
module DB


# Builds a new DBConnection object
class DBConnectionBuilder

	attr_reader :param # Connection parameters with valid parameters
	attr_reader :validParam
	attr_reader :builtObject # Created object


	# List of valid parameters
	@@validParam = [ :adapter, :host, :username, :password, :database ]
	@@validParam.freeze

	# List of required parameters
	@@requiredParam = [ :adapter, :host, :username, :password, :database ]
	@@requiredParam.freeze

	def []=(key, value)
		raise ArgumentError(key) unless @@validParam.include? key
		@param[key] = value
	end

	def validConfig?
		@@requiredParam.each{ |foo|
			return false if @param[foo] == nil
		}
		return true
	end


	def init
		raise StandardError.new("Invalid Parameters") unless validConfig?
		@builtObject = DBConnection.new @param
		return @builtObject
	end


	def initialize
		@param = {
			:adapter => nil,
			:host => nil,
			:username => nil,
			:password => nil,
			:database => nil }

		@builtObject = nil
	end

	# Create a BD Connection
	def self.create
		bdBuilder = GexTools::DB::DBConnectionBuilder.new
		bdBuilder[:adapter] = $config[:adapter]
		bdBuilder[:host] = $config[:host]
		bdBuilder[:username] = $config[:username]
		bdBuilder[:password] = $config[:password]
		bdBuilder[:database] = $config[:database]
		db = bdBuilder.init
		db.logger = $logger if $verbose
		db.connect
		return db
	end

end



end
end