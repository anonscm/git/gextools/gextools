#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'logger'
require 'rubygems'
require 'active_record'



# Migrate everything
class CreateGextoolsTables < ActiveRecord::Migration

	def self.up
    begin

      unless table_exists? :progs then
        create_table :progs do |t|
          t.column :name, :string, :limit => 45, :null => false
          t.column :version, :string, :limit => 45, :null => false
          t.column :path, :text, :null => false
          t.column :executionPrefix, :text
          t.column :executionSuffix, :text
          t.column :prog_type_id, :int, :null => false
        end

        add_index :progs, [:name, :version], :unique => true
        add_index :progs, :name
        add_index :progs, :version
        add_index :progs, :prog_type_id
      end

      unless table_exists? :prog_types then
        create_table :prog_types do |t|
          t.column :name, :string, :limit => 45, :null => false
        end
      end

      unless table_exists? :experiments_progs then
        create_table :experiments_progs, :id =>false do |t|
          t.column :experiment_id, :int, :null => false
          t.column :prog_id, :int, :null => false
        end
        
        add_index :experiments_progs, [:experiment_id, :prog_id], :primary => true
        add_index :experiments_progs, :prog_id
        add_index :experiments_progs, :experiment_id
      end

      unless table_exists? :experiments then
        create_table :experiments do |t|
          t.column :name, :string, :limit => 45, :null => false
          t.column :problem_set_id, :int, :null => false
          t.column :deployment_id, :int, :null => false
          t.column :type, :string, :null => false
        end

        add_index :experiments, :name
        add_index :experiments, :problem_set_id
        add_index :experiments, :deployment_id
      end

      unless table_exists? :deployments then
        create_table :deployments do |t|
          t.column :name, :string, :limit => 45, :null => false
          t.column :physicalConfiguration, :text, :null => false
          t.column :deployment_type_id, :int, :null => false
          t.column :type, :string, :null => false
        end

        add_index :deployments, :name
        add_index :deployments, :deployment_type_id
      end

      unless table_exists? :deployment_types then
        create_table :deployment_types do |t|
          t.column :name, :string, :limit => 45, :null => false
          t.column :description, :text
        end

        add_index :deployment_types, :name
      end

      unless table_exists? :executions then
        create_table :executions do |t|
          t.column :experiment_id, :int, :null => false
          t.column :parameters, :string
          t.column :label, :string, :null => false
          t.column :execution_type_id, :int, :null => false
          t.column :log, :longblob
        end

        add_index :executions, :label
        add_index :executions, :experiment_id
        add_index :executions, :execution_type_id
      end

      unless table_exists? :execution_types then
        create_table :execution_types do |t|
          t.column :name, :string, :limit => 45, :null => false
          t.column :description, :text
        end

        add_index :execution_types, :name
      end

      unless table_exists? :results then
        create_table :results do |t|
          t.column :completionTime, :float, :null => false
          t.column :rawOutput, :longtext
          t.column :extractedResults, :longtext
          t.column :execution_id, :int, :null => false
          t.column :problem_id, :int, :null => false
          t.column :result_status_id, :int, :null => false
        end

        add_index :results, :execution_id
        add_index :results, :problem_id
        add_index :results, :result_status_id
      end

      unless table_exists? :result_statuses then
        create_table :result_statuses do |t|
          t.column :status, :string, :limit => 45, :null => false
        end

        add_index :result_statuses, :status
      end

      unless table_exists? :problems then
        create_table :problems do |t|
          t.column :problem_set_id, :int, :null => false
          t.column :concrete_problem_id, :int, :null => false
          t.column :concrete_problem_type, :string, :null => false
        end

        add_index :problems, :concrete_problem_id
        add_index :problems, :problem_set_id
      end

      unless table_exists? :problem_sets then
        create_table :problem_sets do |t|
          t.column :name, :string, :limit => 45, :null => false
          t.column :instance_id, :int, :null => false
        end

        add_index :problem_sets, :instance_id
        add_index :problem_sets, :name
      end

      unless table_exists? :instances then
        create_table :instances do |t|
          t.column :name, :string, :limit => 45, :null => false
          t.column :concrete_instance_id, :int, :null => false
          t.column :concrete_instance_type, :string, :null => false
          t.column :description, :text, :null => false
          t.column :isGenerated, :bool
          t.column :instance_chunk_format_id, :int, :null => false
        end

        add_index :instances, :instance_chunk_format_id
        add_index :instances, :concrete_instance_id
        add_index :instances, :concrete_instance_type
        add_index :instances, :name
      end

      unless table_exists? :instance_chunks then
        create_table :instance_chunks do |t|
          t.column :name, :string, :limit => 45, :null => false
          t.column :chunkFile, :longblob
          t.column :instance_id, :int, :null => false
        end

        add_index :instance_chunks, :name
        add_index :instance_chunks, :instance_id
      end

      unless table_exists? :instance_chunk_formats then
        create_table :instance_chunk_formats do |t|
          t.column :format, :string, :limit => 45, :null => false
          t.column :extension, :string, :limit => 10, :null => false
        end

        add_index :instance_chunk_formats, :format
      end

      unless table_exists? :instances_generated then
        create_table :instances_generated, :id =>false do |t|
          t.column :instance_id, :int, :null => false
          t.column :cmdLine, :text
        end

        add_index :instances_generated, :instance_id
      end

    rescue
      puts "\nException while migrating the database =>" + $!
      puts "Rolling back transaction...\n"
      self.down
      raise "\nDone rollback."
    end

	end

	def self.down
		drop_table :progs
    drop_table :prog_types
		drop_table :experiments_progs
		drop_table :experiments
		drop_table :deployments
    drop_table :deployment_types
		drop_table :executions
		drop_table :execution_types
    drop_table :results
    drop_table :result_statuses
    drop_table :problems
    drop_table :problem_sets
    drop_table :instances
    drop_table :instance_chunks
    drop_table :instance_chunk_formats
    drop_table :instances_generated
	end

end