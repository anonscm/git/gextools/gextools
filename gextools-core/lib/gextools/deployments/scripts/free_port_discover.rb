#!/usr/bin/env ruby
#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#
# == Synopsis
#
# Get a random free port
#
# == Usage
#
# free_port_discover.rb

require 'socket'

begin
  server = TCPServer.new(0)
  port = server.addr[1]
rescue
  retry
end

port.gsub!(/Warning: Permanently added .+ to the list of known hosts./, "") if (port.is_a? String)

puts "Port Number : #{port}"