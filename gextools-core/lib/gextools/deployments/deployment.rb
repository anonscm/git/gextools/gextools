#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#
require 'active_record'


module GexTools
module Deployments

require 'gextools/experiments/experiment'
require 'gextools/deployments/deployment_type'


# Deployment object, as it will be recorded in the database
class Deployment < ActiveRecord::Base
	validates_uniqueness_of :name

  has_many :experiments, :class_name => "GexTools::Experiments::Experiment"
  belongs_to :deployment_type


	#------------------------------------------#
	# Get infos about this deployment          #
  #------------------------------------------#
	def infos()
    res = "===================================================="
    res << "\nname: #{name}"
    res << "\nphysical configuration: #{physicalConfiguration}"
    res << "\ngrid name: #{deployment_type.name}"
    res << "\n"
    res
	end

  #------------------------------------------#
	# Get nodes reserved by this deployment    #
  #------------------------------------------#
  def getFreePortOnNode(machine)
    raise NotImplementedError.new
  end

  #------------------------------------------#
	# Get gateway access host name             #
  #------------------------------------------#
  def getAccessHostName()
    raise NotImplementedError.new
  end

  #------------------------------------------#
	# Get nodes reserved by this deployment    #
  #------------------------------------------#
  def getReservedNodesNames
    raise NotImplementedError.new
  end

  #------------------------------------------#
  # Reserve grid nodes                       #
  #------------------------------------------#
  def runReservation(vlan, whenTime, wallTime)
    raise NotImplementedError.new
  end

  #------------------------------------------#
	# Copy from the grid (path on the grid     #
  # start with the home of the connected     #
  # user)                                    #
  #------------------------------------------#
	def copyFromTheGrid(baseDir, tempDir)
    raise NotImplementedError.new
  end

  #------------------------------------------#
	# Copy files to the grid                   #
  #------------------------------------------#
	def copyToTheGrid(tempDir)
    raise NotImplementedError.new
	end

  #------------------------------------------#
	# Copy on the grid                         #
  #------------------------------------------#
	def copyOnTheGrid(tempDir, to_specific_resources = [])
		raise NotImplementedError.new
	end

  #------------------------------------------#
	# Copy from remote clusters, on the grid   #
  #------------------------------------------#
	def copyFromRemoteOnTheGrid(tempDir, from_specific_resources = [])
		raise NotImplementedError.new
	end

  #------------------------------------------#
	# Delete on the grid                       #
  #------------------------------------------#
	def delOnTheGrid(tempDir)
		raise NotImplementedError.new
	end
end



end
end