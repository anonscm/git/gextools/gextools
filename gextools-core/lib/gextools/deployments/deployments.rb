#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#
module GexTools
module Deployments

require 'gextools/operation'
require 'gextools/deployments/deployment'
require 'gextools/deployments/deployment_type'
require 'gextools/components_manager'

# Deployments based operation
class Deployments < Operation

	#####################
	protected


	#####################
	public


  #------------------------------------------#
	# Creates an deployment object             #
  #------------------------------------------#
	def self.create(opts={})
		raise ArgumentError(:type) if opts[:type].nil?

    deployment_type = opts[:type]
		# Get the class of the deployments manager of the component
		begin
			managerClass = ComponentsManager.managerClass(deployment_type.name, :deployments)
		rescue
			$logger.fatal "Impossible to find deployments manager class of component '#{deployment_type.name}'"
			raise
		end

		# Process the deployment
		deployment = nil
		begin
			eval "deployment = #{managerClass}.new.generate(opts)"
    rescue SystemExit
      # Fails silently
      return
		rescue
			$logger.fatal "Impossible to process deployment : #{$!.inspect}"
			raise
		end

		# Save the deployment in the database
		res = deployment.save
		if res
      $logger.info "Has successfully saved deployment '#{deployment.name}' ..."
    else
      $logger.error "Impossible to register deployment in the database... Maybe it is already registered"
    end
	end

  #------------------------------------------#
	# Register a new deployment type           #
  #------------------------------------------#
  def self.createDeploymentType(opts={})
    raise ArgumentError.new(:name) if opts[:name].nil?

    deploymentType = nil
    begin
      deploymentType = DeploymentType.new(:name => opts[:name])
    rescue
      $logger.fatal "Impossible to process deployment type. Reason: #{$!.inspect}"
      raise
    end

    # Save the instance in the database
    $logger.info "Saving deployment type '#{deploymentType.name}' ..."
    res = deploymentType.save
    if res
      $logger.info "Has successfully saved deployment type'#{deploymentType.name}' ..."
    else
      $logger.error "Impossible to register deployment type in the database... Maybe it is already registered"
    end
  end

  #------------------------------------------#
	# Main deployment fetch method             #
  #------------------------------------------#
	def self.findDeployment(opts={})

		param = opts.clone
		param.delete :type

    deployments = Deployment.find(:all, :include => [:deployment_type], :conditions => param)

		return deployments
	end

  #------------------------------------------#
	# Deployment type fetch method providing   #
  # a name                                   #
  #------------------------------------------#
  def self.findDeploymentType(name)

		param = {}
		param[:name] = name

		deployments = DeploymentType.find(:all, :conditions => param)
		return deployments[0]
	end

	#------------------------------------------#
	# Instance deployment method providing     #
  # a name and the deployment type           #
  #------------------------------------------#
	def self.findWantedDeployment(name, type)

		begin
			deployments = findDeployment(:name => name, :type => type)
		rescue
			err = "Impossible to find wanted deployment: #{$!.inspect}"
			$logger.error err
			raise Exception.new(err)
		end

		if deployments.length < 1
			$logger.error "Haven't found any deployments of name '#{name}'."
			raise Exception.new
		elsif deployments.length == 1
			return deployments[0]
		else
			$logger.error "There are more than 1 deployments of name '#{name} ! Database corrupted!"
			raise Exception.new
		end
	end

  #------------------------------------------#
	# Instance deployment method providing     #
  # an index and the deployment type         #
  #------------------------------------------#
	def self.findDeploymentOfIndex(index, type)

		begin
			deployments = findDeployment(:id => index, :type => type)
		rescue
			err = "Impossible to find wanted deployment #{$!.inspect}"
			$logger.error err 
			raise Exception.new(err)
		end

		if deployments.length < 1
			$logger.error "Haven't found any deployments of id '#{index}'."
			raise Exception.new
		elsif deployments.length == 1
			return deployments[0]
		else
			$logger.error "There are more than 1 deployments of id '#{index} ! Database corrupted!"
			raise Exception.new
		end
	end

  #------------------------------------------#
	# Obtains infos about deployments on the   #
  # database                                 #
  #------------------------------------------#
	def self.infos(opts={})
    param = opts.clone
    param.delete :name if param[:name].nil?
    deployments = findDeployment(param)

    if deployments.empty? then
      $logger.info "Haven't found any deployment with the specified options"
      raise
    end

		deployments.each{ |elem|
			puts elem.infos
		}
	end

  #------------------------------------------#
	# Delete deployments from the database     #
  #------------------------------------------#
	def self.delete(opts={})
    deployment = findWantedDeployment(opts[:name], opts[:type])

    $logger.info "Do you really want to delete this deployment configuration? (y/n) "
    opt = gets
    if (opt.downcase.chomp == "y") then
      res = deployment.destroy

      if res.nil? then
        $logger.info "Deployment '#{opts[:name]}' was not found on the database"
      else
        $logger.info "Has successfully deleted deployment '#{opts[:name]}' from the database"
      end
    end
	end

end



end
end