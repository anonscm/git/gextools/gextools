#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

module GexTools

  # Manage the components. The following notation is used:
  #   - type => name of main component folder ex: grid, progs, knowledge...
  #   - component => name of the component ex: somewhere, somewhere2...
  #   - part => subtype of the component ex: experiments, results...
  class ComponentsManager

    #------------------------------------------#
    # Includes required components             #
    #------------------------------------------#
    def self.includeComponents
      abort "You must specify a valid program component ! E.g: somewhere" if $config[:component].nil?
      abort "You must specify a valid grid component ! E.g: somewhere"  if $config[:grid].nil?

      begin
        $logger.debug "Attempting to include the required program component : #{$config[:component]}"
        if $config[:component_dev_path].nil? then
          require "gextools/#{$config[:component].to_s}"
        else
          $LOAD_PATH.unshift($config[:component_dev_path]) unless $LOAD_PATH.include?($config[:component_dev_path])
          require "#{$config[:component_dev_path]}/gextools/#{$config[:component].to_s}"
        end

        $logger.debug "Attempting to include the required grid component : #{$config[:grid]}"
        if $config[:grid_dev_path].nil? then
          require "gextools/#{$config[:grid].to_s}"
        else
          $LOAD_PATH.unshift($config[:grid_dev_path]) unless $LOAD_PATH.include?($config[:grid_dev_path])
          require "#{$config[:grid_dev_path]}/gextools/#{$config[:grid].to_s}"
        end
      rescue Exception
        $logger.error "Impossible to retrieve component '#{$config[:component]}'"
        raise
      end
    end

    #------------------------------------------#
    # Return the manager class of a part of a  #
    # component                                #
    #------------------------------------------#
    def self.managerClass(component, part)
      raise ArgumentError, :component if component.nil?
      raise ArgumentError, :part if part.nil?

      res = nil
      begin
        eval "res = GexTools::Components::#{component.to_s.capitalize}::#{part.to_s.capitalize}::#{part.to_s.capitalize}"
      rescue
        $logger.error "Impossible to retrieve class manager for part '#{part}' of component '#{component}': #{$!.inspect}"
        $logger.error $!.backtrace.join("\n") if $verbose
        raise
      end
      return res
    end
    
  end


end