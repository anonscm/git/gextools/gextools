#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

module GexTools


# Abstract Base class for an operation
class Operation

	def self.create(opts={})
		raise NotImplementedError.new
	end


	def self.infos(opts={})
		raise NotImplementedError.new
	end

	def self.delete(opts={})
		raise NotImplementedError.new
	end

end


end