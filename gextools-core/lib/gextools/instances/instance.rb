#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'active_record'
require 'fileutils'

module GexTools
module Instances

require 'gextools/problems/problem'
require 'gextools/instances/instance_generated'
require 'gextools/instances/instance_chunk'
require 'gextools/instances/concrete_instance'


# Instance object and its main operations
class Instance < ActiveRecord::Base
	validates_uniqueness_of :name

  has_many :instanceChunks, :dependent => :destroy
  has_many :problems, :class_name => "GexTools::Problems::Problem"
  belongs_to :instance_chunk_format
  belongs_to :concrete_instance, :polymorphic => true, :inverse_of => :instance

  before_validation :hook_before_validation_create, :on => :create
  before_create :hook_before_create
  before_destroy :hook_after_destroy

  attr_accessor :oldPath
  attr_accessor :peers

  def initialize()
    super

    # Initialize variables
    @peers = {}
  end

  #------------------------------------------#
	# Get infos about this instance            #
  #------------------------------------------#
	def infos()
    res = "===================================================="
    res << "\nname: #{name}"
    res << "\ndescription: #{description}" unless description.nil?
    res << concrete_instance.infos()
    res << "\n"
    res
	end

  #------------------------------------------#
	# Saves instance on new data path during   #
  # its creation                             #
  #------------------------------------------#
  def hook_before_validation_create
    if (@peers.empty?) then
      new_path = File.join($config[:dataDirectory], "instances", name)

      $logger.info "Fetching information from the selected instance ..."

      @oldPath = new_path if @oldPath.nil?

      getInstanceDataFromPath(@oldPath)
      concrete_instance.fetchInstanceStats()
    end
  end

  #------------------------------------------#
	# Saves instance on new data path during   #
  # its creation                             #
  #------------------------------------------#
  def hook_before_create
    new_path = File.join($config[:dataDirectory], "instances", name)

    # Copying into new path...
    @oldPath = File.join($config[:dataDirectory], "instances", name) if (@oldPath.nil?)
    $logger.info "Saving instance infos into #{new_path} ..."
    copyIn(@oldPath, new_path)

    # Saving into database...
    $logger.info "Saving instance infos on database ..."
    uploadIn(new_path)
  end

  #------------------------------------------#
	# Destroys instance dependents             #
  #------------------------------------------#
  def hook_after_destroy
    # Destroy associated concrete instance information
    concrete_instance.destroy unless concrete_instance.nil?

    # Destroy associated generated instance information
    GexTools::Instances::InstanceGenerated.delete_all(:instance_id => id)
  end

  #------------------------------------------#
	# Delegates the parsing of an instance in  #
  # order to build its logic objects just    #
  # after it is fetched on the database      #
  #------------------------------------------#
  def after_find_instance
    if @peers.nil? or @peers.length == 0 then
      @peers = {}

      # Read from instance files, locally
      if File.exist?(File.join($config[:dataDirectory], "instances", name)) then
        path = File.join($config[:dataDirectory], "instances", name)
        getInstanceDataFromPath(path)
      else
        # Read from instance files chunks on the database
        $logger.debug "Instance was not found locally. Searching into database... "

        begin
          chunks = GexTools::Instances::Instances.findChunks(id)
          getInstanceDataFromDatabase(chunks)
        rescue
          $logger.error "Instance was not found. You must register an instance in order to peform this action! #{$!}"
          raise
        end
      end
    end
  end

  #------------------------------------------#
	# Parse local instance files and build the #
  # instance logic peer objects following    #
  # its format                               #
  #------------------------------------------#
  def getInstanceDataFromPath(path)
    raise ArgumentError.new(:path) if
    path.nil? or path == "" or !File.exists?(path) or !File.directory?(path)

    paths = Dir["#{path}/**/*.#{self.instance_chunk_format.extension}"].reject {|fn| File.directory?(fn) }
    paths.each { |filePath|

      # Removing file extension
      peerName = File.basename(filePath, ".#{self.instance_chunk_format.extension}")
      peer = nil

      # Get a peer object from the current file content
      content = nil
      File.open(filePath, "r") { |file|
        content = file.read
      }

      # Get a peer object from the current file, depending on its type
      peer = concrete_instance.parse(content)

      next if peer.nil?

      # Save peer in temporary array
      @peers[peerName] = peer
    }
  end

  #------------------------------------------#
	# Parse database instance chunks and build #
  # the instance logic peer objects          #
  # following its format                     #
  #------------------------------------------#
  def getInstanceDataFromDatabase(chunks)
    chunks.each { |chunk|
      peer = nil
      peerName = chunk.name
      content = chunk.chunkFile

      # Get a peer object from the current file, depending on its type
      peer = concrete_instance.parse(content)

      next if peer.nil?

      # Save peer in temporary array
      @peers[peerName] = peer
    }
  end

  #------------------------------------------#
	# Copy the instance into some path         #
  #------------------------------------------#
  def copyIn(oldpath, newpath)
    raise ArgumentError.new(:oldpath) if oldpath.nil?
    raise ArgumentError.new(:newpath) if newpath.nil?

    # Equals paths, copy is unnecessary
    return if (oldpath == newpath)

    # Says a warning if the directory already exists
    $logger.warn "The output data directory #{newpath} already exists ! Still continuing anyway..." if File.exists? newpath

    # throw error if the old directory do not exists
    raise "The input data directory #{oldpath} do not exist ! Aborting the operation..." unless File.exists? oldpath

    begin
      # Create the directory if it doesn't already exists
      FileUtils.mkdir_p newpath unless File.exists? newpath

      # Copy all file structure to the new path
      FileUtils.cp_r("#{oldpath}/.", newpath)

    rescue
      $logger.error "Imposible to copy instance into file : #{$!.inspect} "
      raise
    end
  end

  #------------------------------------------#
	# Export the instance into some path       #
  #------------------------------------------#
  def exportIn(newpath, format)
    raise ArgumentError.new(:newpath) if newpath.nil?

    # Says a warning if the directory already exists
    $logger.warn "The output data directory #{newpath} already exists ! Still continuing anyway..." if File.exists? newpath

    begin
      # Create the directory if it doesn't already exists
      FileUtils.mkdir_p newpath unless File.exists? newpath
      managerClass = ComponentsManager.managerClass($config[:component], :instances)

      contents = nil
      eval "contents = #{managerClass}.new('').export(format.format, @peers)"

      contents.each { |name, content|
        path = File.join(newpath, "#{name}.#{format.extension}")
        File.open(path, "w") { |file|
          # Write content
          file.write content
        }
      }
    rescue
      $logger.error "Imposible to copy instance into file : #{$!} "
      raise
    end
  end

  #------------------------------------------#
	# Upload instance to the database          #
  #------------------------------------------#
  def uploadIn(newpath)
    raise ArgumentError.new(:newpath) if newpath.nil?

    # throw error if the directory do not exists
    raise "The input data directory #{newpath} do not exist ! Aborting the operation..." unless File.exists? newpath

    begin
      @peers.each { |name, peer|
        fullpath = Dir["#{newpath}/**/#{name}.#{self.instance_chunk_format.extension}"].first
        File.open(fullpath, "rb") { |file|
          chunk = InstanceChunk.new
          chunk.name = name
          chunk.chunkFile = file.read
          self.instanceChunks << chunk
        }
      }

      unless self.instanceChunks.size > 0
        $logger.error "No chunks were generated!"
        raise
      end
    rescue
      $logger.error "Imposible to copy instance into database : #{$!.inspect} "
      raise
    end
  end

end



end
end