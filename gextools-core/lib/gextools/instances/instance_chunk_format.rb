#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'active_record'


module GexTools
module Instances

# Represents the format of an instance
class InstanceChunkFormat < ActiveRecord::Base
  validates_uniqueness_of :format

  has_many :instances

  #------------------------------------------#
	# Parses instance files following this     #
  # format                                   #
  #------------------------------------------#
  def self.parse(path)
    raise NotImplementedError.new
  end

  #------------------------------------------#
	# Export an instance to this format        #
  #------------------------------------------#
  def self.export(name, peer)
    raise NotImplementedError.new
  end

  #------------------------------------------#
	# Prepare a sat analysis of an instance    #
  # that follows this format                 #
  #------------------------------------------#
  def self.prepareSatAnalysis(instance)
    raise NotImplementedError.new
  end
  
end

end
end