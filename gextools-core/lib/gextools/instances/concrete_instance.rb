#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'active_record'

module GexTools
module Instances

require 'gextools/instances/instance'

# Instance object and its main operations
class ConcreteInstance < ActiveRecord::Base

  self.abstract_class = true
  has_one :instance, :as => :concrete_instance, :inverse_of => :concrete_instance, :class_name => "GexTools::Instances::Instance"

  #------------------------------------------#
  # Gets concrete instance infos             #
  #------------------------------------------#
  def infos()
    raise NotImplementedError.new
  end

  #------------------------------------------#
  # Fetches instance statistics              #
  #------------------------------------------#
  def fetchInstanceStats()
    raise NotImplementedError.new
  end

end

end
end