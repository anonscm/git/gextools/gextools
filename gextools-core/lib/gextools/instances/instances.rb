#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'awesome_print'


module GexTools
module Instances

require 'gextools/operation'
require 'gextools/instances/instance'
require 'gextools/instances/instance_chunk'
require 'gextools/instances/instance_generated'
require 'gextools/instances/instance_chunk_format'
require 'gextools/components_manager'

# Instances based operation
class Instances < Operation

  #####################
  protected


  #####################
  public


  #------------------------------------------#
	# Creates an instance object due an        #
  # automatic generation or a simple         #
  # registration                             #
  #------------------------------------------#
  def self.create(opts={})
    raise ArgumentError.new(:component) if opts[:component].nil?

    # Verify if instance exists in the database before execute anything
    instances = findInstance(:name => opts[:name])
    unless instances.empty? then
      $logger.info "Instance already registered on the database."
      raise
    end

    begin
      managerClass = ComponentsManager.managerClass(opts[:component], :instances)
    rescue
      $logger.fatal "Impossible to find instances manager class of component type '#{opts[:component]}'"
      raise
    end

    # Process the instance
    instance = nil
    format = findWantedFormat($config[:format])
    opts[:format] = format
    begin
      case opts[:generate]
      when true	# Generate new instance
        begin
          eval "instance = #{managerClass}.new('#{opts[:generator].name}').generate(opts)"
        rescue SystemExit
          # Fails silently
          return
        end

        $logger.info "The generated instance has the following attributes:\n"
        ap instance
        ap instance.concrete_instance
        puts "\n"
        $logger.info "Do you want to keep this instance? (y/n) "
        
        opt = gets
        if (opt.downcase.chomp == "n") then
          $logger.info "Deleting generated folder..."

          # Deleting files
          path = File.join($config[:dataDirectory], "instances", opts[:name])
          FileUtils.rm_rf(path)

          # Delete from database
          instance.destroy

          $logger.info "The instance #{opts[:name]} was not generated."
          return
        end

      when false	# Register already created instance
        raise ArgumentError.new(:path) if opts[:path].nil?
        eval "instance = #{managerClass}.new('').register(opts)"
        instance.oldPath = opts[:path]
      else
        raise ArgumentError.new(:generate)
      end
    rescue
      $logger.debug "Impossible to process instance : #{$!.inspect}"
      raise $!
    end

    # Save the instance in the database
    $logger.info "Saving instance '#{instance.name}' ..."
    res = instance.save
    if res
      $logger.info "Has successfully saved instance '#{instance.name}' ..."
    else
      $logger.error "Impossible to register instance in the database... Maybe it is already registered"
    end
  end

  #------------------------------------------#
	# Register a new instance format           #
  #------------------------------------------#
  def self.createFormat(opts={})
    raise ArgumentError.new(:format) if opts[:format].nil?

    # Process the instanceFormat
    instanceFormat = nil
    begin
      instanceFormat = InstanceChunkFormat.new(:format => opts[:format])
    rescue
      $logger.fatal "Impossible to process instance. Reason: #{$!.inspect}"
      raise
    end

    # Save the instance in the database
    $logger.info "Saving instance format '#{instanceFormat.format}' ..."
    res = instanceFormat.save
    if res
      $logger.info "Has successfully saved instance format'#{instanceFormat.format}' ..."
    else
      $logger.error "Impossible to register instance format in the database... Maybe it is already registered"
    end
  end

  #------------------------------------------#
	# Main instance fetch method               #
  #------------------------------------------#
  def self.findInstance(opts={})

    param = opts.clone
    param.delete :component unless param[:component].nil?
    param[:concrete_instance_type] = $concreteInstanceTypes unless $concreteInstanceTypes.nil?

    instances = Instance.where(param).includes(:instance_chunk_format, :concrete_instance).all

    instances.each{ |instance|
      instance.after_find_instance
    }

    return instances
  end

  #------------------------------------------#
	# Format fetch method providing a name     #
  #------------------------------------------#
  def self.findWantedFormat(formatName)

    format = nil
    formatName = formatName.upcase
    begin
      format = InstanceChunkFormat.find_by_format(formatName)
    rescue
      err = "Impossible to find wanted format #{$!}"
      $logger.error err
      raise Exception.new(err)
    end

    if format.nil?
      $logger.error "Haven't found any format with name '#{formatName}'."
      raise Exception.new
    end

    return format
  end

  #------------------------------------------#
	# Instance fetch method providing a name   #
  #------------------------------------------#
  def self.findWantedInstance(name)
    
    begin
      instances = findInstance(:name => name)
    rescue
      $logger.error "Impossible to find wanted instance #{$!.inspect}"
      raise Exception.new
    end

    if instances.length < 1
      $logger.error "Haven't found any instance of name '#{name}'."
      raise Exception.new
    elsif instances.length == 1
      return instances[0]
    else
      $logger.error "There are more than 1 instances of name '#{name}'! Database corrupted!"
      raise Exception.new
    end
  end

  #------------------------------------------#
	# Instance fetch method providing an index #
  #------------------------------------------#
  def self.findInstanceOfIndex(id)
    
    begin
      instances = findInstance(:id => id)
    rescue
      err = "Impossible to find wanted instance"
      $logger.error err
      raise Exception.new(err)
    end

    if instances.length < 1
      $logger.error "Haven't found any instance of id '#{id}'."
      raise Exception.new
    elsif instances.length == 1
      return instances[0]
    else
      $logger.error "There are more than 1 instances of id '#{id}'! Database corrupted!"
      raise Exception.new
    end
  end

  #------------------------------------------#
	# Generated instance information fetch     #
  # method providing an index                #
  #------------------------------------------#
  def self.findGeneratedInstance(id)
    begin
      chunk = InstanceGenerated.find_by_instance_id(id)
    rescue
      err = "Impossible to find wanted chunks. Reason: #{$!.inspect}"
      $logger.error err
      raise Exception.new(err)
    end

    if chunk.nil?
      $logger.error "Haven't found any chunk of id '#{id}'."
      raise Exception.new
    end

    return chunk
  end

  #------------------------------------------#
	# Instance chunk fetch method providing    #
  # an index                                 #
  #------------------------------------------#
  def self.findChunks(id)
    begin
      chunks = InstanceChunk.find(:all, :conditions => ["instance_id = #{id}"])
    rescue
      err = "Impossible to find wanted chunks. Reason: #{$!.inspect}"
      $logger.error err
      raise Exception.new(err)
    end

    if chunks.length < 1
      $logger.error "Haven't found any chunk of id '#{id}'."
      raise Exception.new
    end

    return chunks
  end

  #------------------------------------------#
	# Obtains infos about instances on the     #
  # database                                 #
  #------------------------------------------#
  def self.infos(opts={})
    param = opts.clone
    param.delete :name if param[:name].nil?
    instances = findInstance(param)

    if instances.empty? then
      $logger.info "Haven't found any instance with the specified options"
      raise
    end
    
    instances.each{ |elem|
      puts elem.infos
    }
  end


  #------------------------------------------#
	# Export instance to a given format        #
  #------------------------------------------#
  def self.export(opts={})
    
    instance = findWantedInstance(opts[:name])
    format = nil
    unless opts[:format].nil? then
      format = findWantedFormat(opts[:format])
    else
      format = instance.instance_chunk_format
    end

    path = File.join(opts[:path], format.format.downcase, instance.name)

    $logger.info "Exporting instance '#{instance.name}' into folder '#{path}' ..."
    instance.exportIn(path, format)

    $logger.info "Successfully exported #{instance.name} into folder '#{path}' ."
  end


  #------------------------------------------#
	# Delete instances from the database       #
  #------------------------------------------#
  def self.delete(opts={})
    instance = findWantedInstance(opts[:name])

    $logger.info "Do you really want to delete this instance? (y/n) "
    opt = gets
    if (opt.downcase.chomp == "y") then
      res = instance.destroy
      if res.nil? then
        $logger.info "Instance '#{opts[:name]}' was not found on the database"
      else
        $logger.info "Has successfully deleted instance '#{opts[:name]}' from the database"
      end
    end
  end
  
end



end
end