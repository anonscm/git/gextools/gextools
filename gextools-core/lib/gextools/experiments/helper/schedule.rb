#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'logger'
require 'rubygems'
require 'active_record'
require 'tzinfo'

module GexTools
module Experiments
module Helper

# Helper class responsible for experiment date localization and validation
class Schedule

  #------------------------------------------#
	# Builds execution time object from string #
  #------------------------------------------#
  def self.build_execution_time(date_str)
    datetime_slices = date_str.split(" ")
    date_slices = datetime_slices[0].split("-")

    year = date_slices[0]
    month = date_slices[1]
    day = date_slices[2]

    time_slices = datetime_slices[1].split(":")
    hour = time_slices[0]
    minute = time_slices[1]
    second = time_slices[2]

    return Time.local(year, month, day, hour, minute, second)
  end

  #------------------------------------------#
	# Validates if the proposed execution time #
  # is consistent. (e.g. Not in the past)    #
  #------------------------------------------#
  def self.validate_execution_time(date_str)
    begin
      execution_time = build_execution_time(date_str)
      raise "Specified time is not consistent" unless execution_time > Time.now
    rescue
      $logger.error $!
      raise
    end
  end

  #------------------------------------------#
	# Get the localized execution time. The    #
  # grid timezone must be parameterized in   #
  # future versions                          #
  #------------------------------------------#
  def self.get_real_execution_time(date_str)
    begin
      execution_time = build_execution_time(date_str)
      utcTime = execution_time.getutc
      real_time = TZInfo::Timezone.get('Europe/Paris').utc_to_local(utcTime)
      return "#{real_time.year}-#{real_time.month}-#{real_time.day} #{real_time.hour}:#{real_time.min}:#{real_time.sec}"
    rescue
      $logger.error $!
      raise
    end
  end

  #------------------------------------------#
	# If start datetime is specified, holds    #
  # the execution.                           #
  #------------------------------------------#
  def self.wait_until(date_str)
    begin
      execution_time = build_execution_time(date_str)
      # More 6 sec in order to avoid problems
      while (Time.now < execution_time + 6) do
        sleep 3
      end
    rescue
      $logger.error $!
      raise
    end
  end

end



end
end
end