#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

module GexTools
module Experiments

require 'gextools/operation'
require 'gextools/experiments/experiment'
require 'gextools/components_manager'

# Experiments based operation
class Experiments < Operation

	#####################
	protected


	#####################
	public


  #------------------------------------------#
	# Creates an experiment object             #
  #------------------------------------------#
	def self.create(opts={})
		raise ArgumentError.new(:component) if opts[:component].nil?

    # Verify if instance exists in the database before execute anything
    experiment = findExperiment(:name => opts[:name], :component => opts[:component])
    if !experiment.empty? then
      $logger.info "Experiment already registered on the database."
      raise
    end

		begin
			managerClass = ComponentsManager.managerClass(opts[:component], :experiments)
		rescue
			$logger.fatal "Impossible to find experiments manager class of component '#{opts[:component]}'"
			raise
		end

		# Process the experiment
		experiment = nil
		begin
			eval "experiment = #{managerClass}.new.generate(opts)"
    rescue SystemExit
      # Fails silently
      return
		rescue
			$logger.fatal "Impossible to process experiment : #{$!.inspect}"
			raise
		end

		# Save the experiment in the database
		$logger.info "Saving experiment '#{experiment.name}' ..."
		res = experiment.save
		if res
			$logger.info "Has successfully saved experiment '#{experiment.name}' ..."
		else
			$logger.error "Impossible to register experiment in the database... "
		end
	end

  #------------------------------------------#
	# Main experiment fetch method             #
  #------------------------------------------#
	def self.findExperiment(opts={})

		param = opts.clone
		param.delete :component
    param[:type] = $concreteExperimentTypes

    label = param[:executionLabel]
    param.delete :executionLabel if param.has_key?(:executionLabel)
    param[:executions] = {:label => label} unless label.nil?
    
    experiments = Experiment.includes(:deployment, :problem_set,
                                         {:executions => :execution_type},
                                         {:progs => :prog_type})
                                         .where(param)
                                         .all
    
		return experiments
	end

  #------------------------------------------#
	# Experiment fetch method providing a name #
  #------------------------------------------#
	def self.findWantedExperiment(name, component)

		begin
			experiments = findExperiment(:name => name, :component => component)
		rescue
			err = "Impossible to find wanted experiment: "
			$logger.error err + $!.to_s
			raise Exception.new(err)
		end

		if experiments.length < 1
			$logger.error "Haven't found any experiment of name '#{name}'."
			raise Exception.new
		elsif experiments.length == 1
			return experiments[0]
		else
			$logger.error "There are more than 1 experiments of name '#{name}'! Database corrupted!"
			raise Exception.new
		end
	end

  #------------------------------------------#
	# Experiment fetch method providing an     #
  # index and a type                         #
  #------------------------------------------#
	def self.findExperimentOfIndex(id, type)

		begin
			experiments = findExperiment(:id => id, :component => type)
		rescue
			err = "Impossible to find wanted experiment"
			$logger.error err 
			raise Exception.new(err)
		end

		if experiments.length < 1
			$logger.error "Haven't found any experiment of id '#{id}'."
			raise Exception.new
		elsif experiments.length == 1
			return experiments[0]
		else
			$logger.error "There are more than 1 experiments of id '#{id}'! Database corrupted!"
			raise Exception.new
		end
	end

  #------------------------------------------#
	# Obtains infos about experiments on the   #
  # database                                 #
  #------------------------------------------#
	def self.infos(opts={})
    param = opts.clone
    param.delete :name if param[:name].nil?
    experiments = findExperiment(param)

    if experiments.empty? then
      $logger.info "Haven't found any experiment with the specified options"
      raise
    end

		experiments.each{ |elem|
      puts elem.infos
    }
	end

  #------------------------------------------#
	# Delete experiments from the database     #
  #------------------------------------------#
	def self.delete(opts={})
    experiment = findWantedExperiment(opts[:name], opts[:component])

    $logger.info "Do you really want to delete this experiment and all its executions? (y/n) "
    opt = gets
    if (opt.downcase.chomp == "y") then
      res = experiment.destroy
      if res.nil? then
        $logger.info "Experiment '#{opts[:name]}' was not found on the database"
      else
        $logger.info "Has successfully deleted experiment '#{opts[:name]}' from the database"
      end
    end
	end


  #------------------------------------------#
	# Get specific component parameters        #
  #------------------------------------------#
	def self.getParameters(component)
		raise ArgumentError.new(:component) if component.nil?

		begin
			managerClass = ComponentsManager.managerClass(component, :experiments)
		rescue
			$logger.fatal "Impossible to find experiments manager class of component '#{component}'"
			raise
		end

		# Process the experiment
		parameters = {}
		begin
			eval "parameters = #{managerClass}.new.getParameters()"
    rescue SystemExit
      # Fails silently
		rescue
			$logger.fatal "Impossible to get parameters of the experiment execution for component : '#{component}'"
			raise
		end

    return parameters
	end

end



end
end