#
# Gextools (https://sourcesup.renater.fr/projects/gextools/ ) - Tools for
# experiments over a grid  - This file is part of GexTools
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#
require 'active_record'
require 'fileutils'

module GexTools
module Experiments

require 'gextools/deployments/deployment'
require 'gextools/instances/instance'
require 'gextools/instances/instance_chunk'
require 'gextools/problems/problem'
require 'gextools/results/result'
require 'gextools/progs/prog'


# Experiment object, as it will be recorded in the database
class Experiment < ActiveRecord::Base
	validates_uniqueness_of :name
  validate :correct_progs?

  has_many :executions, :class_name => "GexTools::Executions::Execution", :dependent => :destroy, :include => [:execution_type]
  has_many :results, :class_name => "GexTools::Results::Result", :through => :executions
  belongs_to :deployment, :class_name => "GexTools::Deployments::Deployment"
  belongs_to :problem_set, :class_name => "GexTools::Problems::ProblemSet"
  has_and_belongs_to_many :progs, :class_name => "GexTools::Progs::Prog", :include => [:prog_type]

  #####################
	public

  #------------------------------------------#
  # Base folders of an experiment            #
  #------------------------------------------#
	@@baseFolders = {
		:instance => "instance",
		:bin => "bin",
		:log => "log",
		:nil => ""
	}

  #------------------------------------------#
	# Validate if the experiment was created   #
  # with the correct programs                #
  #------------------------------------------#
  def correct_progs?
    raise NotImplementedError.new
  end


  #------------------------------------------#
  # Perform transformations on instance files#
  # and send it to the server                #
  #------------------------------------------#
  def transformsAndSendInstanceFiles(instance, assignedPeers, parameters={})
    raise NotImplementedError.new
  end

	#------------------------------------------#
	# Get infos about this instance            #
  #------------------------------------------#
	def infos()
    res = "===================================================="
    res << "\nname: #{name}"
    res << "\ndeployment: #{deployment.name}"
    res << "\nproblem set: #{problem_set.name}"
    res << "\nassociated programs: "
    progs.each{ |prog|
      res << "\n\t -------------"
      res << "\n\t#{prog.infos}"
    }
    res << "\nassociated executions: "
    executions.each{ |execution|
      res << "\n\t -------------"
      res << "\n\t#{execution.infos}"
    }
    res << "\n"
    res
	end

  #------------------------------------------#
	# Initialize the execution of this         #
  # experiment                               #
  #------------------------------------------#
  def initialize_execution(opts={})
		$logger.info "Executing experiment '#{self.name}' ..."
    execution_started = false

		begin
      # Get a temporary folder
			temp_dir = GexTools::Temp.getTempFolder

      # Getting experiment parameters
      parameters = GexTools::Experiments::Experiments.getParameters($config[:component])
      return if parameters.empty?

      # Creates an instance of an execution in order to associate the results
      execution = createExecution(opts[:execution_label], opts[:execution_type], parameters)

      # Validate execution datetime if it is specified
      GexTools::Experiments::Helper::Schedule.validate_execution_time(opts[:when]) unless opts[:when].nil?

      # Localize execution datetime in order to be sent to the grid
      execution_time = GexTools::Experiments::Helper::Schedule.get_real_execution_time(opts[:when]) unless opts[:when].nil?

      # Run deployment
      $logger.info "Running deployments scripts ...."
      if (!opts[:temporaryDeployment].nil?) then
        @deployment_to_use = GexTools::Deployments::Deployments.findWantedDeployment(opts[:temporaryDeployment], $config[:deploymentType])
      else
        @deployment_to_use = deployment
      end

      $logger.info "Trying to make a job reservation ..."
      @reservation_id = @deployment_to_use.runReservation(opts[:vlan], execution_time, opts[:walltime])
      at_exit do
        # Delete grid reservation
        unless (@reservation_id.nil?) then
          $logger.info "Deleting the reservation on the grid ..."
          @deployment_to_use.deleteReservation(@reservation_id)
        end

        puts "Experiment execution finished."
      end

      # Holds the execution until the registration time
      $logger.info "Holding experiment execution until reserved time..."
      GexTools::Experiments::Helper::Schedule.wait_until(opts[:when]) unless opts[:when].nil?

      # Copy scripts, programs and data to the concerned resources
      copyBaseProgs(temp_dir, parameters)

      # Create deployment in case of multi site experiment
      @deployment_to_use.create_deployment(@reservation_id) if opts[:vlan]

      # Assigning obtained nodes with peers
      assigned_peers = assignedPeersToNodes(@reservation_id)

      # Copy scripts, programs and data to the concerned resources
      copyBaseData(temp_dir, assigned_peers, parameters)

      # Execute specific experiment workflow!
      execution_started = true
      self.execute(@reservation_id, assigned_peers, execution, parameters)

		rescue Exception => e

			$logger.error "There was an error executing experiment '#{name}' : #{e}"
      execution.destroy if (!execution.nil? and !execution.id.nil?)
			raise e
      
		ensure

      if (execution_started) then
        # Retrieve log files
        $logger.info "Retrieving log files to #{temp_dir}..."
        distant_log_dir = File.join("gextools", "data", "#{name}", @@baseFolders[:log])
        log_dir = getRelativePathInsideAnExperimentFolder(temp_dir, :log)
        
        specific_resources = []
        specific_resources << deployment.getConcernedResources()

        @deployment_to_use.copyFromRemoteOnTheGrid(distant_log_dir + "/", specific_resources)
        @deployment_to_use.copyFromTheGrid(distant_log_dir, log_dir)

        # Retrieve log files
        $logger.info "Saving log on the database..."
        saveLog(execution, log_dir)
      end

      # Release temp folder
      GexTools::Temp.release temp_dir

		end

    $logger.info "Has successfully run execution #{execution.label} on experiment '#{name}' ..."
	end

  #####################
	protected

  #------------------------------------------#
  # Assign peers to grid nodes and returns   #
  # an hash containing the association       #
  #------------------------------------------#
  def assignedPeersToNodes(reservation_id)
    reserved_nodes_names = @deployment_to_use.getReservedNodesNames(reservation_id)
    reserved_nodes_number = reserved_nodes_names.size
    if reserved_nodes_number <= 0
      $logger.error "Error when resolving reserved nodes names."
      exit 3
    end

    # Select and recreate used instance in memory
    instance = GexTools::Instances::Instances.findWantedInstance(problem_set.instance.name)
    peers = instance.peers

    # Alerts that there are more reserved nodes cores than peers
    $logger.info "There are #{reserved_nodes_number} reserved nodes for #{peers.size} peers."

    # Assign peers to nodes. It does not take account the possibility of having several
    # peers (i.e: profiles) running on a same server.
    n = 0
    assigned_peers = {}
    peers.each { |peerName, peer|

      node_name = reserved_nodes_names[n]
      port_str = @deployment_to_use.getFreePortOnNode(reservation_id, node_name)
      raise "Bad port format => [#{port_str}]" if (port_str =~ /Port Number : (\d+)/).nil?
      port = $1

      assigned_peers[peerName] = {
        :machine => node_name,
        :port => port,
      }

      # If several peers are assigned to a same node, there are assigned with different port numbers.
      if (n == reserved_nodes_number - 1) then
        n = 0
      else
        n += 1
      end
    }

    return assigned_peers
  end

  #------------------------------------------#
  # Creates an execution object for this     #
  # experiment                               #
  #------------------------------------------#
  def createExecution(label, type, parameters)

    execution = GexTools::Executions::Executions.create(
      :experiment => self,
      :label => label,
      :execution_type => type,
      :parameters => parameters)

    return execution
  end

  #------------------------------------------#
  # Saves the log of the experiment on the   #
  # database                                 #
  #------------------------------------------#
  def saveLog(execution, log_dir)
    log_files = File.join(log_dir, "**", "*.log")
    size = 0.0
    Dir.glob(log_files) { |file|
      size += (File.size(file).to_f/ 2**20).round(2)
    }
    
    $logger.info "The log of this execution has #{size} MB"
    $logger.info "Do you want to save this log on the database? (y/n) "

    opt = gets
    if (opt.downcase.chomp == "y") then
      concat_log_path = File.join(log_dir, "#{execution.label}.log")

      # Put the log level to INFO, momentarily
      $logger.level = Logger::INFO

      file_string = ""
      Dir.glob(log_files) { |file|
        file_string << File.read(file) unless File.directory?(file)
      }

      begin
        File.open(concat_log_path,"w") { |concat_log|
          concat_log.puts("<log>")
          concat_log.puts(file_string)
          concat_log.puts("</log>")
        }


        File.open(concat_log_path,"rb") { |concat_log|
          execution.log = concat_log.read
          execution.save
        }
      rescue
      end

      $logger.level = Logger::DEBUG if $verbose
    end
  end

  #------------------------------------------#
  # Get and save the results                 #
  #------------------------------------------#
	def getResults(id, problem, expExec, resultString, params)

		# Delegate la creation de results
		GexTools::Results::Results.retreive(
        :id => id,
        :deployment => @deployment_to_use,
        :problem => problem,
        :execution => expExec,
        :result_string => resultString,
        :params => params
		)
	end

  #------------------------------------------#
	# Get the relative path inside a           #
  # experiment folder                        #
  #------------------------------------------#
	def getRelativePathInsideAnExperimentFolder(base, part, path="")
		relative_path = File.join(base, "gextools", "data", "#{name}", @@baseFolders[part], path)
		if File.directory? relative_path
			return "#{relative_path}/"
    end

    return "#{relative_path}"
	end

  #------------------------------------------#
	# Get the fullpath inside a experiment     #
  # folder                                   #
  #------------------------------------------#
	def getFullPathInsideAnExperimentFolder(base, part, path="")
		fullpath = File.join(base, "gextools", "data", "#{name}", @@baseFolders[part], path)
    fullpath = File.expand_path(fullpath)
		if File.directory? fullpath
			return "#{fullpath}/"
    end

    return "#{fullpath}"
	end

  #------------------------------------------#
  # Copies scripts and progs to the grid     #
  #------------------------------------------#
	def copyBaseProgs(tempDir, parameters={})

		# Puts base folders into the temp dir
		@@baseFolders.each_key{ |key|
			FileUtils.mkdir_p(getRelativePathInsideAnExperimentFolder(tempDir, key))
		}
    
    scripts_folder = File.join(tempDir, "gextools", "scripts")
    FileUtils.mkdir_p(scripts_folder)

    # Delete ancient instances, problems, logs on distant sites
    $logger.info("Deleting ancient instances, programs and logs on distant sites...")
    @deployment_to_use.delOnTheGrid(getRelativePathInsideAnExperimentFolder("~", :nil))

		# Puts progs into the right experiment folder
    self.progs.each { |prog|
      prog.copyIn(getRelativePathInsideAnExperimentFolder(tempDir, :bin, "#{prog.name}"))
    }

		# Copy deployment scripts to the experiment folder
    script_files = File.join(File.dirname(__FILE__), "..", "deployments", "scripts", "*.rb")
    FileUtils.cp_r(Dir.glob(script_files), scripts_folder)

    # Copy script files also from the concrete grid component...
    $LOAD_PATH.each { |dir|
      if (dir.include?("#{$config[:grid].downcase}/")) then
        script_files = File.join("#{dir}/gextools/#{$config[:grid].downcase}/deployments/scripts/*.rb")
        FileUtils.cp_r(Dir.glob(script_files), scripts_folder)
        break
      end
    }

    # Copy experiment folder to the grid
    $logger.info("Copying experiment folder to the grid ...")
		@deployment_to_use.copyToTheGrid(tempDir)

    # Copy new data folder on the grid
    $logger.info("Copying experiment folder and scripts on the grid ...")
    @deployment_to_use.copyOnTheGrid(getRelativePathInsideAnExperimentFolder("~", :nil))
    @deployment_to_use.copyOnTheGrid("~/gextools/scripts/")
	end

  #------------------------------------------#
  # Copies instance to the grid              #
  #------------------------------------------#
  def copyBaseData(tempDir, assignedPeers, parameters={})
    # Puts instances into the right experiment folder
    transformsAndSendInstanceFiles(self.problem_set.instance, assignedPeers, parameters)

    # Copy experiment folder to the grid
    $logger.info("Copying experiment folder to the grid ...")
		@deployment_to_use.copyToTheGrid(tempDir)

    # Copy new data folder on the grid
    $logger.info("Copying experiment folder and scripts on the grid ...")
    @deployment_to_use.copyOnTheGrid(getRelativePathInsideAnExperimentFolder("~", :nil))
    @deployment_to_use.copyOnTheGrid("~/gextools/scripts/")
  end


end



end
end