
require 'rubygems'
require 'yaml'
require 'logger'
require 'active_record'
require 'active_record/fixtures'

LIB_DIR			= 'lib'
LIB_FULL_DIR	= File.expand_path(LIB_DIR)

namespace :gextools do
  namespace :db do

    def create_database config
      options = {:charset => 'utf8', :collation => 'utf8_unicode_ci'}

      create_db = lambda do |config|
        ActiveRecord::Base.establish_connection config.merge('database' => nil)
        ActiveRecord::Base.connection.create_database config['database'], options
        ActiveRecord::Base.establish_connection config
      end

      begin
        create_db.call config
      rescue Mysql2::Error => sqlerr
        if sqlerr.errno == 1405
          print "#{sqlerr.error}. \nPlease provide the root password for your mysql installation\n>"
          root_password = $stdin.gets.strip

          grant_statement = <<-SQL
            GRANT ALL PRIVILEGES ON #{config['database']}.*
              TO '#{config['username']}'@'localhost'
              IDENTIFIED BY '#{config['password']}' WITH GRANT OPTION;
          SQL

          create_db.call config.merge('database' => nil, 'username' => 'root', 'password' => root_password)
        else
          $stderr.puts sqlerr.error
          $stderr.puts "Couldn't create database for #{config.inspect}, charset: utf8, collation: utf8_unicode_ci"
          $stderr.puts "(if you set the charset manually, make sure you have a matching collation)" if config['charset']
        end
      end
    end

    desc 'Create the database from database.yml for the current DATABASE_ENV'
    task :create => :environment do
      create_database $database_config
      puts "Done.\n"
    end

    desc 'Drops the database for the current DATABASE_ENV'
    task :drop => :environment do
      ActiveRecord::Base.connection.drop_database $database_config['database']
      puts "Done.\n"
    end

    desc "Migrate the gexTools main database.
    Target specific version with VERSION=x and current DATABASE_ENV"
    task :migrate => :environment do
      begin
        ActiveRecord::Migration.verbose = true
        ActiveRecord::Migrator.migrate("#{LIB_DIR}/gextools/db/migrate",
                                            ENV["VERSION"] ? ENV["VERSION"].to_i : nil)
      rescue
        abort "\nProblems were detected in the migration script. Operation was not executed. #{$!}"
      end

      puts "Done.\n"
    end

    task :populate => :migrate do
      Dir.glob("#{LIB_DIR}/db/migrate/fixtures/*.yml").each do |file|
        base_name = File.basename(file, '.*')
        puts "Loading #{base_name}..."
        ActiveRecord::Fixtures.create_fixtures("#{LIB_DIR}/gextools/db/migrate/fixtures", base_name)
      end
    end

    task :environment do
      DATABASE_ENV = ENV['DATABASE_ENV'] || 'production'

      $database_config = YAML::load(File.open(File.join(Dir.home, '.gextools/database.yml'))) [DATABASE_ENV]
      $component_config = YAML::load(File.open(File.join(Dir.home, '.gextools/config.yml')))
      ActiveRecord::Base.establish_connection($database_config)
      ActiveRecord::Base.logger = Logger.new(File.open('database.log', 'a'))
    end

    desc "Unmigrate the specifc component database.
    Target specific version with VERSION=x and current DATABASE_ENV"
    task :unmigrateSpecific => :environment do
      begin
        ActiveRecord::Migrator.down("#{LIB_DIR}/gextools/db/migrate",
                                            ENV["VERSION"] ? ENV["VERSION"].to_i : nil)
      rescue
        abort "\nProblems were detected in the unmigration script. Operation was not executed."
      end
    end

    desc "Unmigrate the main database.
    Target specific version with VERSION=x and current DATABASE_ENV"
    task :unmigrate do
      begin
        ActiveRecord::Migrator.down("#{LIB_DIR}/gextools/db/migrate",
                                            ENV["VERSION"] ? ENV["VERSION"].to_i : nil)
      rescue
        abort "\nProblems were detected in the unmigration script. Operation was not executed."
      end

      puts "Done.\n"
    end

    desc 'Rolls the schema back to the previous version (specify steps w/ STEP=n).'
    task :rollback => :environment do
      step = ENV['STEP'] ? ENV['STEP'].to_i : 1
      ActiveRecord::Migrator.rollback "#{LIB_DIR}/gextools/db/migrate, step"
    end

    desc "Retrieves the current schema version number"
    task :version => :environment do
      puts "Current version: #{ActiveRecord::Migrator.current_version}"
    end

  end
end
