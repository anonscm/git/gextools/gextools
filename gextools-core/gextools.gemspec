lib = File.expand_path('../lib/', __FILE__)
$:.unshift lib unless $:.include?(lib)

Gem::Specification.new do |s|
  s.name = 'gextools'
  s.version = '1.0'
  s.has_rdoc = true
  s.date = Time.now.utc.strftime("%Y-%m-%d")
  s.extra_rdoc_files = ['NOTICE', 'LICENSE']
  s.summary = 'Gextools - Tools for experiments over a grid'
  s.description = s.summary
  s.author = 'Andre Fonseca'
  s.email = 'andre.amorimfonseca@gmail.com'
  s.executables = ['gexDeploy', 'gexExperiments', 'gexInstances', 'gexProblems', 'gexProgs', 'gexResults']
  s.files = %w(LICENSE NOTICE Rakefile) + Dir.glob("{bin,lib,spec}/**/*")
  s.require_path = "lib"
  s.bindir = "bin"

  s.add_development_dependency 'debugger'

  s.add_runtime_dependency "rake"
  s.add_runtime_dependency "mysql2"
  s.add_runtime_dependency "trollop"
  s.add_runtime_dependency "activerecord"
  s.add_runtime_dependency "builder"
  s.add_runtime_dependency "awesome_print"
  s.add_runtime_dependency "googlecharts"
  s.add_runtime_dependency "tzinfo"
end
