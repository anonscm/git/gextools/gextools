#
# Gextools Somewhere2 - Somewhere2 component (
# https://sourcesup.renater.fr/projects/gextools/ ) - This file is part of
# Gextools Somewhere2
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#
# To change this template, choose Tools | Templates
# and open the template in the editor.

module GexTools
module Components
module Somewhere2

  # For development only
  $LOAD_PATH.unshift('../../gextools-somewhere_api/lib') unless $LOAD_PATH.include?('../../gextools-somewhere_api/lib')

  require 'gextools/somewhere_api'

  GexTools::Components::Somewhere2.autoload(:Experiments, "gextools/somewhere2/experiments/experiments")
  GexTools::Components::Somewhere2.autoload(:Instances, "gextools/somewhere2/instances/instances")
  GexTools::Components::Somewhere2.autoload(:Problems, "gextools/somewhere2/problems/problems")
  GexTools::Components::Somewhere2.autoload(:Results, "gextools/somewhere2/results/results")

  $concreteInstanceTypes = ["GexTools::Components::SomewhereApi::Instances::SWInstance"]
  $concreteExperimentTypes = ["GexTools::Components::Somewhere2::Experiments::Experiment"]

end
end
end