#
# Gextools Somewhere2 - Somewhere2 component (
# https://sourcesup.renater.fr/projects/gextools/ ) - This file is part of
# Gextools Somewhere2
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'fileutils'

module GexTools
module Components
module Somewhere2
module Instances

require 'gextools/instances/instances'
require 'gextools/instances/instance'
require 'gextools/instances/instance_generated'
require 'gextools/progs/progs'
require 'gextools/temp'

require 'gextools/somewhere_api/instances/SWR_format'
require 'gextools/somewhere_api/instances/SW_instance'
require 'gextools/somewhere_api/instances/SAT_utils'

require 'gextools/somewhere_api/logic/theory'
require 'gextools/somewhere_api/logic/targets'


# Somewhere Instances based operation
class Instances < GexTools::Instances::Instances
  
	#####################
	protected



	#####################
	public

	@@KNOWN_Generators = ["swontogen", "chaininstancegenerator"]
	@@KNOWN_GeneratorsTypes = ["barabassi", "watts"]

	# Parameters
	@param = nil

	def genCmdLine(generatorName)
		global = case generatorName.downcase
      when "swontogen"
        Trollop::options do
          opt :satName, "The name of the sat solver you want to use", :type => String, :short => '-s', :required => false
          opt :satVersion, "The version of the sat solver you want to use", :type => String, :short => '-S', :required => false
          opt :numberOfPeers, "Number of peers of the instance", :type => :int, :short => '-N', :required => false
          opt :generatorType, "Type of the generator to use", :type => String, :short => '-g', :required => false
          opt :numberOfLiteralPerPeer, "Number of Literals Per Peer", :type => :int, :short => '-p', :required => false
          opt :percentOfEquivLinks, "Percent of Equivalence Links", :type => :float, :short => '-P', :required => false
          opt :bmin, "Min Number of Branching", :type => :int, :short => '-m', :required => false
          opt :bmax, "Max Number of Branching", :type => :int, :short => '-M', :required => false
          opt :dmin, "Min Number of Depth", :type => :int, :short => '-d', :required => false
          opt :dmax, "Max Number of Depth", :type => :int, :short => '-D', :required => false
          opt :mappingGeneratorType, "Type of Mapping Generator", :type => String, :short => '-a', :required => false
          opt :fixedNumberOfMappings, "Fixed number of Mappings", :type => :int, :short => '-F', :required => false
          opt :minNumberOfMappings, "Min Number of Mappings", :type => :int, :short => '-o', :required => false
          opt :maxNumberOfMappings, "Max Number of Mappings", :type => :int, :short => '-O', :required => false
          opt :mappingTypeDistribution, "Mapping Type Distribution (->, <-, disjoint, equiv, xor)", :type => :string, :short => '-t', :required => false
          opt :mappingAtribution, "Mapping Attribution Rule [FIRST,SECOND,RANDOM]", :type => :string, :short => '-A', :required => false, :default => "RANDOM"
         stop_on @@KNOWN_GeneratorsTypes

          opt :initialNumberOfPeers, "Initial number of peers in a Barabassi generator configuration", :type => :int, :short => '-i', :required => false
          opt :edgesToAddPerTimestep, "Number of edges to add per timestep, in a Barabassi generator configuration", :type => :int, :short => '-e', :required => false
          opt :randomSeed, "Random Seed Number, in a Barabassi generator configuration", :type => :int, :short => '-r', :required => false
          opt :numberOfNeighbours, "Number of neighbours per peer to create, in a Watts generator configuration", :type => :int, :short => '-u', :required => false
          opt :rewritingFactor, "Rewriting Factor, in a Watts generator configuration", :type => :double, :short => '-R', :required => false
        end
      when "chaininstancegenerator"
        Trollop::options do
          opt :satName, "The name of the sat solver you want to use", :type => String, :short => '-s', :required => false
          opt :satVersion, "The version of the sat solver you want to use", :type => String, :short => '-S', :required => false
          opt :numberOfPeers, "Number of peers of the instance", :type => :int, :short => '-N', :required => false
          opt :consistent, "Define the consistency of the P2PSI", :type => :bool, :short => '-c', :default => false
        end
      when nil, ""
        {}
      else
        Trollop::die "unknown subcommand #{$cmd.inspect}"
    end

		@param = global
	end


  #------------------------------------------#
	# Deals with the instance registration on  #
  # the database.                            #
  #------------------------------------------#
	def register(param={})
		raise ArgumentError.new(:param) if param.nil?

		opts = @param.merge(param)

		$logger.info "Registering instance '#{opts[:name]}' ..."

		instance = GexTools::Instances::Instance.new

		instance.name = opts[:name]
    instance.isGenerated = false
    instance.instance_chunk_format = opts[:format]
    instance.description = opts[:description]
    
    instance.concrete_instance = GexTools::Components::SomewhereApi::Instances::SWInstance.new
    
		# Perform SAT analysis on instance if necessary
		unless opts[:satName].nil?
      begin
        $logger.info "Analysing instance '#{instance.name}' ..."
        instance.getInstanceDataFromPath(opts[:path])
        sat = GexTools::Progs::Progs.findWantedProg(opts[:satName], opts[:satVersion])
        res = GexTools::Components::SomewhereApi::Instances::SATUtils.analyseSAT(instance, sat)

        instance.concrete_instance.isSat = res
        instance.concrete_instance.fetchInstanceStats
        $logger.info "Has successfully analysed instance '#{instance.name}' ..."
      rescue
        $logger.warn "Unable to perform sat analysis for instance #{instance.name} : " + $!
      end
		end

		return instance
	end


  #------------------------------------------#
	# Deals with the instance generation       #
  #------------------------------------------#
	def generate(param={})
		raise ArgumentError.new(:param) if param.nil?

		opts = @param.merge(param)

		$logger.info "Generating instance '#{opts[:name]}' ..."

		begin
			# Generate generated instance
			generator = opts[:generator]

      # Build generator parameters
      path = File.join($config[:dataDirectory], "instances", opts[:name])
      FileUtils.mkdir_p(path)

      if opts[:generator].name.downcase == "swontogen" then
        opts[:parameters] = " --swg #{opts[:generatorType]}"
        case opts[:generatorType].upcase
          when "WATTS"
            opts[:parameters] += ",#{opts[:numberOfNeighbours]}"
            opts[:parameters] += ",#{opts[:rewritingFactor]}"
          when "BARABASSI"
            opts[:parameters] += ",#{opts[:initialNumberOfPeers]}"
            opts[:parameters] += ",#{opts[:edgesToAddPerTimestep]}"
            opts[:parameters] += ",#{opts[:randomSeed]}"
          else
            $logger.error "Unknown or unsupported somewhere generator type"
            raise ArgumentError.new(:generatorType)
        end

        opts[:parameters] += " --np #{opts[:numberOfPeers]}"
        opts[:parameters] += " --nlp #{opts[:numberOfLiteralPerPeer]}"
        opts[:parameters] += " --lb #{opts[:bmin]}"
        opts[:parameters] += " --ub #{opts[:bmax]}"
        opts[:parameters] += " --ld #{opts[:dmin]}"
        opts[:parameters] += " --ud #{opts[:dmax]}"
        opts[:parameters] += " --pe #{opts[:percentOfEquivLinks]}"
        opts[:parameters] += " --mgt #{opts[:mappingGeneratorType]}"
        unless opts[:fixedNumberOfMappings].nil?
          opts[:parameters] += " --fnm #{opts[:fixedNumberOfMappings]}"
        else
          opts[:parameters] += " --vnm #{opts[:minNumberOfMappings]} #{opts[:maxNumberOfMappings]}"
        end
        opts[:parameters] += " --mtd #{opts[:mappingTypeDistribution]}"
        opts[:parameters] += " --ma #{opts[:mappingAttribution].upcase}" unless opts[:mappingAttribution].nil?
        opts[:parameters] += " --sf #{opts[:format].format}"
        opts[:parameters] += " --sd #{path}"
      elsif opts[:generator].name.downcase == "chaininstancegenerator" then
        opts[:parameters] = " -n #{opts[:name]}"
        opts[:parameters] += " generate"
        opts[:parameters] += " -n #{opts[:numberOfPeers]}"
        opts[:parameters] += " -c" unless opts[:consistent] == false
      end

      # Executing generator...
			output = generator.execute(opts)
      paths = Dir["#{path}/**/*.#{opts[:format].extension}"]
      if paths.empty? then
        $logger.error "No chunks were generated! Review the used generator parameters!"
        raise "No chunks were generated!"
      end
			$logger.info "Has successfully generated instance '#{opts[:name]}' ..."

			# Register the generated instance
      opts[:path] = path
      instance = register(opts)
      instance.isGenerated = true
      generated = GexTools::Instances::InstanceGenerated.new
      generated.instance = instance
      generated.cmdLine = opts[:parameters]
      generated.save()
      
		rescue
			$logger.error "Impossible to generate instance!"
			raise $!
		end

		return instance
	end

  #------------------------------------------#
	# Deals with the instance export on        #
  #------------------------------------------#
  def export(format, peers)
    content = nil
    if (format.upcase == "SWR") then
      content = GexTools::Components::SomewhereApi::Instances::SWRFormat.export(peers)
    elsif (format.upcase == "DIMACS") then
      content = GexTools::Components::SomewhereApi::Instances::DIMACSFormat.export(peers)
    end

    return content
  end


	def initialize(generatorName)
		genCmdLine(generatorName)
	end

end



end
end
end
end
