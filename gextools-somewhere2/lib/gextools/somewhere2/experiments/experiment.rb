#
# Gextools Somewhere2 - Somewhere2 component (
# https://sourcesup.renater.fr/projects/gextools/ ) - This file is part of
# Gextools Somewhere2
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'fileutils'
require 'builder'

module GexTools
module Components
module Somewhere2
module Experiments

require 'gextools/temp'
require 'gextools/results/results'
require 'gextools/experiments/experiment'
require 'gextools/executions/executions'
require 'gextools/executions/helper/thread_pool'
require 'gextools/experiments/helper/schedule'

require 'gextools/somewhere2/instances/instances'
require 'gextools/somewhere2/problems/problems'

require 'gextools/somewhere_api/problems/query'
require 'gextools/somewhere_api/problems/clause_add'

# Experiment object for somewhere, as it will be executed
class Experiment < GexTools::Experiments::Experiment

  #####################
	private

  #------------------------------------------#
	# Validate if the experiment was created   #
  # somewhere2 program                       #
  #------------------------------------------#
  def correct_progs?
    somewhere = nil
    progs.each { |prog|
      if (prog.prog_type.name == "somewhere2") then
        somewhere = prog
      end
    }

    unless (progs.size == 1) and (!somewhere.nil?)
      $logger.error("A Somewhere experiment requires one somewhere jar.")
      errors.add(:progs, "A Somewhere experiment requires one somewhere program.")
    end
  end

  #------------------------------------------#
  # Change the name of the nodes in          #
  #	SomeWhere files to the name of the       #
  #	reserved nodes                           #
  #------------------------------------------#
  def transformsAndSendInstanceFiles(instance, assigned_peers, parameters={})
    begin
      $logger.info("There are #{instance.instanceChunks.size} SomeWhere files to update.")

      instance_folder = getRelativePathInsideAnExperimentFolder("~", :instance, instance.name)
      create_folder_cmd = "mkdir -p #{instance_folder}"
      @deployment_to_use.runCmd(create_folder_cmd)

      log_folder = getRelativePathInsideAnExperimentFolder("~", :log)

      # Apply Transformation to all instances files and transfer them to the grid
      instance.instanceChunks.each { |chunk|
        $logger.info("Sending file : #{chunk.name}")
        content = chunk.chunkFile

        # Build the config xml file content
        xml = ''
        doc = ::Builder::XmlMarkup.new(:target=> xml, :indent => 1 )
        doc.instruct! :xml, :encoding => "UTF-8"

        # Configuration is need to change the NIC if using the shoal module
        doc.somewhere do
          doc.base do |base|
              base.logFile(File.join(log_folder, "#{chunk.name}.log"))
          end
        end

        peer_config_path = File.join(instance_folder , chunk.name + ".config")
        createConfigCmd = "echo '#{xml}' > #{peer_config_path}"
        @deployment_to_use.runCmd(createConfigCmd)

        renamed_peer_path = File.join(instance_folder , chunk.name + "." + instance.instance_chunk_format.extension)
        copyCmd = "echo '#{content}' > #{renamed_peer_path}"
        @deployment_to_use.runCmd(copyCmd)
      }

    rescue Exception
      raise "There was an error applying changing somewhere files ... #{$!}"
      puts $!.backtrace.join("\n")
    end
  end

  #------------------------------------------#
  # Run a problem on the grid                #
  #------------------------------------------#
	def runProblem(id, problem, assigned_peers, exp_exec, parameters)
    # Get parameters
    ttl = parameters[:ttl]
    bl = parameters[:bl]

    # Get the infos of the peer from the first literal in order to set the jar execution parameters.
    # We will need to do it again in order to correct the parameters of the problem string.
    machine = nil
    listen_port = nil
    assigned_peers.each { |peerName, params|
      if (problem.concrete_problem.getConcernedPeers.include?(peerName)) then
        machine = params[:machine]
        listen_port = params[:port]
        break
      end
    }

    # Build problem string
    cmd = nil
    if problem.concrete_problem.getProblemTypeName().upcase == "QUERY" then
      cmd = "echo 'query #{problem.concrete_problem.getConcernedProblem()}"
    elsif problem.concrete_problem.getProblemTypeName().upcase == "CLAUSEADD" then
      cmd = "echo 'addClause #{problem.concrete_problem.getConcernedProblem()}"
    end

    cmd += " -bl #{bl}" unless bl.nil?
    cmd += " -ttl #{ttl}" if ttl !=60
    cmd += "' | nc #{machine} #{listen_port}"

    result_string = @deployment_to_use.runCmd(cmd)

    # Fetch the results from the grid
    params = {}
    getResults(id, problem, exp_exec, result_string, params)
	end
  

  #------------------------------------------#
  # Run problems following an execution type #
  #------------------------------------------#
	def runProblemSet(reservationNumber, assigned_peers, exp_exec, parameters)
		begin
      $logger.info("## Doing problems set named " + problem_set.name)

			$logger.info("--------------- Queries Run Output --------------") if $verbose

      if (exp_exec.execution_type.name.upcase == "SEQUENTIAL") then
        order = 0
        problem_set.problems.each { |problem|
          runProblem(order, problem, assigned_peers, exp_exec, parameters)
          order += 1
        }
      elsif (exp_exec.execution_type.name.upcase == "CONCURRENT") then
        threads = GexTools::Executions::Helper::ThreadPool.new(10)
        problem_set.problems.each { |problem|
          threads.execute{
            runProblem(threads.waiting, problem, assigned_peers, exp_exec, parameters)
          }
        }
        threads.join
        threads.close
      end

			$logger.info("----------------- Output End -----------------") if $verbose
		rescue
			$logger.info("----------------- Output End -----------------") if $verbose
			$logger.error "Error during queries sets : #{$!}"
			raise
		end
	end

  #------------------------------------------#
  # Run program on all nodes                 #
  #------------------------------------------#
  def runSolver(prog, reservation_number, assigned_peers)

    directory = getRelativePathInsideAnExperimentFolder("~", :nil)

    # Create log directory
    log_folder = getRelativePathInsideAnExperimentFolder("~", :log)
    create_folder_cmd = "mkdir -p #{log_folder}"
    @deployment_to_use.runCmd(create_folder_cmd)

    # Get information about the target instance
    instance = self.problem_set.instance
    instance_folder = getRelativePathInsideAnExperimentFolder("~", :instance)

    # Get instance extension
    format = instance.instance_chunk_format

    assigned_peers.each{ |peerName, params|
      machine = params[:machine]
      listen_port = params[:port]

      $logger.info "Running SomeWhere on node : #{machine} ..."

      # Get instance path
      instance_path = File.join(instance_folder, instance.name, "#{peerName}.#{format.extension}")

      # Get config path
      config_path = File.join(instance_folder, instance.name, "#{peerName}.config")

      # Indicates execution output file
      execution_output = File.join(log_folder, "#{peerName}.out")

      # Execute someWhere on this node
      opts = {}
      opts[:cmd] = "#{prog.executionPrefix} #{File.join(directory, "bin", "#{prog.name}")} -loadPropositionalInstance #{instance_path} -importConfigurationFile #{config_path} -listenRemoteCommands #{listen_port} -redirectOutput remote &> #{execution_output} & disown"
      opts[:reservationNumber] = reservation_number
      opts[:machine] = machine
      @deployment_to_use.runCmdOnNode(opts)
    }

    # Wait until all peers have fully loaded the instances...
    $logger.info "Waiting until instances are ready to be queried..."
    assigned_peers_copy = assigned_peers.clone

    # Put the log level to ERROR, momentarily
    $logger.level = Logger::ERROR

    errors = 0
    while (errors < 50 and assigned_peers_copy.length != 0) do

      has_connection_refused = false
      assigned_peers_copy.delete_if{ |peerName, params|
        machine = params[:machine]
        listen_port = params[:port]
      
        cmd = "echo checkLoadedInstance | nc #{machine} #{listen_port}"

        result_string = @deployment_to_use.runCmd(cmd)
        pp "checkLoadedInstance : #{machine}:#{listen_port} => #{result_string}" if $verbose

        has_connection_refused = true if (!result_string.nil? and result_string =~ /Connection refused/)
        true if (!result_string.nil? and result_string =~ /true\n/)
      }

      errors += 1 if has_connection_refused
      sleep 3
    end

    raise "There were problems initializating Somewhere instances on the grid" if (errors >= 100)

    p "Done."

    if $verbose then
      $logger.level = Logger::DEBUG
    else
      $logger.level = Logger::INFO
    end
  end

	#####################
	public

  #------------------------------------------#
	# Execute this experiment!!!               #
  #------------------------------------------#
  def execute(reservationID, assigned_peers, execution, parameters)

    # Getting used programs
    somewhere = nil
    progs.each { |prog|
      if (prog.prog_type.name == "somewhere2" and somewhere.nil?) then
        somewhere = prog
      end
    }

    # Run solver
    runSolver(somewhere, reservationID, assigned_peers)

    # Run related problem set
    runProblemSet(reservationID, assigned_peers, execution, parameters)
	end

end


end
end
end
end
