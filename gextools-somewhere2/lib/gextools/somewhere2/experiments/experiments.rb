#
# Gextools Somewhere2 - Somewhere2 component (
# https://sourcesup.renater.fr/projects/gextools/ ) - This file is part of
# Gextools Somewhere2
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

module GexTools
module Components
module Somewhere2
module Experiments

require 'gextools/experiments/experiments'
require 'gextools/progs/progs'
require 'gextools/somewhere2/experiments/experiment'



# Somewhere experiments based operation
class Experiments < GexTools::Experiments::Experiments


	#####################
	protected



	#####################
	public


	# Parameters
	@param = nil

	def genCmdLine
    global = case $config[:command]
      when "register"
        Trollop::options do
          opt :executableName, "Name of the solver to use", :type => String, :short => '-s', :required => true
          opt :executablerVersion, "Version of the solver to use", :type => String, :short => '-S', :required => true
          stop_on_unknown
        end
      when "execute"
        Trollop::options do
          opt :deleteMappings, "If you want to ignore the mappings of the instance", :short => '-d', :default => false
          opt :ttl, "Time to leave of the deployed programs", :type => :int, :short => '-t', :default => 60
          opt :bl, "Bounded lenghts: maximum number of literals on the result", :type => :int, :short => '-b'
          stop_on_unknown
        end
    end

		@param = global
	end

  #------------------------------------------#
	# Deals with the creation of an experiment #
	# object                          				 #
  #------------------------------------------#
	def generate(param={})
		raise ArgumentError.new(:param) if param.nil?

    opts = @param.merge(param)

		experiment = Experiment.new
		experiment.name = opts[:name]
		experiment.problem_set = opts[:problem_set]
    experiment.deployment = opts[:deployment]

    somewhere = GexTools::Progs::Progs.findWantedProg(opts[:executableName], opts[:executablerVersion])

		experiment.progs << somewhere

		return experiment
	end

  #------------------------------------------#
	# Returns used execution parameters        #
  #------------------------------------------#
  def getParameters()
    return @param
  end

	def initialize
		genCmdLine
	end

end



end
end
end
end