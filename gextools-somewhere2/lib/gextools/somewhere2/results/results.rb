#
# Gextools Somewhere2 - Somewhere2 component (
# https://sourcesup.renater.fr/projects/gextools/ ) - This file is part of
# Gextools Somewhere2
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'builder'
require 'gchart'

module GexTools
module Components
module Somewhere2
module Results

require 'gextools/results/result'
require 'gextools/results/result_status'
require 'gextools/results/results'
require 'gextools/problems/problem'
require 'gextools/executions/execution'

# Somewhere Results based object
class Results < GexTools::Results::Results


	#####################
	protected



	#####################
	public

	# Parameters
	@param = nil

	def genCmdLine
		global = Trollop::options do
		end

		@param = global
	end


  #------------------------------------------#
	# Analyze the answers of a somewhere       #
  # execution and build results objects over #
  # it.                                      #
  #------------------------------------------#
	def saveResultsOfProblem(opts={})
    begin
      # Get useful parameters
      execution = opts[:execution]
      raise ArgumentError.new(:execution) if execution.nil?
      content = opts[:result_string]
      raise ArgumentError.new(:result_string) if content.nil?
      problem = opts[:problem]
      raise ArgumentError.new(:problem) if problem.nil?

      # Obtain execution ttl
      params = opts[:params]
      ttl = 30
      ttl = params[:ttl] unless params[:ttl].nil?

      # Build the result object
      result = GexTools::Results::Result.new
      result.rawOutput = content
      result.execution = execution
      result.problem = problem
      result.completionTime = 0

      # Build the main xml to be saved as "extractedResult"
      xml = ''
      doc = Builder::XmlMarkup.new(:target=> xml, :indent => 1 )
      doc.instruct! :xml, :encoding => "UTF-8"

      # Root element
      doc.extracted do

        # Get the concerned problem in order to be "query" key
        doc.query(problem.concrete_problem.getConcernedProblem())

        # Remove subsumed lines
        subsumed_content = removeSubsumed(content)

        lines = subsumed_content.split("\n");

        # Build "extractedResult"
        doc.results do
          lines.each { |line|
            line.strip!
            unless line.empty? then
              time_line = (line =~ /All results obtained in (.+) seconds./)

              # If the line is an answer
              if time_line.nil? then
                line =~ /#\s+(.+)s\s+(\d+)\s+\(([^\)]+)\)\s+(.+)/

                next if ($1.nil? or $1.empty?)

                # First part will be the time, the second one will be the order and then
                # it can be obtained the correspondent clause and its mapping support (if exists)
                doc.result do |r|
                  r.time($1)
                  r.order($2)
                  r.clause($3)
                  r.annotations($4) unless $4.nil?
                end
              else
                # If the line specifies the total execution time
                result.completionTime = $1
              end
            end
          }
        end
      end

      result.result_status = analyzeResultStatus(result.completionTime, content, ttl)
      result.extractedResults =  xml
      result.save
    rescue
      $logger.error "Error building the result for problem : #{$!}"
      raise
    end
  end

  #------------------------------------------#
	# Remove subsumed answers from rawOutput   #
  # in order to build a constant extracted   #
  # answers set                              #
  #------------------------------------------#
  def removeSubsumed(content)
    subsumed_contents = [];
    lines = content.split("\n");

    lines.each { |line|
      line =~ /(\(SUBSUMES\) ->)/

      unless ($1.nil? or $1.empty?)
        
        # Obtaining information about what the line will be subsumed by this one
        subsumes_line_parts = line.split(/\(SUBSUMES\) ->/)
        subsumed_information = subsumes_line_parts[1].strip

        line.gsub! "(SUBSUMES) ->", ""
        line.gsub! subsumes_line_parts[1], ""
        line.strip!

        subsumed_contents = subsumed_information.scan(/\d/)
      end
    }

    # Use the collected information to erase subsumed lines from the extracted result.
    subsumed_contents.each{ |subsumed_content|
      lines.delete_if { |line|
        line =~ /# [\d\.]+s #{subsumed_content} /
      }
    }

    return lines.join("\n")
  end

  #------------------------------------------#
	# Analyze the somewhere answer in order to #
  # define its status                        #
  #------------------------------------------#
  def analyzeResultStatus(completionTime, content, ttl)
    begin
      # If the answer contains common somewhere fail strings
      if (content =~ /could not listen on management port/ || 
            content =~ /Could not resolve hostname/ ||
            content =~ /Exception/ ||
            content =~ /Permission denied/ ||
            content =~ /Unable to access jarfile/
            content =~ /oardo: Cannot get information from user/ ||
            content =~ /illegal option/) then
        status = GexTools::Results::Results.findFailResultStatus()
        return status
      end

      # If the answer do not contains an answer
      if (completionTime >= ttl) then
        status = GexTools::Results::Results.findTimeoutResultStatus()
        return status
      end

      # Else.. the answer is obtained due a successful execution of somewhere!
      status = GexTools::Results::Results.findSucessResultStatus()
      return status
    rescue
      $logger.error "Error trying to obtain the correct result status : #{$!}"
      raise
    end
  end

  @@latexExportHeader = "
  \\documentclass[a4paper,14pt]{report}
  \\usepackage[utf8]{inputenc}
  \\usepackage[T1]{fontenc}
  \\usepackage[normalem]{ulem}
  \\pdfcompresslevel=9

  \\begin{document}
  \\date{}

  \\begin{tabular}{|c|c|c|c|c|c|}
  \\hline
  Query & Experiment & Results & Time & Clause & Order \\\\
  \\hline

	"

	@@latexExportTail = "
		\\hline
		\\end{tabular}
		\\end{document}
	"

  #------------------------------------------#
	# Export results in latex format TODO      #
  #------------------------------------------#
  def exportLatex(path, experiment, results)
    begin
      instance = experiment.instance

      # If the parent dir doesnt yet exists, create it
      pathParent = File.join(path, experiment.name)
      FileUtils.mkdir_p pathParent unless File.exists? pathParent

      File.open(path, "w+") { |file|
				file.write @@latexExportHeader # Write header

        # Write results
        results.each { |result|
          str += "#{result.problem.concrete_problem.getConcernedProblem} & #{experiment.name} & #{result.completionTime} \\\\\n"
          file.write str
        }

				file.write @@latexExportTail # Write Tail
			}
    rescue Exception
			$logger.error "There was an error generating results latex files ... #{$!}"
			raise
		end
  end

  #------------------------------------------#
	# Export results in xml format             #
  #------------------------------------------#
  def exportXML(path, experiment, results)
    begin
      results.each { |result|
        # If the parent dir doesnt yet exists, create it
        pathParent = File.join(path, experiment.name, result.execution.label)
        FileUtils.mkdir_p pathParent unless File.exists? pathParent

        # Creating file name for result
        file_path = File.join(pathParent , "#{result.problem.id}.xml")

        # Write results
        File.open(file_path, "w+") { |file|
          file.write result.extractedResults
        }
      }
    rescue Exception
			$logger.error "There was an error generating results xml files ... #{$!}"
			raise
		end
  end

  #------------------------------------------#
	# Export some graphics related to a set of #
  # somewhere executions                     #
  #------------------------------------------#
  def exportGraphs(path, experiment, execution_type_results={})
    begin
      raise not_implemented
    rescue
      $logger.info "Somewhere component do not have any specific comparison method."
    end
	end


  #------------------------------------------#
	# Compare results for 1, 10, 100 and 1000  #
  # results                                  #
  #------------------------------------------#
  def compare(path, elements_to_compare)
    begin
      raise not_implemented
    rescue
      $logger.info "Somewhere component do not have any specific comparison method."
    end
  end

  
	def initialize
		genCmdLine
	end

end



end
end
end
end