require 'test/unit'
require 'test/IntegrationTestHelper'

class InconsistenceTest < Test::Unit::TestCase

  @@source_folder = "lib"

  def testInconsistence_1

    # Deleting previous executions of this experiment
    system "cd #{@@source_folder};./gexExperiments.rb -v -n integrationTestInconsistence1 delete -l it_inconsistence_1"

    # Execute experiment again
    system "cd #{@@source_folder};./gexExperiments.rb -v -n integrationTestInconsistence1 execute -l it_inconsistence_1 somewhere -d"
    system "cd #{@@source_folder};./gexResults.rb -v -n integrationTestInconsistence1 export -f XML"

    comparation_result = IntegrationTestHelper.compare("integrationTestInconsistence1", "it_inconsistence_1")

    assert(comparation_result)
  end

  def testInconsistence_2

    # Deleting previous executions of this experiment
    system "cd #{@@source_folder};./gexExperiments.rb -v -n integrationTest4rs3 delete -l it_inconsistence_2"

    # Execute experiment again
    system "cd #{@@source_folder};./gexExperiments.rb -v -n integrationTest4rs3 execute -l it_inconsistence_2 somewhere -d"
    system "cd #{@@source_folder};./gexResults.rb -v -n integrationTest4rs3 export -f XML"

    comparation_result = IntegrationTestHelper.compare("integrationTest4rs3", "it_inconsistence_2")

    assert(comparation_result)
  end

  def testInconsistence_3

    # Deleting previous executions of this experiment
    system "cd #{@@source_folder};./gexExperiments.rb -v -n integrationTestSl4 delete -l it_inconsistence_3"

    # Execute experiment again
    system "cd #{@@source_folder};./gexExperiments.rb -v -n integrationTestSl4 execute -l it_inconsistence_3 somewhere -d"
    system "cd #{@@source_folder};./gexResults.rb -v -n integrationTestSl4 export -f XML"

    comparation_result = IntegrationTestHelper.compare("integrationTestSl4", "it_inconsistence_3")

    assert(comparation_result)
  end

end
