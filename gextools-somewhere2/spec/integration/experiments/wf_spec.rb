require 'test/unit'
require 'test/it_test_helper'

class WellFoundedTest < Test::Unit::TestCase

  @@source_folder = "lib"

  def testWellfounded

    # Deleting previous executions of this experiment
    system "cd #{@@source_folder};./gexExperiments.rb -v -n integrationTestWellFounded delete -l it_wellfounded_1"

    # Execute experiment again
    system "cd #{@@source_folder};./gexExperiments.rb -v -n integrationTestWellFounded execute -l it_wellfounded_1 somewhere -w"
    system "cd #{@@source_folder};./gexResults.rb -v -n integrationTestWellFounded export -f XML"

    comparation_result = IntegrationTestHelper.compare("integrationTestWellFounded", "it_wellfounded_1")

    assert(comparation_result)
  end

end
