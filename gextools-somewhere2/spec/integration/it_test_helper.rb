require 'rubygems'
require 'xmlsimple'

class IntegrationTestHelper

  @@export_folder = "../data/export/results"
  @@expected_folder = "../data/expected/results"


  def self.compare(experiment_name, execution_label)
    export_dir = File.join(@@export_folder, "xml", experiment_name, execution_label)
    expected_dir = File.join(@@expected_folder, "xml", experiment_name, execution_label)

    raise "One of the comparation directories do not exist!" unless File.exist?(export_dir) and File.exist?(expected_dir)

    return self.compareDirs(export_dir, expected_dir)
  end


  def self.compareDirs(export_dir, expected_dir)
    Dir.foreach(expected_dir) { |entry|
      exported_file = File.join(export_dir, entry)
      expected_file = File.join(expected_dir, entry)

      if File.extname(entry) == ".xml" then
        exported_result = XmlSimple.xml_in(exported_file)
        expected_result = XmlSimple.xml_in(expected_file)

        result = expected_result["results"].first["result"]
        unless result.nil? then
          result.each { |expect_res|
            expected_entry = [expect_res["clause"].first , expect_res["annotations"].first]

            contains = false
            result = exported_result["results"].first["result"]
            unless result.nil? then
              result.each do |obtained_res|
                obtained_entry = [obtained_res["clause"].first,  obtained_res["annotations"].first]

                if self.compareEntries(expected_entry, obtained_entry) then
                  contains = true
                  break
                end
              end

              return false unless contains
            end
          }
        end
      end
    }
    return true
  end


  def self.compareEntries(expected_entry = [], obtained_entry = [])

    expectedClause = expected_entry[0]
    expectedMappingSupport = expected_entry[1]

    obtainedClause = obtained_entry[0]
    obtainedMappingSupport = obtained_entry[1]

    # Comparing clauses
    expected_clauses = expectedClause.split("&")
    obtained_clauses = obtainedClause.split("&")

    return false unless (expected_clauses - obtained_clauses).empty?

    # Comparing mapping supports
    expectedMappingList = expectedMappingSupport.scan(/\[+(\S+?)\]+/)
    obtainedMappingList = obtainedMappingSupport.scan(/\[+(\S+?)\]+/)

    expectedMappingList.each_with_index{ |mapping, index|
      obtainedMapping = obtainedMappingList[index]

      expected_mappings = mapping.first.split(",")
      obtained_mappings = obtainedMapping.first.split(",")

      return false unless (expected_mappings - obtained_mappings).empty?
    }

    return true
  end
  
end
