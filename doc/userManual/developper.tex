
\chapter{Creating your own Component}

First of all, each GExTools component acts like a ruby gem and must follow its conventions. However,it is not on the scope of this document the discussing about these conventions but good guides can be found around. \footnote{For example: http://guides.rubygems.org/make-your-own-gem/ }

Also, it is important to remind the reader that in order to use an \emph{in-development} gem as a GExTools component, you will need to indicate the path of its code on the \emph{component.yml} configuration file using the \emph{component\_dev\_path} (if it is a program component) or \emph{component\_dev\_path} (if it is a grid component). It is also necessary that the gem source would be accessible on the system Ruby's \emph{\$LOAD\_PATH}.

\section{A word about the key GExTools class}

Every executable class of GExTools must extend the \emph{App} class. This class is responsible for mixing parameters passed by command-line with the parameters collected on the \emph{database.yml} and \emph{component.yml} files. 
Three important global variables are created during the instantiation of any subclass of the \emph{App} class.
\begin{description}
	\item[\$config] Contains a hash indexing all the parameters contained on the \emph{database.yml} and \emph{component.yml} files.
	\item[\$verbose] Boolean variable indicating that the \emph{verbose} option was used at command-line.
	\item[\$logger] Represents a instance of the logger used by GExTools.
\end{description}

The \emph{Component Manager} class is also important in the context of using components \emph{Operation} subclasses (more information about the \emph{Operation} class on \ref{sec:generalinfo}). From it's main public method (\emph{self.managerClass(component, part)}), the developer can retrieve the operation referring to a specific GExTools concept. For example:

\begin{lstlisting}
	ComponentManager.managerClass(:somewhere, :instances)
\end{lstlisting}

This call will obtain the operation class referring to the \emph{Instances} concept of the \emph{Somewhere} component.

\section{General information}
\label{sec:generalinfo}

In order to create a GExTools component, you need to extend some of the main classes specified on the GExTools core gem. For each basic concept of GExTools, there are two abstract classes that may need to be extent:
\begin{itemize}
	\item Operation Subclass : For each GExTools concept , it exists an extension of the \emph{Operation} class and it may be specialized on a specific component. These classes are named with target concept name on the plural, for example :  the operation implemented for the\emph{Deployment} concept is called \emph{Deployments}.  On your specific component, an \emph{Operation} subclass is necessary for either describe component specific parameters (using \emph{Trollop}) or implement some of the abstract superclass methods.
	\item ActiveRecord Subclass :  For each GExTools concept , it exists an extension of the \emph{ActiveRecord} class and it may be specialized on a specific component. These classes are named with target concept name on the singular, for example :  the \emph{ActiveRecord} class implemented for the\emph{Deployment} concept is also called \emph{Deployment}. On your specific component, an \emph{ActiveRecord} subclass is necessary in order to implement some of its abstract superclass methods.
\end{itemize}

By convention, the subclasses of GExTools \emph{Operations} and \emph{ActiveRecord} classes must have the same name of its parents, being distinguished by its module's namespace.

In the next sections we explain how to create program and grid components to be used by GExTools.

\section{Creating a program component}

In order to implement a specific program component, the following concepts needs to be extend:
\begin{enumerate}
	\item Instance
	\item Problem
	\item Experiment
	\item Result
\end{enumerate}

\subsection{Extending the Instance concept}

The \emph{Instances} operation subclass needs to be extent in order to implement the following methods:

\begin{lstlisting} [style=ruby]
class Instances < GexTools::Instances::Instances
		
	#------------------------------------------#
	# Deals with the instance registration on
	# the database.
	#------------------------------------------#
	def register(param={})
		raise NotImplementedError.new
	end
		
	#------------------------------------------#
	# Deals with the instance generation
 	#------------------------------------------#
	def generate(param={})
		raise NotImplementedError.new
	end
		
	#------------------------------------------#
	# Deals with the instance export
	#------------------------------------------#
 	def export(format, peers={})
 	 	raise NotImplementedError.new
 	end
 		 
end
\end{lstlisting}

Also, the \emph{ConcreteInstance} class need to be extent in order to controls the parsing and how to retrieve specific information of a specific instance type. Its following methods needs to be extent:

\begin{lstlisting} [style=ruby]
class MyConcreteInstance < GexTools::Instances::ConcreteInstance
	
	# IMPORTANT! A concrete instance have to be represented 
	# on database following the ActiveRecord conventions!
	self.table\_name = "my\_concrete\_instance"
		
	#------------------------------------------#
	# Gets concrete instance infos
	#------------------------------------------#
	def infos()
		raise NotImplementedError.new
	end
	
	#------------------------------------------#
	# Parse instance files. May delegate the parsing
	# action to a specific instance format.
	#------------------------------------------#
	def parse(content)
		raise NotImplementedError.new
	end
	
	#------------------------------------------#
	# Fetches instance statistics. May be useful to	
	# make calculations about the instance
	#------------------------------------------#
	def fetchInstanceStats()
		raise NotImplementedError.new
	end
 		 
end
\end{lstlisting}

Finally, one or more instance formats can be specified in your specific component, describing the behaviour of the parsing instance and export instance actions.

\begin{lstlisting} [style=ruby]
class MyFormat < GexTools::Instances::InstanceChunkFormat
		
	#------------------------------------------#
	# Create hash of peers (peerName -> peer) from
	# instance information
	#------------------------------------------#
	def self.parse(content)
		raise NotImplementedError.new
	end
	
	#------------------------------------------#
	# From hash of peers (peerName -> peer) create
	# instance content
	#------------------------------------------#
	def self.export(peers={})
		raise NotImplementedError.new
	end
 		 
end
\end{lstlisting}

Examples of specialized instance implementations can be found in the following gems : \emph{gextools-somewhere}, \emph{gextools-somewhere2} (both making use o the \emph{gextools-somewhere\_api} gem) and in the \emph{gextools-benchmark}.

\subsection{Extending the Problem concept}

The \emph{Problems} operation subclass needs to be extent in order to implement the following methods:

\begin{lstlisting} [style=ruby]
class Problems < GexTools::Problems::Problems

	#------------------------------------------#
	# Deals with the problem set registration
	# on the database.
	#------------------------------------------#
	def register(opts={})
		raise NotImplementedError.new
	end
		
	#------------------------------------------#
	# Deals with the problem set generation	
	#------------------------------------------#
	def generate(opts={})
		raise NotImplementedError.new
	end
		
end
\end{lstlisting}

Also, the \emph{ConcreteProblem} class need to be extent in order to controls the parsing and how to retrieve specific information of a specific instance type. Its following methods needs to be extent:

\begin{lstlisting} [style=ruby]
class MyProblem < GexTools::Problems::ConcreteProblem
	
	# IMPORTANT! A concrete problem have to be 
	# represented on database following the 
	# ActiveRecord conventions!
	self.table\_name = "my\_problem"
	
	#------------------------------------------#
	# Return the parameters to be used when
	# posing the problem on a program
	#------------------------------------------#
 	def getProblemString(assignedPeers)
 		raise NotImplementedError.new
 	end
 	
 	#------------------------------------------#
 	# Return the problem type short name
 	#------------------------------------------#
 	def getProblemTypeName()
 		raise NotImplementedError.new
 	end

 	#------------------------------------------#
 	# Return the problem concerned peers
	#------------------------------------------#
	def getConcernedPeers()
		raise NotImplementedError.new
	end
	
	#------------------------------------------#
	# Return the problem concerned
	# problem string
	#------------------------------------------#
	def getConcernedProblem()
		raise NotImplementedError.new
	end
  	
end
\end{lstlisting}

Examples of specialized problems implementations can be found in the following gems : \emph{gextools-somewhere}, \emph{gextools-somewhere2} (both making use o the \emph{gextools-somewhere\_api} gem), \emph{gextools-benchmark} and \emph{gextools-sat}.

\subsection{Extending the Experiment concept}

The \emph{Experiments} operation subclass needs to be extent in order to implement the following methods:

\begin{lstlisting} [style=ruby]
class Experiment < GexTools::Experiments::Experiments

	#------------------------------------------# 
	# Deals with the creation of an experiment	
	# object 
	#------------------------------------------#
	def generate(param={})
		raise NotImplementedError.new
	end
	
end
\end{lstlisting}

Also, the \emph{Experiment} \emph{activerecord's} class need to be extent in order to controls the execution flow of the experiment. Its following methods needs to be extent:

\begin{lstlisting} [style=ruby]
class Experiment < GexTools::Experiments::Experiment

	#------------------------------------------#
	# Validate if the experiment was created
	# with the correct programs
	#------------------------------------------#
	def correct\_progs?
		raise NotImplementedError.new
	end
	
	#------------------------------------------#
	# Perform transformations on instance files
	# and send it to the server
	#------------------------------------------#
	def transformsAndSendInstanceFiles(instance, assignedPeers, 
		parameters={})
		raise NotImplementedError.new
	end
	
	#------------------------------------------#
	# Execute this experiment!!!
	#------------------------------------------#
	def execute(reservationID, assignedPeers, execution, 
		parameters={})
		raise NotImplementedError.new
	end
  	
end
\end{lstlisting}

Examples of specialized experiments implementations can be found in the following gems : \emph{gextools-somewhere}, \emph{gextools-somewhere2}, \emph{gextools-benchmark} and \emph{gextools-sat}.

\subsection{Extending the Result concept}

The \emph{Results} operation subclass needs to be extent in order to implement the following methods:

\begin{lstlisting} [style=ruby]
class Results < GexTools::Results::Results

	#------------------------------------------#
	# Analyse the answers of an experiment
	# execution and build results objects over 	it.
	#------------------------------------------#
	def saveResultsOfProblem(opts={})
		raise NotImplementedError.new
	end
	
	#------------------------------------------#
	# Export results in latex format
	#------------------------------------------#
	def exportLatex(path, experiment, results)
		raise NotImplementedError.new
	end
	
	#------------------------------------------#
	# Export results in xml format
	#------------------------------------------#
	def exportXML(path, experiment, results)
		raise NotImplementedError.new
	end
	
	#------------------------------------------#
	# Export some graphics related to a set of
	# somewhere executions
	#------------------------------------------#
	def exportGraphs(path, experiment, 
		execution\_type\_results={})
		raise NotImplementedError.new
	end
	
	#------------------------------------------#
	# Compare the results of some experiments or
	# executions
	#------------------------------------------#
	def compare(path, elements\_to\_compare)
		raise NotImplementedError.new
	end
  		
end
\end{lstlisting}

The \emph{Result} \emph{activerecord's} class does not need to be implemented for a program component. An example of implementations of the \emph{Result} concept for program components can be found on the following gems: \emph{gextools-somewhere}, \emph{gextools-somewhere2}, \emph{gextools-benchmark} and \emph{gextools-sat}.

\section{Creating a grid component}

In order to implement a specific grid component, the following concepts need to be extend:
\begin{enumerate}
	\item Deployment
	\item Result
\end{enumerate}

\subsection{Extending the Deployment concept}

The \emph{Deployment} operation subclass needs to be extent in order to implement the following methods:

\begin{lstlisting} [style=ruby]
class Deployments < GexTools::Deployments::Deployments

	#------------------------------------------#
	# Create deployment object
	#------------------------------------------#
	def generate(opts={})
		raise NotImplementedError.new
	end
		
end
\end{lstlisting}

Also, the \emph{Deployment} \emph{activerecord's} class need to be extent in order to controls how the the grid will be accessed by GExTools. Its following methods needs to be extent:

\begin{lstlisting} [style=ruby]
class Deployment < GexTools::Deployments::Deployment

	#------------------------------------------#
	# Get nodes reserved by this deployment
	#------------------------------------------#
	def getFreePortOnNode(machine)
		raise NotImplementedError.new
	end
	
	#------------------------------------------#
	# Get gateway access host name
	#------------------------------------------#
	def getAccessHostName()
		raise NotImplementedError.new
	end
	
	#------------------------------------------#
	# Get nodes reserved by this deployment
	#------------------------------------------#
	def getReservedNodesNames
		raise NotImplementedError.new
	end
	
	#------------------------------------------#
	# Reserve grid nodes
	#------------------------------------------#
	def runReservation(vlan, whenTime, wallTime)
		raise NotImplementedError.new
	end
	
	#------------------------------------------#
	# Copy from the grid (path on the grid
	# start with the home of the connected
	# user)
	#------------------------------------------#
	def copyFromTheGrid(baseDir, tempDir)
		raise NotImplementedError.new
	end
	
	#------------------------------------------#
	# Copy files to the grid
	#------------------------------------------#
	def copyToTheGrid(tempDir)
		raise NotImplementedError.new
	end
	
	#------------------------------------------#
	# Copy on the grid
	#------------------------------------------#
	def copyOnTheGrid(tempDir, 
		to\_specific\_resources = [])
		raise NotImplementedError.new
	end
	
	#------------------------------------------#
	# Copy from remote clusters, on the grid
	#------------------------------------------#
	def copyFromRemoteOnTheGrid(tempDir, 
		from\_specific\_resources = [])
		raise NotImplementedError.new
	end
	
	#------------------------------------------#
	# Delete on the grid
	#------------------------------------------#
	def delOnTheGrid(tempDir)
		raise NotImplementedError.new
	end
		
end
\end{lstlisting}

An example of implementations of the \emph{Deployment} concept for grid components can be found on the \emph{gextools-oar} gem.

\subsection{Extending the Result concept}

The \emph{Result} operation subclass needs to be extent in order to implement the following methods:

\begin{lstlisting} [style=ruby]
class Results < GexTools::Results::Results

	#------------------------------------------#
	# Retrieve raw results of an experiment
	# on this grid and save them in the database
	#------------------------------------------#
	def retreive(opts={})
		raise NotImplementedError.new
	end
		
end
\end{lstlisting}

The \emph{Result} \emph{activerecord's} class does not need to be implemented for a grid component. An example of implementations of the \emph{Result} concept for grid components can be found on the \emph{gextools-oar} gem.