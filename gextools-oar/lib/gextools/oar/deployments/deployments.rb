#
# Gextools Oar - Grid management component (
# https://sourcesup.renater.fr/projects/gextools/ ) - This file is part of
# Gextools Oar
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

module GexTools
module Components
module Oar
module Deployments

require 'gextools/deployments/deployment_type'
require 'gextools/deployments/deployments'
require 'gextools/oar/deployments/deployment.rb'


# OAR Deployments based operation
class Deployments < GexTools::Deployments::Deployments


	#####################
	public

  #------------------------------------------#
	# Create deployment object                 #
  #------------------------------------------#
	def generate(opts={})
		raise ArgumentError.new(:opts) if opts.nil?

		deployment = Deployment.new
		deployment.name = opts[:name]
    deployment.deployment_type = opts[:type]
		deployment.physicalConfiguration = opts[:physicalConfiguration]

		return deployment
	end
  

end



end
end
end
end