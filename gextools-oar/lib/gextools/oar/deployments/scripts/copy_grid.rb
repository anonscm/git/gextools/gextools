#!/usr/bin/env ruby
#
# Gextools Oar - Grid management component (
# https://sourcesup.renater.fr/projects/gextools/ ) - This file is part of
# Gextools Oar
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#
# == Synopsis
#
# Copy/Delete files on the Grid5000
# This script is meant to replace 'copyGrid.sh'.
#
# == Usage
#
# copy_grid.rb command [command Options]
# where commands are :
#   copy
#   delete
# where [command Options] are :
#
# -p, --path
#	Path to the folder/file that must be copied.
#	-r, --resources
#	List of resources to be used. Must be: site names or cluster names.
# -a, --all
# All available sites should be concerned.
# -v, --verbose
# Verbose mode.

begin
	require 'rubygems'
	require 'getoptlong'
  require 'rdoc/usage'
  require 'json'
  require 'restclient'
  require 'net/ssh/multi'
  require 'fileutils'
rescue Exception
	abort("There are missing dependancies, please install required gems. (cf user manual) ")
end

#------------------------------------------#
# Commands                                 #
#------------------------------------------#
@cmdOpts = GetoptLong.new(
  [ '--help', '-h', GetoptLong::NO_ARGUMENT ],
  [ '--path', '-p', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--resources', '-r', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--all', '-a', GetoptLong::NO_ARGUMENT ],
  [ '--verbose', '-v', GetoptLong::NO_ARGUMENT ]
)


#------------------------------------------#
# Initialize grid information              #
#------------------------------------------#
def init
  begin
    @local_machine = Socket.gethostname
    @valid_sites = []

    puts "\nGetting information about the sites on grid5k..."

    # Get a list of all the sites of the grid5k.
    api = RestClient::Resource.new('https://api.grid5000.fr', :timeout => 5)
    sites = JSON.parse api['/2.0/grid5000/sites'].get(:accept => 'application/json').body
    sites['items'].each do |site|
      site_status_link = site['links'].detect{|link| link['title'] == 'status'}
      begin
        # Check connection with a site...
        JSON.parse api[site_status_link['href']].get(:accept => 'application/json').body

        # If the connection is valid, then include the site in the valid_sites list.
        if (@config[:all] or (@config[:resources].include?(site['uid']) and
              !@local_machine.include?(site['uid'])))
          # if resource is detected as a site and it is avaliable, put it on the list.
          @valid_sites.push(site['uid'])
        else
          # Verify if the indicated resource is a cluster on some site...
          site_clusters = JSON.parse api["/2.0/grid5000/sites/#{site['uid']}/clusters"].get(:accept => 'application/json').body
          site_clusters['items'].each do |cluster|
            if (@config[:resources].include?(cluster['uid']) and
                  !@local_machine.include?(site['uid']))
              # If resource is detected as a site and it is avaliable, put it on the list.
              @valid_sites.push(site['uid'])
            end
          end
        end

      rescue RestClient::Exception
        puts "Warning : #{site['uid']} site seens down."
        sleep 1
      end
    end
    puts "Information collected. Executing selected operation on avaliable sites..." unless @valid_sites.empty?
  end
end

#------------------------------------------#
# Delete script                            #
#------------------------------------------#
def delete(path)
  unless @valid_sites.empty?   
    Net::SSH::Multi.start(:on_error => :warn) do |session|
      # define servers in groups for more granular access
      session.group :app do
        @valid_sites.each do |site|
          session.use site
        end
      end

      # execute commands on a subset of servers
      del_command = "rm -rf #{path}"
      session.with(:app).exec(del_command) do |ch, stream, data|
        if (@config[:verbose]) then
          puts "[#{ch[:host]}] #{data}"
        end
      end

      # run the aggregated event loop
      session.loop
    end
  end

  if ((@config[:all] = true) or (@config[:resources] == nil)) then
    # Deleting locally
    if File.exists?(path) then
      FileUtils.rm_rf(path)
    end
  end
end

#------------------------------------------#
# Copy script                              #
#------------------------------------------#
def copy(path)
  puts @valid_sites
  return if @valid_sites.empty?

  Net::SSH::Multi.start(:on_error => :warn) do |session|
    # define servers in groups for more granular access
    session.group :app do
      @valid_sites.each do |site|
        session.use site
      end
    end

    session.with(:app).exec("mkdir -p #{path}")
    
    # transfer files into the grid
    copy_command = "rsync -ravz -e \"ssh  -o StrictHostKeyChecking=no\" --exclude=\".svn/\" #{@local_machine}:#{path} #{path}"
    session.with(:app).exec(copy_command) do |ch, stream, data|
      if (@config[:verbose]) then
        puts "[#{ch[:host]}] #{data}"
      end
    end

    # run the aggregated event loop
    session.loop
  end
end

#------------------------------------------#
# Copy remote script                       #
#------------------------------------------#
def copy_remote(path)
  puts @valid_sites
  return if @valid_sites.empty?

  Net::SSH::Multi.start(:on_error => :warn) do |session|
    # define servers in groups for more granular access
    session.group :app do
      @valid_sites.each do |site|
        session.use site
      end
    end

    session.with(:app).exec("mkdir -p #{path}")

    # transfer files into the grid
    copy_command = "rsync -ravz -e \"ssh  -o StrictHostKeyChecking=no\" --exclude=\".svn/\" #{path} #{@local_machine}:#{path}"
    session.with(:app).exec(copy_command) do |ch, stream, data|
      if (@config[:verbose]) then
        puts "[#{ch[:host]}] #{data}"
      end
    end

    # run the aggregated event loop
    session.loop
  end
end

#------------------------------------------#
# Main script. Do the actions specified    #
# by the command line.                     #
#------------------------------------------#
begin
  RDoc::usage if ARGV.length == 0

  @config = {}
  @cmd = ARGV.shift unless (ARGV.include?('--help') or ARGV.include?('-h'))

  # Get general command options and check if they are valid
  @cmdOpts.each { |opt, arg|
			case opt
				when "--path"
          @config[:path] = arg
          if ("#{@config[:path]}".empty?)
            abort("Path must be specified.")
          end
        when "--resources"
          @config[:resources] = arg
        when "--all"
          @config[:all] = true
        when "--verbose"
          @config[:verbose] = true
        when '--help'
          RDoc::usage
				else
					puts "Error: Invalid parameter #{opt.inspect} \m See --help."
			end
  }

  if (@config[:all] == true and (@config[:resources] != nil)) then
    puts "The \"all\" option was choosen. Desconsiderating other options..."
    @config[:resources] = nil;
  elsif (@config[:all] == false and @config[:resources] == nil and @cmd != "delete") then
    abort("You must specify a cluster list OR a site list in order to continue...")
  end

  # Get the proper command and initialize its execution
  case @cmd
    when "copy"
      if (!File.exists?(@config[:path])) then
        abort("Error: Path must exists on the local machine!")
      end

      init
      copy(@config[:path])
    when "copy_remote"
      init
      copy_remote(@config[:path])
    when "delete"
      init
      delete(@config[:path])
    else
      puts "Error: Invalid command: #{@cmd.inspect} \m See --help."
  end

  puts "Done.\n"
  
end