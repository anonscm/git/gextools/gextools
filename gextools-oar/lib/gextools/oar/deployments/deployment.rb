#
# Gextools Oar - Grid management component (
# https://sourcesup.renater.fr/projects/gextools/ ) - This file is part of
# Gextools Oar
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'net/ssh'
require 'rest_client'
require 'debugger'


module GexTools
module Components
module Oar
module Deployments

require 'gextools/deployments/deployment'
require 'gextools/deployments/deployment_type'


# Deployment object for OAR, as it will be recorded in the database
class Deployment < GexTools::Deployments::Deployment

  #------------------------------------------#
	# Copy from the grid (path on the grid     #
  # start with the home of the connected     #
  # user)                                    #
  #------------------------------------------#
	def copyFromTheGrid(baseDir, tempDir)

		# Get usefull connexion infos
		login = $config[:gridLogin]
		hostname = $config[:gridHostname] + ".g5k"

		$logger.info "Trying to transfert files from the grid, with login=#{login} and hostname=#{hostname} ..."

		# Prepare the rsync cmd line
		cmd = "rsync -avz #{login}@#{hostname}:~/#{baseDir} #{tempDir}/ "

		# Run the rsync command
		res = nil
		if $verbose
			res = system cmd
		else
			res = system(cmd + " >/dev/null")
		end

		raise "Error copying files from the grid" unless res == true
		$logger.info "Successfully transfered files from the grid ..."
	end

  #------------------------------------------#
	# Copy files to the grid                   #
  #------------------------------------------#
	def copyToTheGrid(tempDir)

		# Get usefull connexion infos
		login = $config[:gridLogin]
		hostname = $config[:gridHostname] + ".g5k"

		$logger.info "Trying to transfert files on the grid, with login=#{login} and hostname=#{hostname} ..."

		# Prepare the rsync cmd line
		cmd = "rsync -avz #{tempDir}/* #{login}@#{hostname}:~"

		# Run the rsync command
		res = nil
		if $verbose
			res = system cmd
		else
			res = system(cmd + " >/dev/null")
		end

		raise "Error copying files on the grid" unless res == true
		$logger.info "Successfully transfered files on the grid ..."
	end

  #------------------------------------------#
	# Copy on the grid                         #
  #------------------------------------------#
	def copyOnTheGrid(tempDir, to_specific_resources = [])
    copy_command = ""
    if to_specific_resources.nil? or to_specific_resources.empty? then
      copy_command = "~/gextools/scripts/copy_grid.rb copy -p #{tempDir} -r '#{getConcernedResources()}'"
    else
      copy_command = "~/gextools/scripts/copy_grid.rb copy -p #{tempDir} -r '#{to_specific_resources.join(" ")}'"
    end
    copy_command += " -v" if $verbose
		runCmd(copy_command)
	end

  #------------------------------------------#
	# Copy on the grid                         #
  #------------------------------------------#
	def copyFromRemoteOnTheGrid(tempDir, from_specific_resources = [])
    copy_command = ""
    if from_specific_resources.nil? or from_specific_resources.empty? then
      copy_command = "~/gextools/scripts/copy_grid.rb copy_remote -p #{tempDir} -r '#{getConcernedResources()}'"
    else
      copy_command = "~/gextools/scripts/copy_grid.rb copy_remote -p #{tempDir} -r '#{from_specific_resources.join(" ")}'"
    end
    copy_command += " -v" if $verbose
		runCmd(copy_command)
	end

  #------------------------------------------#
	# Delete on the grid                       #
  #------------------------------------------#
	def delOnTheGrid(tempDir)
		del_command = "~/gextools/scripts/copy_grid.rb delete -p #{tempDir} -r '#{getConcernedResources()}'"
    del_command += " -v" if $verbose
		runCmd(del_command)
	end

  #------------------------------------------#
  # Reserve grid nodes with oargridsub       #
  #------------------------------------------#
  def runReservation(vlan, whenTime, wallTime)
    # Inserts vlan information
    temp_conf = String.new(physicalConfiguration)
    temp_conf.sub!(/(\w+):rdef="/, "\\1:rdef=\"{\\\\\\\\\\\\\\\\\\\\\\\\\\\"type='kavlan-global'\\\\\\\\\\\\\\\\\\\\\\\\\\\"}/vlan=1+") if vlan

    # Run and Get the result
    reservation_str = "oargridsub"
    reservation_str += " -t deploy" if vlan
    reservation_str += " -s '#{whenTime}'" unless whenTime.nil?
    reservation_str += " -w #{wallTime}" unless wallTime.nil?
    reservation_str += " #{temp_conf}"
    result = runCmd(reservation_str)
    raise "SSH timeout" if result.nil?

    # Get the oar reservation number on each site
    batches_site_id = result.scan(/\[OAR_GRIDSUB\] \[(\w+)\] Reservation success on \w+ : batchId = (\w+)/)
    @batches = Hash[batches_site_id]

    # Get the oargrid number
    result =~ /Grid reservation id = (\d+)/
    oargrid_number = $1

    raise "Problem during oargrid reservation" if oargrid_number.nil?
    sleep 1

    $logger.info "OARGRID Reservation #{oargrid_number} reserved"

    return oargrid_number
  end

  #------------------------------------------#
  # Create a deployment using kdeploy        #
  #------------------------------------------#
  def create_deployment(oargrid_number)
    $logger.info "Creating multi-cluster deployment..."

    node_names = getReservedNodesNames(oargrid_number)

    @vlan_id = nil
    @batches.each { |frontend, job_id|
      vlan_id_str = runCmd(frontend, "kavlan -V -j #{job_id}")
      if (!vlan_id_str.nil? and vlan_id_str =~ /(\d+)/)
        @vlan_id = $1
        break
      end
    }

    raise "No VLan was identified" if @vlan_id.nil?

    result = runCmd("kadeploy3 -e squeeze-x64-big -f - -k --multi-server --vlan #{@vlan_id} <<< \"#{node_names.join("\n")}\"")
    $logger.info result if $verbose

    $logger.info "Done."
  end

  #------------------------------------------#
  # Get an array of the reserved nodes names #
  #------------------------------------------#
  def getReservedNodesNames(oargrid_number)
    node_names = runCmd("oargridstat -wl #{oargrid_number}").split("\n").uniq
    node_names.delete_if { |node_name|
      node_name =~ /Not all jobs are running on/
    }

    unless (@vlan_id.nil?) then
      node_names.each { |name|
        name.sub!(/(\w+)-(\d+)./, "\\1-\\2-kavlan-#{@vlan_id}.")
      }
    end

    return node_names
  end

  #------------------------------------------#
	# Get gateway access host name             #
  #------------------------------------------#
  def getAccessHostName()
    $config[:gridHostname] =~ /(.+)\.(.+)\.(.+)\.(.+)/
    return $2
  end

  #------------------------------------------#
  # Delete reservation on the grid           #
  #------------------------------------------#
  def deleteReservation(oargrid_number)
    return runCmd("oargriddel -l #{oargrid_number}")
  end

  #------------------------------------------#
  # Get an array of the concerned site names #
  #------------------------------------------#
  def getConcernedSiteNames(oargrid_number)
    sites = []
    nodes = runCmd("oargridstat -l #{oargrid_number}").split("\n")
    nodes.each { |node|
      site_name = node.split(".").fetch(1)
      sites.push(" #{site_name}") unless sites.include?(" #{site_name}")
    }
    return sites.to_s.strip
  end

  #------------------------------------------#
  # Gets the name of the resource (cluster   #
  # or site) on the deployment physical      #
  # configuration                            #
  #------------------------------------------#
  def getConcernedResources()
    resources = []
    resources_physic = physicalConfiguration.split(",")
    resources_physic.each { |resource_conf|
      resource_name = resource_conf.split(":")[0]
      resources.push(resource_name)
    }
    return resources.join(" ")
  end

  #------------------------------------------#
	# Get nodes reserved by this deployment    #
  #------------------------------------------#
  def getFreePortOnNode(reservation_number, machine)
    params = {}
    params[:machine] = machine
    params[:reservationNumber] = reservation_number
    params[:cmd] = "~/gextools/scripts/free_port_discover.rb"
    
    res = runCmdOnNode(params)
    return res.strip
  end

  #------------------------------------------#
  # Run experiment programs on the grid      #
  #------------------------------------------#
	def runCmdOnNode(opts={})
    if (@vlan_id.nil?) then
      cmd = "OAR_JOB_KEY_FILE=$(ls /tmp/oargrid/oargrid_ssh_key_*_#{opts[:reservationNumber]}) oarsh #{opts[:machine]} \"cd;"
    else
      cmd = "ssh #{opts[:machine]} \"cd;"
    end

    cmd += opts[:cmd] + "\""

		res = runCmd(cmd)
		return res
	end

  #------------------------------------------#
  # Run a command on the grid                #
  #------------------------------------------#
	def runCmd(frontend = nil, cmd)
		res = nil
		begin
      frontend = $config[:gridHostname] if frontend.nil?
      frontend += ".g5k"

      Net::SSH.start(frontend, $config[:gridLogin], :forward_agent => "no") do |ssh|
        $logger.info "SSH Command : " + cmd if $verbose
        res = ssh.exec!(cmd)
      end

		rescue Exception
      $logger.error "Error connecting to the grid : #{$!.inspect}"
		end

    res.gsub!(/Warning: Permanently added .+ to the list of known hosts./, "") unless res.nil?

		return res
	end

end



end
end
end
end