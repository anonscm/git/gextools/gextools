#
# Gextools Oar - Grid management component (
# https://sourcesup.renater.fr/projects/gextools/ ) - This file is part of
# Gextools Oar
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

module GexTools
module Components
module Oar
module Results

require 'gextools/results/results'
require 'gextools/experiments/experiments'
require 'gextools/executions/executions'
require 'gextools/oar/results/results.rb'
require 'gextools/oar/deployments/deployments.rb'


# OAR Results based operation
class Results < GexTools::Results::Results

  #####################
	public

  #------------------------------------------#
	# Retrieve raw results of an experiment    #
  # on this grid and save them in the        #
  # database                                 #
  #------------------------------------------#
	def retreive(opts={})
		raise ArgumentError.new(:opts) if opts.nil?

    result_string = opts[:result_string]
    execution = opts[:execution]
    experiment = execution.experiment
    problem = opts[:problem]
    params = opts[:params]
    id = opts[:id]

    $logger.info "Retreiving results of problem '#{id}' on experiment '#{experiment.name}' and saving it into the database ..."

		# Retreive files, analyse them, and save them
		begin

      # Analyse and save results on the database
      GexTools::Results::Results.saveResultsOfProblem(
        :component => $config[:component],
        :execution => execution,
        :result_string => result_string,
        :problem => problem,
        :params => params
      )

      $logger.info "Has successfully retreived results of problem '#{id}' on experiment '#{experiment.name}' ..."
		rescue
			$logger.error "There was an error retreiving results of problem '#{id}' on experiment '#{experiment.name}' : #{$!}"
			raise
		end

	end

end



end
end
end
end