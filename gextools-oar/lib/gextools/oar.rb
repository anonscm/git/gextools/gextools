#
# Gextools Oar - Grid management component (
# https://sourcesup.renater.fr/projects/gextools/ ) - This file is part of
# Gextools Oar
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca, Leo Cazenille
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

module GexTools
module Components
module Oar

  GexTools::Components::Oar.autoload(:Deployments, "gextools/oar/deployments/deployments")
  GexTools::Components::Oar.autoload(:Results, "gextools/oar/results/results")

end
end
end