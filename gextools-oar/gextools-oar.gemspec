lib = File.expand_path('../lib/', __FILE__)
$:.unshift lib unless $:.include?(lib)
 
Gem::Specification.new do |s|
  s.name = 'gextools-oar'
  s.version = '1.0'
  s.has_rdoc = true
  s.date = Time.now.utc.strftime("%Y-%m-%d")
  s.extra_rdoc_files = ['NOTICE', 'LICENSE']
  s.summary = 'Gextools Oar - Grid management component'
  s.description = s.summary
  s.author = 'Andre Fonseca'
  s.email = 'andre.amorimfonseca@gmail.com'
  s.files = %w(LICENSE NOTICE Rakefile) + Dir.glob("{lib,spec}/**/*")
  s.require_path = "lib"
  
  s.add_runtime_dependency "gextools"
  s.add_runtime_dependency "net-ssh"
  s.add_runtime_dependency "net-ssh-multi"
  s.add_runtime_dependency "getopt"
  s.add_runtime_dependency "json"
  s.add_runtime_dependency "rest-client"
end
