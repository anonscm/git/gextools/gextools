#
# Gextools Benchmark - Somewhere Benchmark component
# (https://sourcesup.renater.fr/projects/gextools/ )
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'builder'

module GexTools
module Components
module Benchmark
module Results

require 'gextools/results/result'
require 'gextools/results/result_status'
require 'gextools/results/results'
require 'gextools/problems/problem'
require 'gextools/executions/execution'

# Somewhere Results based object
class Results < GexTools::Results::Results


	#####################
	protected



	#####################
	public

	# Parameters
	@param = nil

	def genCmdLine
		global = Trollop::options do
		end

		@param = global
	end


  #------------------------------------------#
	# Analyze the answers of a somewhere       #
  # execution and build results objects over #
  # it.                                      #
  #------------------------------------------#
	def saveResultsOfProblem(opts={})
    begin
      # Get useful parameters
      execution = opts[:execution]
      raise ArgumentError.new(:execution) if execution.nil?
      problem = opts[:problem]
      raise ArgumentError.new(:problem) if problem.nil?
      content = opts[:result_string]

      # Obtain execution parameters
      params = opts[:params]

      # Build the result object
      result = GexTools::Results::Result.new
      result.rawOutput = content
      result.execution = execution
      result.problem = problem
      result.completionTime = 0

      # Build the main xml to be saved as "extractedResult"
      xml = ''
      doc = Builder::XmlMarkup.new(:target=> xml, :indent => 1 )
      doc.instruct! :xml, :encoding => "UTF-8"

      unless (content.nil?) then
        # Root element
        doc.extracted do
          timeStr = content.slice(/Benchmark took \d+ milliseconds to receive all answers/)
          result.completionTime = timeStr.slice(/\d+/) unless timeStr.nil?

          answers = content.scan(/Answer message([^=]+)/)
          answers.each { |answer|
            doc.answer do |xml_answer|
              origStr = answer.to_s.slice(/Origin : ([^\s]+)/)
              xml_answer.origin origStr.delete("Origin : ") unless origStr.nil?

              histStr = answer.to_s.slice(/History : ([^\]]+)/)
              xml_answer.history histStr.delete("History : [") unless histStr.nil?

              contentStr =  answer.to_s.slice(/Content : ([^\]]+)/)
              xml_answer.content contentStr.delete("Content : [") unless contentStr.nil?
            end
          }

        end
      end

      result.result_status = analyzeResultStatus(result.completionTime, content)
      result.extractedResults =  xml
      result.save
    rescue
      $logger.error "Error building the result for problem : #{$!}"
      raise
    end
  end

  #------------------------------------------#
	# Analyze the somewhere answer in order to #
  # define its status                        #
  #------------------------------------------#
  def analyzeResultStatus(completionTime, content)
    begin
      # If the answer contains common somewhere fail strings
      if (content.nil? ||
          content =~ /There was an error starting the benchmark/) then
        status = GexTools::Results::Results.findFailResultStatus()
        return status
      end

      # If the answer do not contains an answer
      if (completionTime >= 60000) then
        status = GexTools::Results::Results.findTimeoutResultStatus()
        return status
      end

      # Else.. the answer is obtained due a successful execution of somewhere!
      status = GexTools::Results::Results.findSucessResultStatus()
      return status
    rescue
      $logger.error "Error trying to obtain the correct result status : #{$!}"
      raise
    end
  end

  #------------------------------------------#
	# Export results in xml format             #
  #------------------------------------------#
  def exportXML(path, experiment, results)
    begin
      results.each { |result|
        # If the parent dir doesnt yet exists, create it
        pathParent = File.join(path, experiment.name, result.execution.label)
        FileUtils.mkdir_p pathParent unless File.exists? pathParent

        # Creating file name for result
        file_path = File.join(pathParent , "#{result.problem.id}.xml")

        # Write results
        File.open(file_path, "w+") { |file|
          file.write result.extractedResults
        }
      }
    rescue Exception
			$logger.error "There was an error generating results xml files ... #{$!}"
			raise
		end
  end

  #------------------------------------------#
	# Export results in Latex format           #
  #------------------------------------------#
  def exportLatex(path, experiment, execution_type_results={})
		raise not_implemented
	end


  #------------------------------------------#
	# Export some graphics related to a set of #
  # somewhere executions                     #
  #------------------------------------------#
  def exportGraphs(path, experiment, execution_type_results={})
		begin
      raise not_implemented
    rescue
      $logger.info "Benchmark component do not have any specific graphs method."
    end
	end


  #------------------------------------------#
	# Compare results for 1, 10, 100 and 1000  #
  # results                                  #
  #------------------------------------------#
  def compare(path, elements_to_compare=[])
    begin
      raise not_implemented
    rescue
      $logger.info "Benchmark component do not have any specific comparison method."
    end
  end

  
	def initialize
		genCmdLine
	end

end



end
end
end
end