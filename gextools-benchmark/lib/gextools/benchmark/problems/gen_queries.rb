#
# Gextools Benchmark - Somewhere Benchmark component
# (https://sourcesup.renater.fr/projects/gextools/ )
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

module GexTools
module Components
module Benchmark
module Problems

require 'gextools/problems/problem'

require 'gextools/benchmark/problems/query'
require 'gextools/benchmark/problems/problems'


# Benchmark Queries Generation
class GenQueries


	#####################
	protected


	#####################
	public

  #------------------------------------------#
	# Deals with 'queries' problem             #
  # generation                               #
  #------------------------------------------#
	def self.generate(opts)
		raise ArgumentError.new(:tries) if opts[:tries].nil?
    raise ArgumentError.new(:maxPayload) if opts[:maxPayload].nil?

    # Select peer from which the query will start
    instanceChunksNames = []
    opts[:instance].instanceChunks.each { |chunk|
      instanceChunksNames << chunk.name
    }

    queriedPeer = instanceChunksNames.min

		# Generate !
    problems = []
    opts[:tries].times {
      problem = GexTools::Problems::Problem.new
      concrete_query = Query.new
      concrete_query.payload = 1 + rand(opts[:maxPayload])
      concrete_query.queriedPeer = queriedPeer
      problem.concrete_problem = concrete_query

      problems << problem
    }

		return problems
	end

  #------------------------------------------#
	# Parse 'queries' problems                 #
  #------------------------------------------#
  def self.parse(line)
    raise NoMethodError.new
  end

end



end
end
end
end