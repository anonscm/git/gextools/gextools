#
# Gextools Benchmark - Somewhere Benchmark component
# (https://sourcesup.renater.fr/projects/gextools/ )
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

module GexTools
module Components
module Benchmark
module Problems

require 'gextools/problems/problems'
require 'gextools/problems/problem_set'

require 'gextools/benchmark/problems/query'
require 'gextools/benchmark/problems/gen_queries'


# Somewhere Problems based operation
class Problems < GexTools::Problems::Problems


	#####################
	protected


	#####################
	public

	# Parameters
	@param = nil

	def genCmdLine
		global = Trollop::options do
      opt :tries, "Number of tries in this benchmark", :type => :int, :short => '-t', :required => true
			opt :maxPayload, "The maximum size of the generated query", :type => :int, :short => '-p', :default => 1
		end

		@param = global
	end

  #------------------------------------------#
	# Deals with the problem set registration  #
  # on the database.                         #
  #------------------------------------------#
	def register(opts={})
    raise NoMethodError.new
	end

  #------------------------------------------#
	# Deals with the problem set generation    #
  #------------------------------------------#
	def generate(opts={})
		raise ArgumentError.new(:opts) if opts.nil?

		param = opts.merge(@param)

    problem_set = GexTools::Problems::ProblemSet.new
    problem_set.name = param[:name]
    problem_set.instance = param[:instance]

    problem_set.problems << GenQueries.generate(param)
    
		return problem_set
	end


	def initialize
		genCmdLine
	end

end



end
end
end
end