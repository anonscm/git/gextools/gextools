#
# Gextools Benchmark - Somewhere Benchmark component
# (https://sourcesup.renater.fr/projects/gextools/ )
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

module GexTools
module Components
module Benchmark
module Problems

require 'gextools/instances/instance'
require 'gextools/experiments/experiments'
require 'gextools/results/result'
require 'gextools/problems/problem'
require 'gextools/problems/concrete_problem'

# Query object for benchmark, as it will be recorded in the database
class Query < GexTools::Problems::ConcreteProblem

  self.table_name = "benchmark_queries"

  #------------------------------------------#
	# Return the problem type short name       #
  #------------------------------------------#
  def getProblemTypeName()
    return "query"
  end

  #------------------------------------------#
	# Return peer concerned by this query      #
  # For the benchmark, it will be always the #
  # first peer                               #
  #------------------------------------------#
  def getConcernedPeers()
    return queriedPeer
  end


end



end
end
end
end