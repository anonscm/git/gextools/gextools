#
# Gextools Benchmark - Somewhere Benchmark component
# (https://sourcesup.renater.fr/projects/gextools/ )
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

module GexTools
module Components
module Benchmark
module Logic

# Define benchmark peer
class Peer < Hash

	def initialize(name, lcFactor, pqFactor, combFactor, neighbors)
    raise ArgumentError.new(:name) if name.nil?
		raise ArgumentError.new(:neighbors) if neighbors.nil?

		self[:name] = name # The peer name
		self[:lcFactor] = lcFactor # The local consequences factor
    self[:pqFactor] = pqFactor # The local consequences factor
    self[:combFactor] = combFactor
		self[:neighbors] = neighbors # The list containing references to the peer's neighbors
	end

end

end
end
end
end