#
# Gextools Benchmark - Somewhere Benchmark component
# (https://sourcesup.renater.fr/projects/gextools/ )
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'fileutils'


module GexTools
module Components
module Benchmark
module Experiments

require 'gextools/temp'
require 'gextools/results/results'
require 'gextools/experiments/experiment'
require 'gextools/executions/executions'
require 'gextools/executions/helper/thread_pool'
require 'gextools/experiments/helper/schedule'

require 'gextools/benchmark/instances/instances'
require 'gextools/benchmark/problems/problems'
require 'gextools/benchmark/problems/query'

# Experiment object for somewhere, as it will be executed
class Experiment < GexTools::Experiments::Experiment

  #####################
	private

  #------------------------------------------#
	# Validate if the experiment was created   #
  # somewhere2 program                       #
  #------------------------------------------#
  def correct_progs?
    somewhere = nil
    progs.each { |prog|
      if (prog.prog_type.name == "benchmark") then
        somewhere = prog
      end
    }

    unless (progs.size == 1) and (!somewhere.nil?)
      $logger.error("A Somewhere experiment requires one benchmark jar.")
      errors.add(:progs, "A Somewhere experiment requires one benchmark program.")
    end
  end

  #------------------------------------------#
  # Change the name of the nodes in          #
  #	SomeWhere files to the name of the       #
  #	reserved nodes                           #
  #------------------------------------------#
  def transformsAndSendInstanceFiles(instance, assignedPeers, parameters={})
    begin
      $logger.info("There are #{instance.instanceChunks.size} SomeWhere files to update.")

      instance_folder = getRelativePathInsideAnExperimentFolder("~", :instance, instance.name)
      createFolderCmd = "mkdir -p #{instance_folder}"
      deployment.runCmd(createFolderCmd)

      log_folder = getRelativePathInsideAnExperimentFolder("~", :log)

      # Apply Transformation to all instances files and transfer them to the grid
      instance.instanceChunks.each { |chunk|
        $logger.info("Sending file : #{chunk.name}")
        content = chunk.chunkFile

        # Build the config xml file content
        xml = ''
        doc = ::Builder::XmlMarkup.new(:target=> xml, :indent => 1 )
        doc.instruct! :xml, :encoding => "UTF-8"

        # Configuration is need to change the NIC if using the shoal module
        doc.somewhere do
          doc.base do |base|
              base.logFile(File.join(log_folder, "#{chunk.name}.log"))
          end
        end

        peer_config_path = File.join(instance_folder , chunk.name + ".config")
        createConfigCmd = "echo '#{xml}' > #{peer_config_path}"
        deployment.runCmd(createConfigCmd)

        renamed_peer_path = File.join(instance_folder , chunk.name + "." + instance.instance_chunk_format.extension)
        copyCmd = "echo '#{content}' > #{renamed_peer_path}"
        deployment.runCmd(copyCmd)
      }

    rescue Exception
      raise "There was an error applying changing benchmark files ... #{$!}"
      puts $!.backtrace.join("\n")
    end
  end

  #------------------------------------------#
  # Run a problem on the grid                #
  #------------------------------------------#
	def runProblem(id, problem, reservationNumber, assignedPeers, expExec)
    # Get parameters
    payload = problem.concrete_problem.payload

    # Get the infos of the peer from the first literal in order to set the jar execution parameters.
    # We will need to do it again in order to correct the parameters of the problem string.
    machine = nil
    listenPort = nil
    assignedPeers.each { |peerName, params|
      if (problem.concrete_problem.getConcernedPeers == peerName) then
        machine = params[:machine]
        listenPort = params[:port]
        break
      end
    }

    # Build problem string
    opts = {}
    opts[:cmd] = "echo \"startBenchmark #{payload}\" | nc #{machine} #{listenPort}"

    opts[:reservationNumber] = reservationNumber
    opts[:machine] = machine

    resultString = deployment.runCmdOnNode(opts)

    # Fetch the results from the grid
    params = {}
    getResults(id, problem, expExec, resultString, params)
	end
  

  #------------------------------------------#
  # Run problems following an execution type #
  #------------------------------------------#
	def runProblemSet(reservationNumber, assignedPeers, expExec)
		begin
      $logger.info("## Doing problems set named " + problem_set.name)

			$logger.info("--------------- Queries Run Output --------------") if $verbose

      if (expExec.execution_type.name.upcase == "SEQUENTIAL") then
        order = 0
        problem_set.problems.each { |problem|
          runProblem(order, problem, reservationNumber, assignedPeers, expExec)
          order += 1
        }
      elsif (expExec.execution_type.name.upcase == "CONCURRENT") then
        threads = GexTools::Executions::Helper::ThreadPool.new(10)
        problem_set.problems.each { |problem|
          threads.execute{
            runProblem(threads.waiting, problem, reservationNumber, assignedPeers, expExec)
          }
        }
        threads.join
        threads.close
      end

			$logger.info("----------------- Output End -----------------") if $verbos
		rescue
			$logger.info("----------------- Output End -----------------") if $verbose
			$logger.error "Error during queries sets : #{$!}"
			raise
		end
	end

  #------------------------------------------#
  # Run program on all nodes                 #
  #------------------------------------------#
  def runSolver(prog, reservationNumber, assignedPeers)
    
    directory = getRelativePathInsideAnExperimentFolder("~", :nil)
    outputDirectory = File.join(directory, "instance")

    # Create log directory
    log_folder = getRelativePathInsideAnExperimentFolder("~", :log)
    createFolderCmd = "mkdir -p #{log_folder}"
    deployment.runCmd(createFolderCmd)

    # Get information about the target instance
    instance = self.problem_set.instance
    instance_folder = getRelativePathInsideAnExperimentFolder("~", :instance)

    # Get instance extension
    format = instance.instance_chunk_format

    assignedPeers.each{ |peerName, params|
      machine = params[:machine]
      listenPort = params[:port]

      $logger.info "Running SomeWhere on node : #{machine} ..."

      # Get instance path
      instance_path = File.join(instance_folder, instance.name, "#{peerName}.#{format.extension}")

      # Get config path
      config_path = File.join(instance_folder, instance.name, "#{peerName}.config")

      # Indicates execution output file
      execution_output = File.join(log_folder, "#{peerName}.out")

      # Execute someWhere on this node
      opts = {}
      opts[:cmd] = "#{prog.executionPrefix} #{File.join(directory, "bin", "#{prog.name}.jar")} -loadBenchmarkInstance #{instance_path} -importConfigurationFile #{config_path} -listenRemoteCommands #{listenPort} -redirectOutput remote > #{execution_output} 2>&1 & disown; sleep 10"
      opts[:reservationNumber] = reservationNumber
      opts[:machine] = machine
      deployment.runCmdOnNode(opts)
    }
  end

	#####################
	public

  #------------------------------------------#
	# Execute this experiment!!!               #
  #------------------------------------------#
  def execute(reservationID, assignedPeers, execution, parameters)

    # Getting used programs
    somewhere = nil
    progs.each { |prog|
      if (prog.prog_type.name == "benchmark" and somewhere.nil?) then
        somewhere = prog
      end
    }
    
    # Run solver
    runSolver(somewhere, reservationID, assignedPeers)

    # Run related problem set
    runProblemSet(reservationID, assignedPeers, execution)
	end

end


end
end
end
end