#
# Gextools Benchmark - Somewhere Benchmark component
# (https://sourcesup.renater.fr/projects/gextools/ )
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'fileutils'


module GexTools
module Components
module Benchmark
module Instances

require 'gextools/instances/instances'
require 'gextools/instances/instance'
require 'gextools/instances/instance_generated'
require 'gextools/progs/progs'
require 'gextools/temp'

require 'gextools/benchmark/instances/benchmark_instance'
require 'gextools/benchmark/instances/XML_format'


# Somewhere Instances based operation
class Instances < GexTools::Instances::Instances
  
	#####################
	protected



	#####################
	public

	@@KNOWN_GeneratorsTypes = ["barabassi", "watts"]

	# Parameters
	@param = nil

	def genCmdLine(generatorName)
		global = case generatorName.downcase
      when "swontogen"
        Trollop::options do
          opt :numberOfPeers, "Number of peers of the instance", :type => :int, :short => '-N', :required => false
          opt :generatorType, "Type of the generator to use", :type => String, :short => '-g', :required => false
          opt :numberOfLiteralPerPeer, "Number of Literals Per Peer", :type => :int, :short => '-p', :required => false
          opt :percentOfEquivLinks, "Percent of Equivalence Links", :type => :float, :short => '-P', :required => false
          opt :bmin, "Min Number of Branching", :type => :int, :short => '-m', :required => false
          opt :bmax, "Max Number of Branching", :type => :int, :short => '-M', :required => false
          opt :dmin, "Min Number of Depth", :type => :int, :short => '-d', :required => false
          opt :dmax, "Max Number of Depth", :type => :int, :short => '-D', :required => false
          opt :mappingGeneratorType, "Type of Mapping Generator", :type => String, :short => '-a', :required => false
          opt :fixedNumberOfMappings, "Fixed number of Mappings", :type => :int, :short => '-F', :required => false
          opt :minNumberOfMappings, "Min Number of Mappings", :type => :int, :short => '-o', :required => false
          opt :maxNumberOfMappings, "Max Number of Mappings", :type => :int, :short => '-O', :required => false
          stop_on @@KNOWN_GeneratorsTypes

          opt :initialNumberOfPeers, "Initial number of peers in a Barabassi generator configuration", :type => :int, :short => '-i', :required => false
          opt :edgesToAddPerTimestep, "Number of edges to add per timestep, in a Barabassi generator configuration", :type => :int, :short => '-e', :required => false
          opt :randomSeed, "Random Seed Number, in a Barabassi generator configuration", :type => :int, :short => '-r', :required => false
          opt :numberOfNeighbours, "Number of neighbours per peer to create, in a Watts generator configuration", :type => :int, :short => '-u', :required => false
          opt :rewritingFactor, "Rewriting Factor, in a Watts generator configuration", :type => :double, :short => '-R', :required => false
        end
      when nil, ""
        {}
      else
        Trollop::die "unknown subcommand #{$cmd.inspect}"
    end

		@param = global
	end


  #------------------------------------------#
	# Deals with the instance registration on  #
  # the database.                            #
  #------------------------------------------#
	def register(param={})
		raise ArgumentError.new(:param) if param.nil?

		opts = @param.merge(param)

		$logger.info "Registering instance '#{opts[:name]}' ..."

		instance = GexTools::Instances::Instance.new

		instance.name = opts[:name]
    instance.isGenerated = false
    instance.instance_chunk_format = opts[:format]
    instance.description = opts[:description]
    
    instance.concrete_instance = BenchmarkInstance.new

		return instance
	end


  #------------------------------------------#
	# Deals with the instance generation       #
  #------------------------------------------#
	def generate(param={})
		raise ArgumentError.new(:param) if param.nil?

		opts = @param.merge(param)

		$logger.info "Generating instance '#{opts[:name]}' ..."

		begin
			# Generate generated instance
			generator = opts[:generator]

      # Build generator parameters
      path = File.join($config[:dataDirectory], "instances", opts[:name])
      FileUtils.mkdir_p(path)

      if opts[:generator].name.downcase == "swontogen" then
        opts[:parameters] = " --swg #{opts[:generatorType]}"
        case opts[:generatorType].upcase
          when "WATTS"
            opts[:parameters] += ",#{opts[:numberOfNeighbours]}"
            opts[:parameters] += ",#{opts[:rewritingFactor]}"
          when "BARABASSI"
            opts[:parameters] += ",#{opts[:initialNumberOfPeers]}"
            opts[:parameters] += ",#{opts[:edgesToAddPerTimestep]}"
            opts[:parameters] += ",#{opts[:randomSeed]}"
          else
            $logger.error "Unknown or unsupported somewhere generator type"
            raise ArgumentError.new(:generatorType)
        end

        opts[:parameters] += " --np #{opts[:numberOfPeers]}"
        opts[:parameters] += " --nlp #{opts[:numberOfLiteralPerPeer]}"
        opts[:parameters] += " --lb #{opts[:bmin]}"
        opts[:parameters] += " --ub #{opts[:bmax]}"
        opts[:parameters] += " --ld #{opts[:dmin]}"
        opts[:parameters] += " --ud #{opts[:dmax]}"
        opts[:parameters] += " --pe #{opts[:percentOfEquivLinks]}"
        opts[:parameters] += " --mgt #{opts[:mappingGeneratorType]}"
        unless opts[:fixedNumberOfMappings].nil?
          opts[:parameters] += " --fnm #{opts[:fixedNumberOfMappings]}"
        else
          opts[:parameters] += " --vnm #{opts[:minNumberOfMappings]} #{opts[:maxNumberOfMappings]}"
        end
        opts[:parameters] += " --sf #{opts[:format].format}"
        opts[:parameters] += " --sd #{path}"
        opts[:parameters] += " --benchmark"
      end

      # Executing generator...
			generator.execute(opts)
			$logger.info "Has successfully generated instance '#{opts[:name]}' ..."

			# Register the generated instance
      opts[:path] = path
      instance = register(opts)
      instance.isGenerated = true
      generated = GexTools::Instances::InstanceGenerated.new
      generated.instance = instance
      generated.cmdLine = opts[:parameters]
      generated.save()
      
		rescue
			$logger.error "Impossible to generate instance with opts: #{opts.inspect}. Reason: #{$!.inspect}"
			raise
		end

		return instance
	end

  #------------------------------------------#
	# Deals with the instance export on        #
  #------------------------------------------#
  def export(format, peers={})
    content = nil
    if (format.upcase == "XML") then
      content = GexTools::Components::Benchmark::Instances::XMLormat.export(peers)
    end

    return content
  end


	def initialize(generatorName)
		genCmdLine(generatorName)
	end

end



end
end
end
end