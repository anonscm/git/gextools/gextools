#
# Gextools Benchmark - Somewhere Benchmark component
# (https://sourcesup.renater.fr/projects/gextools/ )
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#


module GexTools
module Components
module Benchmark
module Instances


# Instance object for somewhere, as it will be recorded in the database
class BenchmarkInstance < GexTools::Instances::ConcreteInstance

  self.table_name = "benchmark_instances"

  #------------------------------------------#
  # Gets concrete instance infos             #
  #------------------------------------------#
  def infos()
    res = "\n\tnumber of peers: #{self.peersNumber}"
    res << "\n\tmean of local answer generation probability: #{self.answersProb}"
    res << "\n\tmean of queries propagation probability: #{self.propagateProb}"
    res << "\n\tmean of combination probability: #{self.combinationProb}"
    res
  end

  #------------------------------------------#
  # Fetches instance statistics              #
  #------------------------------------------#
  def fetchInstanceStats()
    self.peersNumber = instance.peers.size
    self.answersProb = 0

    if (instance.peers.size > 0) then
      answersProbNotNormalized = 0
      instance.peers.each { |name, peer|
        answersProbNotNormalized += Float(peer[:lcFactor]) unless peer[:lcFactor].nil?
      }

      self.answersProb = answersProbNotNormalized / instance.peers.size

      propagateProbNotNormalized = 0
      instance.peers.each { |name, peer|
        propagateProbNotNormalized += Float(peer[:pqFactor]) unless peer[:pqFactor].nil?
      }

      self.propagateProb = propagateProbNotNormalized / instance.peers.size

      combinationProbNotNormalized = 0
      instance.peers.each { |name, peer|
        combinationProbNotNormalized += Float(peer[:combFactor]) unless peer[:combFactor].nil?
      }

      self.combinationProb = combinationProbNotNormalized / instance.peers.size
    end
  end

end



end
end
end
end