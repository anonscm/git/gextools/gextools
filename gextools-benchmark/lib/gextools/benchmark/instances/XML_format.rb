#
# Gextools Benchmark - Somewhere Benchmark component
# (https://sourcesup.renater.fr/projects/gextools/ )
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'builder'
require 'rexml/document'


module GexTools
module Components
module Benchmark
module Instances

require 'gextools/instances/instances'
require 'gextools/instances/instance_chunk_format'
require 'gextools/progs/progs'

require 'gextools/benchmark/logic/peer'


# Deals with saving and importing a peer in swr format
class XMLFormat <  GexTools::Instances::InstanceChunkFormat
  
	def self.parse(content)
		raise ArgumentError.new(:content) if content.nil?

    doc = REXML::Document.new(content)
    name = doc.elements['peer/uri'].text

    lcElements = doc.elements['peer/localConsequencesFactor']
    lcFactor = lcElements.text unless lcElements.nil?

    pqElements = doc.elements['peer/propagateQueriesFactor']
    pqFactor = pqElements.text unless pqElements.nil?

    combElements = doc.elements['peer/combinationFactor']
    combFactor = combElements.text unless combElements.nil?
    
    neighbors = []
    doc.elements.each('peer/neighbors/neighbor') { |element|
      neighbors.push(element.text)
    }

		peer = GexTools::Components::Benchmark::Logic::Peer.new(name, lcFactor, pqFactor, combFactor, neighbors)
		return peer
	end


  def self.export(peers={})

    contents = {}
    peers.each { |name, peer|

      # Build the main xml to be saved as "extractedResult"
      xml = ''
      doc = ::Builder::XmlMarkup.new(:target=> xml, :indent => 1 )
      doc.instruct! :xml, :encoding => "UTF-8"

      # Root element
      doc.peer do

        # Write name, instance name and localConsequenceFactor
        doc.uri(name)
        doc.instance(peer[:instance])
        doc.localConsequencesFactor(peer[:lcFactor])
        doc.propagateQueriesFactor(peer[:pqFactor])

        # Write neighbors information
        doc.neighbors do
          peer[:neighbors].each { |neighbor|
            doc.neighbor(neighbor)
          }
        end

      end

      contents[name] = xml
    }

    return contents

  end
  
  def self.prepareSatAnalysis(instance)
    # Unsupported
    raise NotImplementedError.new
  end

end



end
end
end
end