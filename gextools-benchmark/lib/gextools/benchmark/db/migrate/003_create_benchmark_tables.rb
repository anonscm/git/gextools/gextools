#
# Gextools Benchmark - Somewhere Benchmark component
# (https://sourcesup.renater.fr/projects/gextools/ )
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#

require 'logger'
require 'rubygems'
require 'active_record'



# Migrate everything
class CreateBenchmarkTables < ActiveRecord::Migration

	def self.up
    begin
      return if table_exists? "benchmark_instances"

      create_table :benchmark_instances do |t|
        t.column :peersNumber, :int, :null => false
        t.column :answersProb, :float
        t.column :propagateProb, :float
        t.column :combinationProb, :float
      end

      create_table :benchmark_queries do |t|
        t.column :queriedPeer, :string, :null => false
        t.column :payload, :string, :null => false
      end
      
    rescue
      puts "\nException while migrating the database =>" + $!
      puts "Rolling back transaction...\n"
      self.down
      raise "Done rollback."
    end

	end

	def self.down
		drop_table :benchmark_instances
    drop_table :benchmark_queries
	end

end