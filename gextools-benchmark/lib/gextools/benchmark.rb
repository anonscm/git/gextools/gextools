#
# Gextools Benchmark - Somewhere Benchmark component
# (https://sourcesup.renater.fr/projects/gextools/ )
# Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.  The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#
# To change this template, choose Tools | Templates
# and open the template in the editor.

module GexTools
module Components
module Benchmark

  GexTools::Components::Benchmark.autoload(:Experiments, "gextools/benchmark/experiments/experiments")
  GexTools::Components::Benchmark.autoload(:Instances, "gextools/benchmark/instances/instances")
  GexTools::Components::Benchmark.autoload(:Problems, "gextools/benchmark/problems/problems")
  GexTools::Components::Benchmark.autoload(:Results, "gextools/benchmark/results/results")

end
end
end