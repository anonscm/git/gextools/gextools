lib = File.expand_path('../lib/', __FILE__)
$:.unshift lib unless $:.include?(lib)

Gem::Specification.new do |s|
  s.name = 'gextools-benchmark'
  s.version = '1.0'
  s.has_rdoc = true
  s.date = Time.now.utc.strftime("%Y-%m-%d")
  s.extra_rdoc_files = ['NOTICE', 'LICENSE']
  s.summary = 'Gextools Benchmark - Somewhere Benchmark component'
  s.description = s.summary
  s.author = 'Andre Fonseca'
  s.email = 'andre.amorimfonseca@gmail.com'
  s.files = %w(LICENSE NOTICE Rakefile) + Dir.glob("{lib,spec}/**/*")
  s.require_path = "lib"

  s.add_runtime_dependency "gextools"
  s.add_runtime_dependency "rexml/document"
end
